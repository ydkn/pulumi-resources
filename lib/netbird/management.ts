import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Certificate } from "../certManager/index.js";
import * as node from "../node/index.js";

export interface IdpArgs {
  issuer: pulumi.Input<string>;
  audience: pulumi.Input<string>;
  managerType: pulumi.Input<string>;
  tokenEndpoint: pulumi.Input<string>;
  clientId: pulumi.Input<string>;
  clientSecret: pulumi.Input<string>;
  oidcConfigurationEndpoint: pulumi.Input<string>;
  managementEndpoint: pulumi.Input<string>;
}

export interface ManagementArgs {
  namespace?: pulumi.Input<string>;
  idp: pulumi.Input<IdpArgs>;
  hostname: pulumi.Input<string>;
  relayHostname: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  clusterIssuer?: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  storageSize?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  version?: pulumi.Input<string>;
  protect?: boolean;
}

export class Management extends ComponentResource {
  public static readonly relayPort = 33080;

  private readonly relaySecretToken: pulumi.Output<string>;
  private readonly dataEncryptionKey: pulumi.Output<string>;
  private readonly signalDeployment: k8s.apps.v1.Deployment;
  private readonly signalPodDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly signalService: kubernetes.v1.Service;
  private readonly relayCertificate: Certificate;
  private readonly relaySecret: kubernetes.v1.Secret;
  private readonly relayDaemonSet: k8s.apps.v1.DaemonSet;
  private readonly relayPodDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly relayService: kubernetes.v1.Service;
  private readonly managementSecret: kubernetes.v1.Secret;
  private readonly managementVolumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly managementDeployment: k8s.apps.v1.Deployment;
  private readonly managementPodDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly managementService: kubernetes.v1.Service;
  private readonly dashboardSecret: kubernetes.v1.Secret;
  private readonly dashboardDeployment: k8s.apps.v1.Deployment;
  private readonly dashboardPodDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly dashboardService: kubernetes.v1.Service;
  private readonly ingress: kubernetes.v1.SecureIngress;
  public readonly namespace: pulumi.Output<string>;
  public readonly managementUrl: pulumi.Output<string>;
  public readonly adminUrl: pulumi.Output<string>;

  /**
   * Create a new Netbird management resource.
   * @param name The name of the Netbird management resource.
   * @param args A bag of arguments to control the Netbird management creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ManagementArgs, opts?: pulumi.ComponentResourceOptions) {
    super("netbird:mgmt", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    if (args.storageSize === undefined) {
      args.storageSize = "10Gi";
    }

    this.relaySecretToken = pulumi.secret(
      new random.RandomPassword(`${name}-relay-secret`, { length: 32, special: false }, emptyOpts).result,
    );
    this.dataEncryptionKey = pulumi.secret(
      new random.RandomString(`${name}-data`, { length: 32 }, emptyOpts).result.apply((key) =>
        Buffer.from(key).toString("base64"),
      ),
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "netbird",
        "app.kubernetes.io/component": "netbird",
        "app.kubernetes.io/instance": name,
      },
    };

    const signalMetadata = {
      name: `${name}-signal`,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "netbird",
        "app.kubernetes.io/component": "signal",
        "app.kubernetes.io/instance": name,
      },
    };

    this.signalDeployment = new k8s.apps.v1.Deployment(
      `${name}-signal`,
      {
        metadata: signalMetadata,
        spec: {
          selector: { matchLabels: signalMetadata.labels },
          replicas: args.replicas,
          template: {
            metadata: { labels: signalMetadata.labels },
            spec: {
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              containers: [
                {
                  name: "signal",
                  image: pulumi.interpolate`netbirdio/signal:${args.version ?? "latest"}`,
                  imagePullPolicy: "Always",
                  ports: [
                    { name: "http", protocol: "TCP", containerPort: 80 },
                    { name: "signal", protocol: "TCP", containerPort: 10000 },
                  ],
                  resources: {
                    requests: { memory: "96Mi" },
                    limits: { memory: "128Mi" },
                  },
                },
              ],
              affinity: Management.affinity(signalMetadata),
            },
          },
        },
      },
      k8sOpts,
    );

    this.signalPodDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      `${name}-signal`,
      {
        metadata: signalMetadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.signalService = new kubernetes.v1.Service(
      `${name}-signal`,
      {
        metadata: {
          ...signalMetadata,
          annotations: {
            "traefik.ingress.kubernetes.io/service.serversscheme": "h2c",
            "traefik.ingress.kubernetes.io/service.passhostheader": "true",
          },
        },
        spec: {
          selector: signalMetadata.labels,
          ports: [{ name: "signal", protocol: "TCP", port: 10000, targetPort: "signal" }],
        },
      },
      { ...k8sOpts, dependsOn: [this.signalDeployment] },
    );

    const relayMetadata = {
      name: `${name}-relay`,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "netbird",
        "app.kubernetes.io/component": "relay",
        "app.kubernetes.io/instance": name,
      },
    };

    this.relayCertificate = new Certificate(
      `${name}-relay`,
      {
        metadata: relayMetadata,
        spec: {
          commonName: args.relayHostname,
          dnsNames: [args.relayHostname],
          privateKey: { algorithm: "RSA", size: 4096, rotationPolicy: "Always" },
          issuerRef: {
            kind: "ClusterIssuer",
            name: args.clusterIssuer ?? Certificate.DefaultClusterIssuer,
          },
        },
      },
      k8sOpts,
    );

    this.relaySecret = new kubernetes.v1.Secret(
      `${name}-relay`,
      {
        metadata: relayMetadata,
        stringData: {
          AUTH_SECRET: this.relaySecretToken,
        },
      },
      k8sOpts,
    );

    this.relayDaemonSet = new k8s.apps.v1.DaemonSet(
      `${name}-relay`,
      {
        metadata: relayMetadata,
        spec: {
          selector: { matchLabels: relayMetadata.labels },
          template: {
            metadata: { labels: relayMetadata.labels },
            spec: {
              nodeSelector: args.nodeSelector ?? node.gatewayNodeSelector,
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              hostNetwork: true,
              containers: [
                {
                  name: "relay",
                  image: pulumi.interpolate`netbirdio/relay:${args.version ?? "latest"}`,
                  imagePullPolicy: "Always",
                  env: [
                    { name: "NB_LOG_LEVEL", value: "info" },
                    { name: "NB_LISTEN_ADDRESS", value: `:${Management.relayPort}` },
                    {
                      name: "NB_EXPOSED_ADDRESS",
                      value: pulumi.interpolate`rels://${args.relayHostname}:${Management.relayPort}`,
                    },
                    { name: "NB_TLS_CERT_FILE", value: "/certs/tls.crt" },
                    { name: "NB_TLS_KEY_FILE", value: "/certs/tls.key" },
                  ],
                  envFrom: [{ secretRef: { name: this.relaySecret.metadata.name }, prefix: "NB_" }],
                  ports: [
                    {
                      name: "relay",
                      protocol: "TCP",
                      containerPort: Management.relayPort,
                      hostPort: Management.relayPort,
                    },
                  ],
                  volumeMounts: [{ name: "certs", mountPath: "/certs", readOnly: true }],
                  resources: {
                    requests: { memory: "64Mi" },
                    limits: { memory: "96Mi" },
                  },
                },
              ],
              affinity: Management.affinity(relayMetadata),
              volumes: [{ name: "certs", secret: { secretName: this.relayCertificate.secretName } }],
            },
          },
        },
      },
      k8sOpts,
    );

    this.relayPodDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      `${name}-relay`,
      {
        metadata: relayMetadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.relayService = new kubernetes.v1.Service(
      `${name}-relay`,
      {
        metadata: relayMetadata,
        spec: {
          selector: relayMetadata.labels,
          ports: [{ name: "relay", protocol: "TCP", port: Management.relayPort, targetPort: "relay" }],
        },
      },
      { ...k8sOpts, dependsOn: [this.relayDaemonSet] },
    );

    const managementMetadata = {
      name: `${name}-management`,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "netbird",
        "app.kubernetes.io/component": "management",
        "app.kubernetes.io/instance": name,
      },
    };

    this.managementSecret = new kubernetes.v1.Secret(
      `${name}-management`,
      {
        metadata: managementMetadata,
        stringData: {
          "management.json": pulumi
            .all([args.idp, args.hostname, args.relayHostname, this.relaySecretToken, this.dataEncryptionKey])
            .apply(([idp, hostname, relayHostname, relaySecretToken, dataEncryptionKey]) =>
              JSON.stringify({
                Stuns: [],
                TURNConfig: { Turns: [], TimeBasedCredentials: false, CredentialsTTL: "24h" },
                Relay: {
                  Addresses: [`rels://${relayHostname}:${Management.relayPort}`],
                  CredentialsTTL: "24h",
                  Secret: relaySecretToken,
                },
                Signal: {
                  Proto: "https",
                  URI: `${hostname}:443`,
                },
                HttpConfig: {
                  AuthIssuer: idp.issuer,
                  AuthAudience: idp.audience,
                  OIDCConfigEndpoint: idp.oidcConfigurationEndpoint,
                },
                IdpManagerConfig: {
                  ManagerType: idp.managerType,
                  ClientConfig: {
                    Issuer: idp.issuer,
                    TokenEndpoint: idp.tokenEndpoint,
                    ClientID: idp.clientId,
                    ClientSecret: idp.clientSecret,
                    GrantType: "client_credentials",
                  },
                  ExtraConfig: { ManagementEndpoint: idp.managementEndpoint },
                },
                PKCEAuthorizationFlow: {
                  ProviderConfig: {
                    Audience: idp.audience,
                    ClientID: idp.audience,
                    Scope: "openid email profile offline_access roles",
                    RedirectURLs: ["http://localhost:53000/", "http://localhost:54000/"],
                  },
                },
                StoreConfig: { Engine: "sqlite" },
                DataStoreEncryptionKey: dataEncryptionKey,
              }),
            ),
        },
      },
      k8sOpts,
    );

    const managementVolumeClaimName = managementMetadata.name;

    this.managementVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      `${name}-management`,
      {
        metadata: managementMetadata,
        spec: {
          storageClassName: args.storageClassName,
          accessModes: ["ReadWriteOnce"],
          resources: { requests: { storage: args.storageSize } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.managementDeployment = new k8s.apps.v1.Deployment(
      `${name}-management`,
      {
        metadata: managementMetadata,
        spec: {
          selector: { matchLabels: managementMetadata.labels },
          replicas: args.replicas,
          strategy: { type: "Recreate" },
          template: {
            metadata: { labels: managementMetadata.labels },
            spec: {
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              initContainers: [
                {
                  name: "config",
                  image: "busybox:latest",
                  imagePullPolicy: "Always",
                  command: ["sh", "-c", "cat /tmp/management.json > /etc/netbird/management.json"],
                  volumeMounts: [
                    { name: "management-config", mountPath: "/tmp/management.json", subPath: "management.json" },
                    { name: "management-etc", mountPath: "/etc/netbird" },
                  ],
                  resources: {
                    requests: { memory: "24Mi" },
                    limits: { memory: "32Mi" },
                  },
                },
              ],
              containers: [
                {
                  name: "management",
                  image: pulumi.interpolate`netbirdio/management:${args.version ?? "latest"}`,
                  imagePullPolicy: "Always",
                  args: [
                    "--port=80",
                    "--log-file=console",
                    "--log-level=info",
                    "--disable-anonymous-metrics=true",
                    pulumi.interpolate`--single-account-mode-domain=${args.hostname}`,
                    "--dns-domain=netbird",
                    "--idp-sign-key-refresh-enabled",
                    "--disable-anonymous-metrics",
                  ],
                  ports: [{ name: "http", protocol: "TCP", containerPort: 80 }],
                  volumeMounts: [
                    { name: "management-etc", mountPath: "/etc/netbird" },
                    { name: "data", mountPath: "/var/lib/netbird" },
                  ],
                  resources: {
                    requests: { memory: "256Mi" },
                    limits: { memory: "384Mi" },
                  },
                },
              ],
              affinity: Management.affinity(managementMetadata),
              volumes: [
                { name: "management-config", secret: { secretName: this.managementSecret.metadata.name } },
                { name: "data", persistentVolumeClaim: { claimName: managementVolumeClaimName } },
                { name: "management-etc", emptyDir: {} },
              ],
            },
          },
        },
      },
      k8sOpts,
    );

    this.managementPodDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      `${name}-management`,
      {
        metadata: managementMetadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: managementMetadata.labels },
        },
      },
      k8sOpts,
    );

    this.managementService = new kubernetes.v1.Service(
      `${name}-management`,
      {
        metadata: {
          ...managementMetadata,
          annotations: {
            "traefik.ingress.kubernetes.io/service.serversscheme": "h2c",
            "traefik.ingress.kubernetes.io/service.passhostheader": "true",
          },
        },
        spec: {
          selector: managementMetadata.labels,
          ports: [{ name: "http", protocol: "TCP", port: 80 }],
        },
      },
      { ...k8sOpts, dependsOn: [this.managementDeployment] },
    );

    const dashboardMetadata = {
      name: `${name}-dashboard`,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "netbird",
        "app.kubernetes.io/component": "dashboard",
        "app.kubernetes.io/instance": name,
      },
    };

    this.dashboardSecret = new kubernetes.v1.Secret(
      `${name}-dashboard`,
      {
        metadata: dashboardMetadata,
        stringData: pulumi.all([args.idp, args.hostname]).apply(([idp, hostname]) => {
          return {
            NETBIRD_MGMT_API_ENDPOINT: `https://${hostname}`,
            NETBIRD_MGMT_GRPC_API_ENDPOINT: `https://${hostname}`,
            AUTH_AUDIENCE: idp.audience,
            AUTH_CLIENT_ID: idp.audience,
            AUTH_AUTHORITY: idp.issuer,
            USE_AUTH0: "false",
            AUTH_SUPPORTED_SCOPES: "openid email profile offline_access roles",
            AUTH_REDIRECT_URI: "/nb-auth",
            AUTH_SILENT_REDIRECT_URI: "/nb-silent-auth",
            NETBIRD_TOKEN_SOURCE: "accessToken",
          };
        }),
      },
      k8sOpts,
    );

    this.dashboardDeployment = new k8s.apps.v1.Deployment(
      `${name}-dashboard`,
      {
        metadata: dashboardMetadata,
        spec: {
          selector: { matchLabels: dashboardMetadata.labels },
          replicas: args.replicas,
          template: {
            metadata: { labels: dashboardMetadata.labels },
            spec: {
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              containers: [
                {
                  name: "dashboard",
                  image: "netbirdio/dashboard:latest",
                  imagePullPolicy: "Always",
                  envFrom: [{ secretRef: { name: this.dashboardSecret.metadata.name } }],
                  ports: [{ name: "http", protocol: "TCP", containerPort: 80 }],
                  resources: {
                    requests: { memory: "64Mi" },
                    limits: { memory: "96Mi" },
                  },
                },
              ],
              affinity: Management.affinity(dashboardMetadata),
            },
          },
        },
      },
      k8sOpts,
    );

    this.dashboardPodDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      `${name}-dashboard`,
      {
        metadata: dashboardMetadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: dashboardMetadata.labels },
        },
      },
      k8sOpts,
    );

    this.dashboardService = new kubernetes.v1.Service(
      `${name}-dashboard`,
      {
        metadata: {
          ...dashboardMetadata,
          annotations: {
            "traefik.ingress.kubernetes.io/service.passhostheader": "true",
          },
        },
        spec: {
          selector: dashboardMetadata.labels,
          ports: [{ name: "http", protocol: "TCP", port: 80 }],
        },
      },
      { ...k8sOpts, dependsOn: [this.dashboardDeployment] },
    );

    this.ingress = new kubernetes.v1.SecureIngress(
      name,
      {
        metadata,
        spec: {
          ingressClassName: args.ingressClassName,
          rules: [
            {
              host: args.hostname,
              http: {
                paths: [
                  {
                    path: "/signalexchange.SignalExchange/",
                    pathType: "Prefix",
                    backend: { service: { name: this.signalService.metadata.name, port: { name: "signal" } } },
                  },
                  {
                    path: "/api/",
                    pathType: "Prefix",
                    backend: { service: { name: this.managementService.metadata.name, port: { name: "http" } } },
                  },
                  {
                    path: "/management.ManagementService/",
                    pathType: "Prefix",
                    backend: { service: { name: this.managementService.metadata.name, port: { name: "http" } } },
                  },
                  {
                    path: "/",
                    pathType: "Prefix",
                    backend: { service: { name: this.dashboardService.metadata.name, port: { name: "http" } } },
                  },
                ],
              },
            },
          ],
        },
        clusterIssuer: args.clusterIssuer,
      },
      k8sOpts,
    );

    this.managementUrl = pulumi.interpolate`https://${args.hostname}`;
    this.adminUrl = pulumi.interpolate`https://${args.hostname}`;

    this.registerOutputs();
  }

  private static affinity(metadata: { labels: { [key: string]: string } }): k8s.types.output.core.v1.Affinity {
    return {
      podAntiAffinity: {
        preferredDuringSchedulingIgnoredDuringExecution: [
          {
            weight: 100,
            podAffinityTerm: {
              labelSelector: {
                matchExpressions: [
                  {
                    key: "app.kubernetes.io/name",
                    operator: "In",
                    values: [metadata.labels["app.kubernetes.io/name"]],
                  },
                  {
                    key: "app.kubernetes.io/component",
                    operator: "In",
                    values: [metadata.labels["app.kubernetes.io/component"]],
                  },
                  {
                    key: "app.kubernetes.io/instance",
                    operator: "In",
                    values: [metadata.labels["app.kubernetes.io/instance"]],
                  },
                ],
              },
              topologyKey: "kubernetes.io/hostname",
            },
          },
        ],
      },
    } as k8s.types.output.core.v1.Affinity;
  }
}
