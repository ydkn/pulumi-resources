#!/bin/sh

INTERVAL="20"
INITAL_DELAY="10"

apk --quiet --no-cache add nftables

sleep "${INITAL_DELAY}" &
wait $!

while :; do
  if [ -n "${NETWORK_RANGE}" ]; then
    input_handle=$(nft -a -n list chain ip netbird netbird-acl-input-filter 2>/dev/null | grep "iifname \"${NB_INTERFACE_NAME}\" ip saddr ${NETWORK_RANGE} ip daddr ${NETWORK_RANGE} jump netbird-acl-input-rules" | awk '{print $NF}')
    if [ -z "${input_handle}" ]; then
      nft insert rule ip netbird netbird-acl-input-filter iifname "${NB_INTERFACE_NAME}" ip saddr "${NETWORK_RANGE}" ip daddr "${NETWORK_RANGE}" jump netbird-acl-input-rules
    fi

    output_handle=$(nft -a -n list chain ip netbird netbird-acl-output-filter 2>/dev/null | grep "oifname \"${NB_INTERFACE_NAME}\" ip saddr ${NETWORK_RANGE} ip daddr ${NETWORK_RANGE} jump netbird-acl-output-rules" | awk '{print $NF}')
    if [ -z "${output_handle}" ]; then
      nft insert rule ip netbird netbird-acl-output-filter oifname "${NB_INTERFACE_NAME}" ip saddr "${NETWORK_RANGE}" ip daddr "${NETWORK_RANGE}" jump netbird-acl-output-rules
    fi
  fi

  if [ "${MASQUERADE_ALL}" = "true" ]; then
    if ! nft -a -n list chain ip netbird netbird-rt-postrouting 2>/dev/null | grep "type nat hook postrouting priority" >/dev/null; then
      nft add chain ip netbird netbird-rt-postrouting '{ type nat hook postrouting priority srcnat - 1; }'
    fi

    masquerade_handle=$(nft -a -n list chain ip netbird netbird-rt-postrouting 2>/dev/null | grep "oifname \"${NB_INTERFACE_NAME}\" masquerade" | awk '{print $NF}')
    if [ -z "${masquerade_handle}" ]; then
      nft insert rule ip netbird netbird-rt-postrouting oifname "${NB_INTERFACE_NAME}" masquerade
    fi
  fi

  if [ -n "${MSS}" ]; then
    mss_handle=$(nft -a -n list chain ip netbird netbird-rt-fwd 2>/dev/null | grep "oifname \"${NB_INTERFACE_NAME}\"" | grep "maxseg size set ${MSS}" | awk '{print $NF}')
    if [ -z "${mss_handle}" ]; then
      nft insert rule ip netbird netbird-rt-fwd oifname "${NB_INTERFACE_NAME}" tcp flags syn / syn,rst counter tcp option maxseg size set "${MSS}"
    fi
  fi

  sleep "${INTERVAL}" &
  wait $!
done
