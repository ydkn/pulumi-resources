import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as node from "../node/index.js";

export interface Interface {
  addresses: pulumi.Input<pulumi.Input<string>[]>;
  port?: pulumi.Input<number>;
  privateKey: pulumi.Input<string>;
  name?: pulumi.Input<string>;
  mtu?: pulumi.Input<number>;
}

export interface Peer {
  publicKey: pulumi.Input<string>;
  presharedKey?: pulumi.Input<string>;
  allowedIPs: pulumi.Input<pulumi.Input<string>[]>;
  endpoint?: pulumi.Input<string>;
}

export interface WireguardArgs {
  namespace?: pulumi.Input<string>;
  interface: pulumi.Input<Interface>;
  peers?: pulumi.Input<pulumi.Input<Peer>[]>;
  natInterface?: pulumi.Input<string>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export class Wireguard extends ComponentResource {
  public static readonly defaultInterfaceName = "wireguard";
  public static readonly defaultInterfacePort = 51920;
  public static readonly defaultMTU = 1500;

  private readonly config: kubernetes.v1.Secret;
  private readonly daemonSet: k8s.apps.v1.DaemonSet;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  public readonly interfaceName: pulumi.Output<string>;
  public readonly interfacePort: pulumi.Output<number>;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new wireguard resource.
   * @param name The name of the wireguard resource.
   * @param args A bag of arguments to control the wireguard deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: WireguardArgs, opts?: pulumi.ComponentResourceOptions) {
    super("wireguard", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.interfaceName = pulumi.all([args.interface]).apply(([iface]) => iface.name ?? Wireguard.defaultInterfaceName);
    this.interfacePort = pulumi.all([args.interface]).apply(([iface]) => iface.port ?? Wireguard.defaultInterfacePort);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "wireguard",
        "app.kubernetes.io/component": "wireguard",
        "app.kubernetes.io/instance": name,
      },
    };

    const configData = pulumi
      .all([args.interface, this.interfaceName, this.interfacePort, args.peers ?? [], args.natInterface])
      .apply(([iface, interfaceName, interfacePort, peers, natInterface]) =>
        pulumi.all([iface.addresses]).apply((addresses) =>
          pulumi.all(peers).apply((peers) => {
            const conf = [
              "[Interface]",
              `Address = ${addresses.join(",")}`,
              `MTU = ${iface.mtu ?? Wireguard.defaultMTU}`,
              `ListenPort = ${interfacePort}`,
              `PrivateKey = ${iface.privateKey}`,
              `PostUp = iptables -A FORWARD -i %i -j ACCEPT`,
              `PostUp = iptables -A FORWARD -o %i -j ACCEPT`,
              `PostDown = iptables -D FORWARD -i %i -j ACCEPT`,
              `PostDown = iptables -D FORWARD -o %i -j ACCEPT`,
            ];

            if (natInterface !== undefined) {
              conf.push(
                `PostUp = iptables -t nat -A POSTROUTING -o ${natInterface} -j MASQUERADE`,
                `PostDown = iptables -t nat -D POSTROUTING -o ${natInterface} -j MASQUERADE`,
              );
            }

            for (const peer of peers) {
              conf.push(
                "[Peer]",
                "PersistentKeepalive = 10",
                `AllowedIPs = ${peer.allowedIPs.join(",")}`,
                `PublicKey = ${peer.publicKey}`,
              );

              if (peer.presharedKey !== undefined) {
                conf.push(`PresharedKey = ${peer.presharedKey}`);
              }

              if (peer.endpoint !== undefined) {
                const endpoint = /:\d{1,5}$/.exec(peer.endpoint)
                  ? peer.endpoint
                  : `${peer.endpoint}:${Wireguard.defaultInterfacePort}`;

                conf.push(`Endpoint = ${endpoint}`);
              }
            }

            const data: { [key: string]: string } = {};
            data[`${interfaceName}.conf`] = conf.join("\n");

            return data;
          }),
        ),
      );

    this.config = new kubernetes.v1.Secret(name, { metadata, stringData: configData }, opts);

    this.daemonSet = new k8s.apps.v1.DaemonSet(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              nodeSelector: args.nodeSelector ?? node.gatewayNodeSelector,
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              hostNetwork: true,
              containers: [
                {
                  name: "wireguard",
                  image: "ydkn/k8s-wireguard:latest",
                  imagePullPolicy: "Always",
                  securityContext: { privileged: true, capabilities: { add: ["NET_ADMIN"] } },
                  volumeMounts: [{ name: "conf", mountPath: "/etc/wireguard", readOnly: true }],
                  ports: [
                    {
                      name: "wireguard",
                      protocol: "UDP",
                      containerPort: this.interfacePort,
                      hostPort: this.interfacePort,
                    },
                  ],
                  resources: {
                    requests: { memory: "8Mi" },
                    limits: { memory: "12Mi" },
                  },
                },
              ],
              volumes: [{ name: "conf", secret: { secretName: metadata.name, defaultMode: 0o400 } }],
            },
          },
        },
      },
      opts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          maxUnavailable: 0,
          selector: { matchLabels: metadata.labels },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
