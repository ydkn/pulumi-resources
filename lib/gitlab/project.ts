import * as pulumi from "@pulumi/pulumi";
import * as gitlab from "@pulumi/gitlab";
import { ComponentResource } from "../common/index.js";

export interface ProjectArgs extends gitlab.ProjectArgs {
  protect?: boolean;
}

export class Project extends ComponentResource {
  private readonly project: gitlab.Project;
  private readonly pipelineBadge: gitlab.ProjectBadge;

  /**
   * Create a new GitLab project resource.
   * @param name The name of the GitLab project resource.
   * @param args A bag of arguments to control the GitLab project creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ProjectArgs, opts?: pulumi.ComponentResourceOptions) {
    super("gitlab:project", name, args, opts);

    opts = this.opts(gitlab.Provider);

    if (args.autoCancelPendingPipelines === undefined) {
      args.autoCancelPendingPipelines = "enabled";
    }

    if (args.autoDevopsEnabled === undefined) {
      args.autoDevopsEnabled = false;
    }

    if (args.autocloseReferencedIssues === undefined) {
      args.autocloseReferencedIssues = true;
    }

    if (args.buildsAccessLevel === undefined) {
      args.buildsAccessLevel = "private";
    }

    if (args.containerRegistryAccessLevel === undefined) {
      args.containerRegistryAccessLevel = "private";
    }

    args.containerExpirationPolicy = pulumi
      .all([args.containerRegistryAccessLevel, args.containerExpirationPolicy])
      .apply(([accessLevel, policy]) => {
        if (accessLevel === "disabled") {
          return {};
        }

        if (policy !== undefined) {
          return policy;
        }

        return {
          enabled: true,
          cadence: "1d",
          olderThan: "14d",
          keepN: 10,
          nameRegexDelete: ".*",
          nameRegexKeep: "^(v\\d(\\.\\d)+|latest)$",
        };
      });

    if (args.defaultBranch === undefined) {
      args.defaultBranch = "main";
    }

    if (args.initializeWithReadme === undefined) {
      args.initializeWithReadme = false;
    }

    if (args.issuesEnabled === undefined) {
      args.issuesEnabled = true;
    }

    if (args.lfsEnabled === undefined) {
      args.lfsEnabled = false;
    }

    if (args.mergeRequestsEnabled === undefined) {
      args.mergeRequestsEnabled = true;
    }

    if (args.onlyAllowMergeIfAllDiscussionsAreResolved === undefined) {
      args.onlyAllowMergeIfAllDiscussionsAreResolved = true;
    }

    if (args.onlyAllowMergeIfPipelineSucceeds === undefined) {
      args.onlyAllowMergeIfPipelineSucceeds = true;
    }

    if (args.packagesEnabled === undefined) {
      args.packagesEnabled = false;
    }

    if (args.pagesAccessLevel === undefined) {
      args.pagesAccessLevel = "disabled";
    }

    if (args.printingMergeRequestLinkEnabled === undefined) {
      args.printingMergeRequestLinkEnabled = true;
    }

    if (args.removeSourceBranchAfterMerge === undefined) {
      args.removeSourceBranchAfterMerge = true;
    }

    if (args.sharedRunnersEnabled === undefined) {
      args.sharedRunnersEnabled = true;
    }

    if (args.snippetsEnabled === undefined) {
      args.snippetsEnabled = false;
    }

    if (args.squashOption === undefined) {
      args.squashOption = "default_on";
    }

    if (args.visibilityLevel === undefined) {
      args.visibilityLevel = "private";
    }

    if (args.wikiEnabled === undefined) {
      args.wikiEnabled = false;
    }

    this.project = new gitlab.Project(name, args, { ...opts, protect: args.protect });

    this.pipelineBadge = new gitlab.ProjectBadge(
      `${name}-pipeline-badge`,
      {
        project: this.project.id,
        name: "Pipeline Status",
        linkUrl: "https://gitlab.com/%{project_path}/pipelines",
        imageUrl: "https://gitlab.com/%{project_path}/badges/main/pipeline.svg",
      },
      opts,
    );

    this.registerOutputs();
  }
}
