import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface RenovateArgs {
  namespace?: pulumi.Input<string>;
  accessToken: pulumi.Input<string>;
  githubAccessToken: pulumi.Input<string>;
  schedule?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export class Renovate extends ComponentResource {
  private readonly secret: kubernetes.v1.Secret;
  private readonly cronjob: k8s.batch.v1.CronJob;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new renovate resource.
   * @param name The name of the renovate resource.
   * @param args A bag of arguments to control the renovate creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: RenovateArgs, opts?: pulumi.ComponentResourceOptions) {
    super("gitlab:renovate", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.schedule === undefined) {
      args.schedule = "0 2 * * 0";
    }

    if (args.resources === undefined) {
      args.resources = { requests: { memory: "1024Mi" }, limits: { memory: "1280Mi" } };
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "gitlab",
        "app.kubernetes.io/component": "renovate",
        "app.kubernetes.io/instance": name,
      },
    };

    this.secret = new kubernetes.v1.Secret(
      name,
      {
        metadata,
        stringData: pulumi.all([args.accessToken, args.githubAccessToken]).apply(([accessToken, githubAccessToken]) => {
          return { RENOVATE_TOKEN: accessToken, GITHUB_COM_TOKEN: githubAccessToken };
        }),
      },
      opts,
    );

    this.cronjob = new k8s.batch.v1.CronJob(
      name,
      {
        metadata,
        spec: {
          schedule: args.schedule,
          concurrencyPolicy: "Forbid",
          successfulJobsHistoryLimit: 0,
          failedJobsHistoryLimit: 1,
          jobTemplate: {
            metadata: { labels: metadata.labels },
            spec: {
              template: {
                spec: {
                  restartPolicy: "OnFailure",
                  containers: [
                    {
                      name: "renovate",
                      image: "renovate/renovate:latest",
                      imagePullPolicy: "Always",
                      args: ["--platform=gitlab", "--autodiscover"],
                      envFrom: [{ secretRef: { name: this.secret.metadata.name } }],
                      resources: args.resources,
                    },
                  ],
                  tolerations: [{ key: "ydkn.network/role", operator: "Equal", value: "build", effect: "NoSchedule" }],
                },
              },
              backoffLimit: 10,
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
