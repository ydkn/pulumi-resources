import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../../common/index.js";
import { RunnerArgs, Runner } from "./runner.js";

export interface RunnersArgs {
  namespace?: pulumi.Input<string>;
  runners: pulumi.Input<{ [key: string]: pulumi.Input<RunnerArgs> }>;
}

export class Runners extends ComponentResource {
  private readonly runners: pulumi.Output<Runner[]>;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new GitLab runners resource.
   * @param name The name of the GitLab runners resource.
   * @param args A bag of arguments to control the GitLab runners creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: RunnersArgs, opts?: pulumi.ComponentResourceOptions) {
    super("gitlab:runners", name, args, opts);

    const k8sOpts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    this.runners = pulumi.all([args.runners]).apply(([runnerConfigs]) => {
      const runners: Runner[] = [];

      for (const runnerName in runnerConfigs) {
        // eslint-disable-next-line security/detect-object-injection
        const runner = new Runner(
          runnerName,
          { ...runnerConfigs[runnerName], namespace: this.namespace },
          { ...opts, parent: this },
        );

        runners.push(runner);
      }

      return runners;
    });

    this.registerOutputs();
  }
}
