import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../../common/index.js";
import * as kubernetes from "../../kubernetes/index.js";

export interface RunnerArgs {
  request?: pulumi.Input<string>;
  limit?: pulumi.Input<string>;
}

export interface RunnerArgs {
  namespace?: pulumi.Input<string>;
  token: pulumi.Input<string>;
  concurrency?: pulumi.Input<number>;
  checkInterval?: pulumi.Input<number>;
  image?: pulumi.Input<string>;
  memory?: pulumi.Input<RunnerArgs>;
  helperMemory?: pulumi.Input<RunnerArgs>;
  serviceMemory?: pulumi.Input<RunnerArgs>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export class Runner extends ComponentResource {
  private static readonly defaultImage = "ubuntu:latest";

  private readonly helmRelease: k8s.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new GitLab runner resource.
   * @param name The name of the GitLab runner resource.
   * @param args A bag of arguments to control the GitLab runner creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: RunnerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("gitlab:runners:runner", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.concurrency === undefined) {
      args.concurrency = 2;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "gitlab-runners",
        "app.kubernetes.io/component": "runner",
        "app.kubernetes.io/instance": name,
      },
    };

    const config = pulumi
      .all([args.image, args.memory, args.helperMemory, args.serviceMemory, args.nodeSelector])
      .apply(([image, memory, helperMemory, serviceMemory, nodeSelector]) =>
        [
          "[[runners]]",
          "[runners.feature_flags]",
          "FF_GITLAB_REGISTRY_HELPER_IMAGE = true",
          "[runners.kubernetes]",
          `image = "${image ?? Runner.defaultImage}"`,
          memory && memory.request ? `memory_request = "${memory.request}"` : undefined,
          memory && memory.limit ? `memory_limit = "${memory.limit}"` : undefined,
          helperMemory && helperMemory.request ? `helper_memory_request = "${helperMemory.request}"` : undefined,
          helperMemory && helperMemory.limit ? `helper_memory_limit = "${helperMemory.limit}"` : undefined,
          serviceMemory && serviceMemory.request ? `service_memory_request = "${serviceMemory.request}"` : undefined,
          serviceMemory && serviceMemory.limit ? `service_memory_limit = "${serviceMemory.limit}"` : undefined,
          "[runners.kubernetes.node_selector]",
          ...(nodeSelector ? Object.keys(nodeSelector).map((key) => `"${key}" = "${nodeSelector[key]}"`) : []),
          "[runners.kubernetes.node_tolerations]",
          `"ydkn.network/role=build" = ""`,
          "[[runners.kubernetes.volumes.host_path]]",
          `name = "docker"`,
          `mount_path = "/var/run/docker.sock"`,
          `path = "/var/run/docker.sock"`,
        ]
          .filter((l) => l)
          .join("\n"),
      );

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: metadata.namespace,
        repositoryOpts: { repo: "https://charts.gitlab.io/" },
        chart: "gitlab-runner",
        values: {
          fullnameOverride: name,
          gitlabUrl: "https://gitlab.com/",
          runnerToken: args.token,
          concurrent: args.concurrency,
          checkInterval: args.checkInterval ?? 30,
          rbac: { create: true },
          runners: { config },
          resources: {
            requests: { memory: "64Mi" },
            limits: { memory: "96Mi" },
          },
          service: { enabled: true },
          metrics: {
            enabled: true,
            serviceMonitor: { enabled: true },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
