import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface MiddlewareHeadersArgs {
  customRequestHeaders?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  customResponseHeaders?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export interface MiddlewareSpecArgs {
  headers?: pulumi.Input<MiddlewareHeadersArgs>;
}

export interface MiddlewareArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<MiddlewareSpecArgs>;
}

export class Middleware extends k8s.apiextensions.CustomResource {
  /**
   * Create a new middlware resource.
   * @param name The name of the middlware resource.
   * @param args A bag of arguments to control the middlware creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: MiddlewareArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        apiVersion: "traefik.io/v1alpha1",
        kind: "Middleware",
        metadata: args.metadata,
        spec: args.spec,
      },
      opts,
    );
  }
}
