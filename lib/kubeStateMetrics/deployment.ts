import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface KubeStateMetricsArgs {
  namespace?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export class KubeStateMetrics extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new kube-state-metrics resource.
   * @param name The name of the kube-state-metrics resource.
   * @param args A bag of arguments to control the kube-state-metrics creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: KubeStateMetricsArgs, opts?: pulumi.ComponentResourceOptions) {
    super("kube-state-metrics", name, args, opts);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    if (args.resources === undefined) {
      args.resources = { requests: { memory: "128Mi" }, limits: { memory: "256Mi" } };
    }

    opts = this.opts(k8s.Provider);

    this.namespace = pulumi.output(args.namespace ?? "kube-system");

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://prometheus-community.github.io/helm-charts" },
        chart: "kube-state-metrics",
        values: {
          fullnameOverride: name,
          replicas: args.replicas,
          prometheus: {
            monitor: {
              enabled: true,
              honorLabels: true,
              metricRelabelings: [
                {
                  action: "drop",
                  regex: "go_.*",
                  sourceLabels: ["__name__"],
                },
              ],
            },
          },
          resources: args.resources,
          service: {
            ipDualStack: {
              ipFamilyPolicy: "PreferDualStack",
            },
          },
          tolerations: [
            { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
            { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
          ],
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
