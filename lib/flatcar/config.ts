import * as pulumi from "@pulumi/pulumi";
import * as compress from "zlib";
import { ComponentResource } from "../common/index.js";
import { IgnitionArgs, IgnitionPartitionArgs } from "../node/index.js";

export interface ConfigArgs extends IgnitionArgs {
  hostname: pulumi.Input<string>;
}

export class Config extends ComponentResource {
  public readonly ignitionConfig: pulumi.Output<string>;

  /**
   * Create a new Flatcar config resource.
   * @param name The name of the Flatcar config resource.
   * @param args A bag of arguments to control the Flatcar config creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ConfigArgs, opts?: pulumi.ComponentResourceOptions) {
    super("flatcar:config", name, args, opts);

    this.ignitionConfig = pulumi
      .all([
        this.defaultConfig(),
        args.hostname,
        args.users,
        args.disks,
        args.filesystems,
        args.files,
        args.links,
        args.systemdUnits,
      ])
      .apply(([config, hostname, users, disks, filesystems, files, links, systemdUnits]) => {
        /* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call */
        if (hostname !== undefined) {
          config.storage.files.push({
            path: "/etc/hostname",
            contents: this.encodeContents(hostname),
            mode: 0o644,
            overwrite: true,
          });
        }

        for (const user of users ?? []) {
          config.passwd.users.push(user);
        }

        for (const disk of disks ?? []) {
          config.storage.disks.push({
            device: disk.device,
            partitions: disk.partitions.map((partition: IgnitionPartitionArgs) => {
              return {
                label: partition.label,
                size_mib: partition.size,
                start_mib: partition.start,
                number: partition.number,
                resize: partition.resize,
              };
            }),
            wipe_table: disk.wipeTable,
          });
        }

        for (const filesystem of filesystems ?? []) {
          config.storage.filesystems.push({
            device: filesystem.device,
            path: filesystem.path,
            format: filesystem.format,
            label: filesystem.label,
          });

          if (filesystem.path !== undefined) {
            const checkSystemdName = filesystem.device.replace(/-/g, "\\x2d").replace(/^\//, "").replace(/\//g, "-");

            config.systemd.units.push({
              name: `${filesystem.path.replace(/^\//, "").replace(/\//g, "-")}.mount`,
              enabled: true,
              contents: [
                "[Unit]",
                "Before=local-fs.target",
                `Requires=systemd-fsck@${checkSystemdName}.service`,
                `After=systemd-fsck@${checkSystemdName}.service`,
                "[Mount]",
                `Where=${filesystem.path}`,
                `What=${filesystem.device}`,
                `Type=${filesystem.format}`,
                "Options=defaults,noatime",
                "[Install]",
                "RequiredBy=local-fs.target",
              ].join("\n"),
            });
          }
        }

        for (const file of files ?? []) {
          config.storage.files.push({
            path: file.path,
            contents: this.encodeContents(file.contents),
            mode: file.mode ?? 0o644,
            overwrite: file.overwrite ?? true,
          });
        }

        for (const link of links ?? []) {
          config.storage.links.push({
            path: link.path,
            target: link.target,
            overwrite: true,
          });
        }

        for (const unit of systemdUnits ?? []) {
          config.systemd.units.push(unit);
        }

        return JSON.stringify(config);
      });

    this.registerOutputs();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private defaultConfig(): any {
    return {
      ignition: { version: "3.3.0" },
      passwd: {
        users: [],
      },
      storage: {
        disks: [],
        filesystems: [],
        directories: [],
        files: [
          {
            path: "/etc/sysctl.d/10-kernel-logging.conf",
            contents: this.encodeContents("kernel.printk=4"),
            mode: 0o644,
            overwrite: true,
          },
          {
            path: "/etc/sysctl.d/20-fs-inotify.conf",
            contents: this.encodeContents("fs.inotify.max_user_instances = 8192\nfs.inotify.max_user_watches = 524288"),
            mode: 0o644,
            overwrite: true,
          },
          {
            path: "/etc/sysctl.d/90-ipv4-ip-forward.conf",
            contents: this.encodeContents("net.ipv4.ip_forward = 1"),
            mode: 0o644,
            overwrite: true,
          },
          {
            path: "/etc/sysctl.d/90-ipv6-ip-forwarding.conf",
            contents: this.encodeContents("net.ipv6.conf.all.forwarding = 1"),
            mode: 0o644,
            overwrite: true,
          },
          { path: "/etc/systemd/resolved.conf", append: [this.encodeContents("LLMNR=no")], overwrite: false },
        ],
        links: [],
      },
      systemd: { units: [] },
    };
  }

  private encodeContents(data: string): { compression: string; source: string } {
    return {
      compression: "gzip",
      source: `data:;base64,${compress.gzipSync(Buffer.from(data), { level: 9 }).toString("base64")}`,
    };
  }
}
