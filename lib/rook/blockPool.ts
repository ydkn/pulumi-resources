import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";

export interface BlockPoolSpecReplicatedArgs {
  size?: pulumi.Input<number>;
}

export interface BlockPoolSpecErasureCodedArgs {
  dataChunks: pulumi.Input<number>;
  codingChunks: pulumi.Input<number>;
}

export interface BlockPoolSpecArgs {
  failureDomain?: pulumi.Input<string>;
  replicated?: pulumi.Input<BlockPoolSpecReplicatedArgs>;
  erasureCoded?: pulumi.Input<BlockPoolSpecErasureCodedArgs>;
}

export interface BlockPoolArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<BlockPoolSpecArgs>;
  clusterId: pulumi.Input<string>;
  defaultStorageClass?: pulumi.Input<boolean>;
}

export class BlockPool extends ComponentResource {
  private readonly cephBlockPool: k8s.apiextensions.CustomResource;
  private readonly storageClass: k8s.storage.v1.StorageClass;

  /**
   * Create a new ceph block pool resource.
   * @param name The name of the ceph block pool resource.
   * @param args A bag of arguments to control the ceph block pool creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: BlockPoolArgs, opts?: pulumi.ComponentResourceOptions) {
    super("rook:ceph:block-pool", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.cephBlockPool = new k8s.apiextensions.CustomResource(
      name,
      {
        apiVersion: "ceph.rook.io/v1",
        kind: "CephBlockPool",
        metadata: args.metadata,
        spec: pulumi.all([args.spec]).apply(([spec]) => {
          return {
            failureDomain: spec.failureDomain ?? "host",
            replicated: spec.replicated,
            erasureCoded: spec.erasureCoded,
          };
        }),
      },
      opts,
    );

    this.storageClass = new k8s.storage.v1.StorageClass(
      name,
      {
        metadata: pulumi.all([args.metadata, args.defaultStorageClass]).apply(([metadata, defaultStorageClass]) => {
          return {
            ...metadata,
            annotations: {
              ...metadata.annotations,
              "storageclass.kubernetes.io/is-default-class": defaultStorageClass ? "true" : "false",
            },
          };
        }),
        provisioner: pulumi.interpolate`${args.clusterId}.rbd.csi.ceph.com`,
        parameters: {
          pool: this.cephBlockPool.metadata.name,
          clusterID: args.clusterId,
          imageFormat: "2",
          imageFeatures: "layering",
          "csi.storage.k8s.io/provisioner-secret-name": "rook-csi-rbd-provisioner",
          "csi.storage.k8s.io/provisioner-secret-namespace": this.cephBlockPool.metadata.namespace,
          "csi.storage.k8s.io/controller-expand-secret-name": "rook-csi-rbd-provisioner",
          "csi.storage.k8s.io/controller-expand-secret-namespace": this.cephBlockPool.metadata.namespace,
          "csi.storage.k8s.io/node-stage-secret-name": "rook-csi-rbd-node",
          "csi.storage.k8s.io/node-stage-secret-namespace": this.cephBlockPool.metadata.namespace,
          "csi.storage.k8s.io/fstype": "ext4",
        },
        reclaimPolicy: "Delete",
        allowVolumeExpansion: true,
        mountOptions: [],
      },
      opts,
    );

    this.registerOutputs();
  }
}
