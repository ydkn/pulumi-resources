import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as yaml from "js-yaml";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface CephArgs {
  namespace?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  kubeletDirPath?: pulumi.Input<string>;
}

export class Ceph extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new rook ceph operator resource.
   * @param name The name of the rook ceph operator resource.
   * @param args A bag of arguments to control the rook ceph operator creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: CephArgs, opts?: pulumi.ComponentResourceOptions) {
    super("rook:ceph", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://charts.rook.io/release" },
        chart: "rook-ceph",
        values: {
          fullnameOverride: name,
          unreachableNodeTolerationSeconds: 300,
          pspEnable: false,
          csi: {
            enableCephfsSnapshotter: false,
            enableRBDSnapshotter: false,
            provisionerReplicas: args.replicas,
            kubeletDirPath: args.kubeletDirPath,
            provisionerTolerations: [
              { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
              { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
            ],
            pluginTolerations: [
              { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
              { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
            ],
            nfs: { enabled: false },
            csiRBDProvisionerResource: yaml.dump([
              {
                name: "csi-provisioner",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-resizer",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-attacher",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-snapshotter",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-rbdplugin",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "liveness-prometheus",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
            ]),
            csiRBDPluginResource: yaml.dump([
              {
                name: "driver-registrar",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-rbdplugin",
                resource: { requests: { memory: "64Mi" }, limits: { memory: "96Mi" } },
              },
              {
                name: "liveness-prometheus",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
            ]),
            csiCephFSProvisionerResource: yaml.dump([
              {
                name: "csi-provisioner",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-resizer",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-attacher",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-cephfsplugin",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "liveness-prometheus",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
            ]),
            csiCephFSPluginResource: yaml.dump([
              {
                name: "driver-registrar",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-cephfsplugin",
                resource: { requests: { memory: "64Mi" }, limits: { memory: "96Mi" } },
              },
              {
                name: "liveness-prometheus",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
            ]),
            csiNFSProvisionerResource: yaml.dump([
              {
                name: "csi-provisioner",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-nfsplugin",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "liveness-prometheus",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
            ]),
            csiNFSPluginResource: yaml.dump([
              {
                name: "driver-registrar",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
              {
                name: "csi-nfsplugin",
                resource: { requests: { memory: "48Mi" }, limits: { memory: "64Mi" } },
              },
              {
                name: "liveness-prometheus",
                resource: { requests: { memory: "32Mi" }, limits: { memory: "48Mi" } },
              },
            ]),
            enableLiveness: true,
          },
          monitoring: { enabled: true },
          resources: {
            requests: { memory: "256Mi" },
            limits: { memory: "512Mi" },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
