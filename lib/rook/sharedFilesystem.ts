import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as sharedFilesystem from "../kubernetes/v1/sharedFilesystem.js";

interface SharedFilesystemProperties {
  clusterId: pulumi.Input<string>;
  fsType: pulumi.Input<string>;
  fsName: pulumi.Input<string>;
  pool: pulumi.Input<string>;
  csiProvisionerIdentity: pulumi.Input<string>;
  subvolumeName: pulumi.Input<string>;
  subvolumePath: pulumi.Input<string>;
  controllerExpandSecretRef: pulumi.Input<k8s.types.input.core.v1.SecretReference>;
  nodeStageSecretRef: pulumi.Input<k8s.types.input.core.v1.SecretReference>;
  volumeHandle: pulumi.Input<string>;
}

export const SharedFilesystemType = "cephfs";

export interface SharedFilesystemConfig extends sharedFilesystem.SharedFilesystemConfig, SharedFilesystemProperties {}

export interface SharedFilesystemArgs extends sharedFilesystem.SharedFilesystemArgs, SharedFilesystemProperties {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
}

export class SharedFilesystem extends ComponentResource {
  private static readonly driver = "rook.cephfs.csi.ceph.com";
  public static readonly defaultSize = "10Gi";
  public static readonly accessModes = ["ReadWriteMany"];

  public readonly persistentVolume: k8s.core.v1.PersistentVolume;
  public readonly persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim;

  /**
   * Create a new ceph shared filesystem resource.
   * @param name The name of the ceph shared filesystem resource.
   * @param args A bag of arguments to control the ceph shared filesystem creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: SharedFilesystemArgs, opts?: pulumi.ComponentResourceOptions) {
    super("rook:ceph:shared-filesystem", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    const persistentVolumeUUID = new random.RandomUuid(name, {}, emptyOpts);
    const volumeName = pulumi.all([persistentVolumeUUID.result]).apply(([uuid]) => `fs-${uuid}`);

    this.persistentVolume = new k8s.core.v1.PersistentVolume(
      name,
      {
        metadata: pulumi.all([args.metadata, volumeName]).apply(([metadata, volumeName]) => {
          return { ...metadata, name: volumeName, namespace: undefined };
        }),
        spec: {
          storageClassName: args.storageClassName,
          accessModes: SharedFilesystem.accessModes,
          capacity: { storage: args.storage ?? SharedFilesystem.defaultSize },
          persistentVolumeReclaimPolicy: "Retain",
          volumeMode: "Filesystem",
          csi: {
            driver: SharedFilesystem.driver,
            fsType: args.fsType,
            volumeAttributes: {
              clusterID: args.clusterId,
              fsName: args.fsName,
              pool: args.pool,
              subvolumeName: args.subvolumeName,
              subvolumePath: args.subvolumePath,
              rootPath: args.subvolumePath,
              staticVolume: "true",
              "storage.kubernetes.io/csiProvisionerIdentity": args.csiProvisionerIdentity,
            },
            controllerExpandSecretRef: args.controllerExpandSecretRef,
            nodeStageSecretRef: args.nodeStageSecretRef,
            volumeHandle: args.volumeHandle,
          },
        },
      },
      k8sOpts,
    );

    this.persistentVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: args.metadata,
        spec: {
          accessModes: SharedFilesystem.accessModes,
          storageClassName: args.storageClassName,
          volumeName: this.persistentVolume.metadata.name,
          resources: { requests: { storage: args.storage ?? SharedFilesystem.defaultSize } },
          volumeMode: "Filesystem",
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}

export interface SharedFilesystemFromConfigArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  storageClassName: pulumi.Input<string>;
  size: pulumi.Input<string>;
  controllerExpandSecretRef: pulumi.Input<k8s.types.input.core.v1.SecretReference>;
  nodeStageSecretRef: pulumi.Input<k8s.types.input.core.v1.SecretReference>;
  protect?: pulumi.Input<boolean>;
}

export function sharedFilesystemConfigFromConfig(
  name: string,
  args: pulumi.Input<SharedFilesystemFromConfigArgs>,
  opts?: pulumi.ComponentResourceOptions,
): pulumi.Output<SharedFilesystemConfig> {
  return pulumi.all([args]).apply(([args]) => {
    const filesystem = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: { ...args.metadata, name: args.metadata.name ?? name },
        spec: {
          accessModes: SharedFilesystem.accessModes,
          resources: { requests: { storage: args.size } },
          storageClassName: args.storageClassName,
          volumeMode: "Filesystem",
        },
      },
      { ...opts, protect: args.protect ?? opts?.protect },
    );

    return filesystem.spec.volumeName.apply((volumeName) => {
      const volume = k8s.core.v1.PersistentVolume.get(volumeName, volumeName, opts);

      return pulumi.secret({
        type: SharedFilesystemType,
        id: volume.metadata.name,
        storageClassName: volume.spec.storageClassName,
        storage: volume.spec.capacity.storage,
        clusterId: volume.spec.csi.volumeAttributes.clusterID,
        fsType: volume.spec.csi.fsType,
        fsName: volume.spec.csi.volumeAttributes.fsName,
        pool: volume.spec.csi.volumeAttributes.pool,
        csiProvisionerIdentity: volume.spec.csi.volumeAttributes["storage.kubernetes.io/csiProvisionerIdentity"],
        subvolumeName: volume.spec.csi.volumeAttributes.subvolumeName,
        subvolumePath: volume.spec.csi.volumeAttributes.subvolumePath,
        controllerExpandSecretRef: args.controllerExpandSecretRef,
        nodeStageSecretRef: args.nodeStageSecretRef,
        volumeHandle: volume.spec.csi.volumeHandle,
      });
    });
  });
}

export function sharedFilesystemArgsFromSharedFilesystemConfig(
  filesystemConfig: pulumi.Input<SharedFilesystemConfig>,
  metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>,
): SharedFilesystemArgs {
  const filesystemConfigApply = pulumi.all([filesystemConfig]);

  return {
    metadata: metadata ?? {},
    clusterId: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.clusterId),
    csiProvisionerIdentity: filesystemConfigApply.apply(
      ([filesystemConfig]) => filesystemConfig.csiProvisionerIdentity,
    ),
    fsType: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.fsType),
    fsName: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.fsName),
    pool: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.pool),
    controllerExpandSecretRef: filesystemConfigApply.apply(
      ([filesystemConfig]) => filesystemConfig.controllerExpandSecretRef,
    ),
    nodeStageSecretRef: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.nodeStageSecretRef),
    storageClassName: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.storageClassName!),
    storage: filesystemConfigApply.apply(
      ([filesystemConfig]) => filesystemConfig.storage ?? SharedFilesystem.defaultSize,
    ),
    subvolumeName: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.subvolumeName),
    subvolumePath: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.subvolumePath),
    volumeHandle: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.volumeHandle),
  };
}
