import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

interface StreamSpec {
  name: string;
  subjects: string[];
  servers: string[];
  discard?: string;
  maxAge?: string;
  maxBytes?: number;
  maxMsgSize?: number;
  maxMsgs?: number;
  storage?: string;
  retention?: string;
}

export interface StreamSpecArgs {
  name: pulumi.Input<string>;
  subjects: pulumi.Input<pulumi.Input<string>[]>;
  servers: pulumi.Input<pulumi.Input<string>[]>;
  discard?: pulumi.Input<string>;
  maxAge?: pulumi.Input<string>;
  maxBytes?: pulumi.Input<number>;
  maxMsgSize?: pulumi.Input<number>;
  maxMsgs?: pulumi.Input<number>;
  storage?: pulumi.Input<string>;
  retention?: pulumi.Input<string>;
}

export interface StreamArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<StreamSpecArgs>;
}

export class Stream extends k8s.apiextensions.CustomResource {
  public readonly spec: pulumi.Output<StreamSpec>;

  /**
   * Create a new NATS stream resource.
   * @param name The name of the NATS stream resource.
   * @param args A bag of arguments to control the NATS stream creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: StreamArgs, opts?: pulumi.CustomResourceOptions) {
    const spec = pulumi.all([args.spec]).apply(([spec]) => {
      if (spec.storage === undefined) {
        spec.storage = "file";
      }

      if (spec.retention === undefined) {
        spec.retention = "interest";
      }

      return spec;
    });

    super(
      name,
      {
        apiVersion: "jetstream.nats.io/v1beta2",
        kind: "Stream",
        metadata: args.metadata,
        spec,
      },
      opts,
    );

    this.spec = pulumi.output(spec);
  }
}
