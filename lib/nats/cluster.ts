import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { request } from "urllib";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { Dashboard } from "../grafana/index.js";

export interface VolumeArgs {
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface ClusterArgs {
  namespace?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  volume?: pulumi.Input<VolumeArgs>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
}

export class Cluster extends ComponentResource {
  private static crdBranch = "main";
  private static memLimitRegexp = /(\d+)(Mi|Gi)/;
  private static maxPayloadSize = 10 * 1024 * 1024; // 10MB

  private readonly nackCRD: k8s.yaml.ConfigFile;
  private readonly helmRelease: kubernetes.helm.v3.Release;
  private readonly nackHelmRelease: kubernetes.helm.v3.Release;
  private readonly grafanaDashboard: Dashboard;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new NATS cluster resource.
   * @param name The name of the NATS cluster resource.
   * @param args A bag of arguments to control the NATS cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("nats:cluster", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.resources === undefined) {
      args.resources = {
        requests: { memory: "128Mi" },
        limits: { memory: "256Mi" },
      };
    }

    const goMemoryLimit = pulumi.all([args.resources]).apply(([resources]) => {
      const memLimit = resources?.limits?.memory ?? "256Mi";
      const matches = Cluster.memLimitRegexp.exec(memLimit);

      if (matches === null || matches.length < 3) {
        return "";
      }

      let limit = parseInt(matches[1]);

      if (matches[2] === "Gi") {
        limit *= 1024;
      }

      limit = Math.floor(limit * 0.9);

      return `${limit}MiB`;
    });

    this.nackCRD = new k8s.yaml.ConfigFile(
      name,
      { file: ["https://raw.githubusercontent.com/nats-io/nack", Cluster.crdBranch, "deploy/crds.yml"].join("/") },
      opts,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://nats-io.github.io/k8s/helm/charts" },
        chart: "nats",
        values: {
          fullnameOverride: name,
          config: {
            jetstream: {
              enabled: true,
              memoryStore: {
                enabled: true,
              },
              fileStorage: {
                enabled: true,
                pvc: {
                  enabled: true,
                  size: pulumi.all([args.volume]).apply(([volume]) => volume?.size ?? "10Gi"),
                  storageClassName: pulumi.all([args.volume]).apply(([volume]) => volume?.storageClassName),
                },
              },
            },
            merge: {
              max_payload: Cluster.maxPayloadSize,
            },
          },
          container: {
            env: { GOMEMLIMIT: goMemoryLimit },
            merge: { resources: args.resources },
          },
          service: {
            merge: {
              metadata: {
                labels: args.serviceLabels ?? {},
                annotations: args.hostname
                  ? {
                      "external-dns.alpha.kubernetes.io/hostname": args.hostname,
                    }
                  : {},
              },
              spec: {
                type: args.serviceType ?? "ClusterIP",
                loadBalancerClass: args.serviceLoadBalancerClass,
                ipFamilyPolicy: "PreferDualStack",
              },
            },
          },
          natsBox: { enabled: false },
          promExporter: {
            enabled: true,
            podMonitor: { enabled: true },
          },
        },
      },
      opts,
    );

    this.nackHelmRelease = new kubernetes.helm.v3.Release(
      `${name}-nack`,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://nats-io.github.io/k8s/helm/charts" },
        chart: "nack",
        values: {
          nameOverride: `${name}-nack`,
          nats: {
            url: pulumi.interpolate`nats://${name}.${this.namespace}.svc:4222`,
          },
          resources: {
            requests: { memory: "48Mi" },
            limits: { memory: "64Mi" },
          },
        },
      },
      opts,
    );

    const dashboardPromise = new Promise<string | undefined>((resolve, reject) => {
      request(
        `https://raw.githubusercontent.com/nats-io/prometheus-nats-exporter/main/walkthrough/grafana-jetstream-dash-helm.json`,
        { dataType: "text", gzip: true },
      )
        .then((body) => {
          if (body.statusCode !== 200) {
            return reject(new Error(`failed to fetch dashboard json`));
          }

          return resolve((body.data as string).replaceAll("${DS__NATS-PROMETHEUS}", "prometheus"));
        })
        .catch(() => {
          return reject(new Error(`failed to fetch dashboard json`));
        });
    });

    this.grafanaDashboard = new Dashboard(
      `${name}-grafana-dashboard`,
      {
        metadata: { namespace: this.namespace, name: pulumi.interpolate`${name}-grafana-dashboard` },
        name: "nats-jetstream",
        data: pulumi.all([dashboardPromise]).apply(([promise]) => promise ?? ""),
      },
      opts,
    );

    this.registerOutputs();
  }
}
