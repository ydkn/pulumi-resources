import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

interface ConsumerSpec {
  streamName: string;
  servers: string[];
  durableName?: string;
  deliverSubject?: string;
  deliverGroup?: string;
}

export interface ConsumerSpecArgs {
  streamName: pulumi.Input<string>;
  servers: pulumi.Input<pulumi.Input<string>[]>;
  durableName?: pulumi.Input<string>;
  deliverSubject?: pulumi.Input<string>;
  deliverGroup?: pulumi.Input<string>;
}

export interface ConsumerArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<ConsumerSpecArgs>;
}

export class Consumer extends k8s.apiextensions.CustomResource {
  public readonly spec: pulumi.Output<ConsumerSpec>;

  /**
   * Create a new NATS consumer resource.
   * @param name The name of the NATS consumer resource.
   * @param args A bag of arguments to control the NATS consumer creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ConsumerArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        apiVersion: "jetstream.nats.io/v1beta2",
        kind: "Consumer",
        metadata: args.metadata,
        spec: args.spec,
      },
      opts,
    );

    this.spec = pulumi.output(args.spec);
  }
}
