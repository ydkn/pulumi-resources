import { OperatorArgs, Operator } from "./operator.js";
import { TenantArgs, Tenant } from "./tenant.js";

export { OperatorArgs, Operator, TenantArgs, Tenant };
