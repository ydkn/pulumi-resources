import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface OperatorArgs {
  namespace?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  clusterDomain?: pulumi.Input<string>;
}

export class Operator extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new MinIO operator resource.
   * @param name The name of the MinIO operator resource.
   * @param args A bag of arguments to control the MinIO operator creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: OperatorArgs, opts?: pulumi.ComponentResourceOptions) {
    super("minio:operator", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://operator.min.io/" },
        chart: "operator",
        values: {
          fullnameOverride: name,
          operator: {
            replicaCount: args.replicas,
            env: [
              { name: "WATCHED_NAMESPACE", value: "" },
              { name: "MINIO_CONSOLE_TLS_ENABLE", value: "false" },
              { name: "OPERATOR_STS_ENABLED", value: "on" },
              { name: "CLUSTER_DOMAIN", value: args.clusterDomain ?? "cluster.local" },
            ],
            resources: {
              requests: { cpu: "100m", memory: "192Mi" },
              limits: { cpu: "400m", memory: "256Mi" },
            },
          },
          console: {
            enabled: false,
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
