import * as pulumi from "@pulumi/pulumi";

export interface TLSConfigArgs {
  insecureSkipVerify?: pulumi.Input<boolean>;
}

export interface RelabelingArgs {
  action?: pulumi.Input<string>;
  regex?: pulumi.Input<string>;
  sourceLabels?: pulumi.Input<pulumi.Input<string>[]>;
  targetLabel?: pulumi.Input<string>;
  replacement?: pulumi.Input<string>;
}
