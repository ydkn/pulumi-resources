import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { RelabelingArgs, TLSConfigArgs } from "./monitor.js";

export interface PodMetricsEndpoint {
  targetPort?: pulumi.Input<number | string>;
  honorLabels?: pulumi.Input<boolean>;
  tlsConfig?: pulumi.Input<TLSConfigArgs>;
  metricRelabelings?: pulumi.Input<pulumi.Input<RelabelingArgs>[]>;
}

export interface PodMonitorSpec {
  selector?: pulumi.Input<k8s.types.input.meta.v1.LabelSelector>;
  podMetricsEndpoints?: pulumi.Input<pulumi.Input<PodMetricsEndpoint>[]>;
}

export interface PodMonitorArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<PodMonitorSpec>;
}

export class PodMonitor extends k8s.apiextensions.CustomResource {
  /**
   * Create a new pod monitor resource.
   * @param name The name of the pod monitor resource.
   * @param args A bag of arguments to control the pod monitor creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: PodMonitorArgs, opts?: pulumi.CustomResourceOptions) {
    super(name, { ...args, apiVersion: "monitoring.coreos.com/v1", kind: "PodMonitor" }, opts);
  }
}
