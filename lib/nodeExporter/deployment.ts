import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface NodeExporterArgs {
  namespace?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export class NodeExporter extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new node-exporter resource.
   * @param name The name of the node-exporter resource.
   * @param args A bag of arguments to control the node-exporter creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: NodeExporterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("node-exporter", name, args, opts);

    if (args.resources === undefined) {
      args.resources = { requests: { memory: "48Mi" }, limits: { memory: "64Mi" } };
    }

    opts = this.opts(k8s.Provider);

    this.namespace = pulumi.output(args.namespace ?? "kube-system");

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://prometheus-community.github.io/helm-charts" },
        chart: "prometheus-node-exporter",
        values: {
          fullnameOverride: name,
          prometheus: {
            monitor: {
              enabled: true,
              metricRelabelings: [
                {
                  action: "drop",
                  regex: "go_.*",
                  sourceLabels: ["__name__"],
                },
                {
                  action: "drop",
                  regex: "^tmpfs$",
                  sourceLabels: ["fstype"],
                },
              ],
            },
          },
          resources: args.resources,
          service: {
            ipDualStack: {
              ipFamilyPolicy: "PreferDualStack",
            },
          },
          tolerations: [{ operator: "Exists" }],
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
