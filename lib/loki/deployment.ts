import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { DataSource } from "../grafana/index.js";

export interface LokiArgs {
  namespace?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  storage?: pulumi.Input<string>;
  retention?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  grafanaDatasourceName?: pulumi.Input<string>;
}

export class Loki extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  private readonly grafanaDatasource: DataSource;
  public readonly namespace: pulumi.Output<string>;
  public readonly url: pulumi.Output<string>;

  /**
   * Create a new loki resource.
   * @param name The name of the loki resource.
   * @param args A bag of arguments to control the loki creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: LokiArgs, opts?: pulumi.ComponentResourceOptions) {
    super("loki", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.storage === undefined) {
      args.storage = "10Gi";
    }

    if (args.retention === undefined) {
      args.retention = "1440h";
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.url = pulumi.concat(
      "http://",
      pulumi.output(args.hostname ?? pulumi.concat(name, ".", this.namespace, ".", "svc")),
      ":3100",
    );

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://grafana.github.io/helm-charts" },
        chart: "loki",
        values: {
          fullnameOverride: name,
          deploymentMode: "SingleBinary",
          loki: {
            auth_enabled: false,
            limits_config: { reject_old_samples_max_age: "24h", retention_period: "720h" },
            commonConfig: { replication_factor: args.replicas ?? 1 },
            storage: { type: "filesystem" },
            compactor: { retention_enabled: true, delete_request_store: "filesystem" },
            configStorageType: "Secret",
            schemaConfig: {
              configs: [
                {
                  from: "2024-04-01",
                  store: "tsdb",
                  object_store: "filesystem",
                  schema: "v13",
                  index: { prefix: "loki_index_", period: "24h" },
                },
              ],
            },
            server: { log_level: "error" },
            ingester: { chunk_encoding: "snappy" },
            tracing: { enabled: false },
            querier: { max_concurrent: 3 },
          },
          singleBinary: {
            replicas: args.replicas ?? 1,
            persistence: { storageClass: args.storageClassName, size: args.storage },
            extraEnv: [{ name: "GOMEMLIMIT", value: "480MiB" }],
            resources: {
              requests: { memory: "384Mi" },
              limits: { memory: "512Mi" },
            },
          },
          backend: { replicas: 0 },
          read: { replicas: 0 },
          write: { replicas: 0 },
          ingester: { replicas: 0 },
          querier: { replicas: 0 },
          queryFrontend: { replicas: 0 },
          queryScheduler: { replicas: 0 },
          distributor: { replicas: 0 },
          compactor: { replicas: 0 },
          indexGateway: { replicas: 0 },
          bloomCompactor: { replicas: 0 },
          bloomGateway: { replicas: 0 },
          tableManager: { enabled: false },
          chunksCache: { enabled: false },
          resultsCache: { enabled: false },
          gateway: { enabled: false },
          lokiCanary: { enabled: false },
          memberlist: { service: { publishNotReadyAddresses: true } },
          test: { enabled: false },
          monitoring: {
            serviceMonitor: { enabled: false },
            selfMonitoring: {
              enabled: false,
              grafanaAgent: { installOperator: false },
            },
          },
        },
      },
      opts,
    );

    pulumi.all([args.serviceType, args.hostname]).apply(([serviceType, hostname]) => {
      if (serviceType === "LoadBalancer") {
        new kubernetes.v1.Service(
          name,
          {
            hostnames: args.hostname ? [args.hostname] : undefined,
            metadata: {
              namespace: this.namespace,
              name: pulumi.interpolate`${name}-lb`,
              labels: args.serviceLabels,
              annotations: hostname ? { "external-dns.alpha.kubernetes.io/hostname": hostname } : {},
            },
            spec: {
              loadBalancerClass: args.serviceLoadBalancerClass,
              type: args.serviceType,
              selector: {
                "app.kubernetes.io/component": "single-binary",
                "app.kubernetes.io/instance": "loki",
                "app.kubernetes.io/name": "loki",
              },
              ports: [
                { name: "http-metrics", protocol: "TCP", port: 3100, targetPort: "http-metrics" },
                { name: "grpc", protocol: "TCP", port: 9095, targetPort: "grpc" },
              ],
            },
          },
          { ...opts, dependsOn: [this.helmRelease] },
        );
      }
    });

    this.grafanaDatasource = new DataSource(
      `${name}-grafana-datasource`,
      {
        metadata: { namespace: this.namespace, name: pulumi.interpolate`${name}-grafana-datasource` },
        type: "loki",
        name: pulumi.all([args.grafanaDatasourceName]).apply(([dsName]) => dsName ?? "Loki"),
        url: pulumi.concat("http://", name, ".", this.namespace, ".", "svc", ":3100"),
      },
      opts,
    );

    this.registerOutputs();
  }
}
