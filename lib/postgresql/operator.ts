import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface OperatorArgs {
  namespace?: pulumi.Input<string>;
}

export class Operator extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new PostgreSQL operator resource.
   * @param name The name of PostgreSQL operator resource.
   * @param args A bag of arguments to control the PostgreSQL operator creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: OperatorArgs, opts?: pulumi.ComponentResourceOptions) {
    super("postgresql:operator", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://cloudnative-pg.github.io/charts" },
        chart: "cloudnative-pg",
        values: {
          fullnameOverride: name,
          config: { secret: true },
          monitoring: { podMonitorEnabled: true },
          resources: {
            requests: { cpu: "50m", memory: "256Mi" },
            limits: { cpu: "200m", memory: "320Mi" },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
