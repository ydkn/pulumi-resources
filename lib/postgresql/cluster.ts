import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface ServiceArgs {
  type?: pulumi.Input<string>;
  loadBalancerClass?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  labels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export interface RoleArgs {
  name: pulumi.Input<string>;
  login?: pulumi.Input<boolean>;
  createdb?: pulumi.Input<boolean>;
  createrole?: pulumi.Input<boolean>;
}

export interface ManagedArgs {
  roles?: pulumi.Input<pulumi.Input<RoleArgs>[]>;
}

export interface PostgresqlArgs {
  parameters?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export interface StorageArgs {
  size: pulumi.Input<string>;
  storageClass?: pulumi.Input<string>;
}

export interface ClusterSpecArgs {
  instances?: pulumi.Input<number>;
  storage?: pulumi.Input<StorageArgs>;
  postgresql?: pulumi.Input<PostgresqlArgs>;
  primaryUpdateStrategy?: pulumi.Input<string>;
  managed?: pulumi.Input<ManagedArgs>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export interface ClusterArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<ClusterSpecArgs>;
  service?: pulumi.Input<ServiceArgs>;
  protect?: boolean;
}

export class Cluster extends ComponentResource {
  private static readonly defaultStorageSize = "10Gi";

  private readonly postgresSecret: k8s.core.v1.Secret;
  private readonly cluster: k8s.apiextensions.CustomResource;
  private readonly service: k8s.core.v1.Service;
  public readonly hostnameInternal: pulumi.Output<string>;
  public readonly hostname: pulumi.Output<string>;
  public readonly postgresPassword: pulumi.Output<string>;

  /**
   * Create a new PostgreSQL cluster resource.
   * @param name The name of the PostgreSQL cluster resource.
   * @param args A bag of arguments to control the PostgreSQL cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("postgresql:cluster", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    this.postgresPassword = pulumi.secret(
      new random.RandomPassword(`${name}-postgres`, { length: 32, special: false }, emptyOpts).result,
    );

    this.postgresSecret = new k8s.core.v1.Secret(
      `${name}-postgres`,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return { ...metadata, name: pulumi.interpolate`${name}-postgres` };
        }),
        type: "kubernetes.io/basic-auth",
        stringData: {
          username: "postgres",
          password: this.postgresPassword,
        },
      },
      k8sOpts,
    );

    const roles = pulumi.all([args.spec ?? {}]).apply(([spec]) =>
      pulumi.all((spec.managed ?? ({} as { roles?: pulumi.Input<RoleArgs>[] })).roles ?? []).apply((roles) =>
        roles.map((role) => {
          const password = pulumi.secret(
            new random.RandomPassword(`${name}-role-${role.name}`, { length: 32, special: false }, emptyOpts).result,
          );

          const secret = new k8s.core.v1.Secret(
            `${name}-role-${role.name}`,
            {
              metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
                return { ...metadata, name: pulumi.interpolate`${name}-role-${role.name}` };
              }),
              type: "kubernetes.io/basic-auth",
              stringData: {
                username: role.name,
                password,
              },
            },
            k8sOpts,
          );

          return {
            ...role,
            login: role.login ?? true,
            createdb: role.createdb,
            passwordSecret: { name: secret.metadata.name },
            ensure: "present",
          };
        }),
      ),
    );

    this.cluster = new k8s.apiextensions.CustomResource(
      name,
      {
        apiVersion: "postgresql.cnpg.io/v1",
        kind: "Cluster",
        metadata: args.metadata,
        spec: pulumi.all([args.spec, roles]).apply(([spec, roles]) => {
          const s = {
            ...spec,
            instances: spec.instances ?? 1,
            primaryUpdateStrategy: spec.primaryUpdateStrategy ?? "unsupervised",
            storage: spec.storage ?? ({} as StorageArgs),
            enableSuperuserAccess: true,
            superuserSecret: { name: this.postgresSecret.metadata.name },
            affinity: { enablePodAntiAffinity: true, topologyKey: "kubernetes.io/hostname" },
            managed: { roles },
          };

          if (s.storage.size === undefined) {
            s.storage.size = Cluster.defaultStorageSize;
          }

          return s;
        }),
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: pulumi.all([args.metadata, args.service ?? {}]).apply(([metadata, service]) => {
          return { ...metadata, labels: { ...metadata.labels, ...service.labels } };
        }),
        spec: pulumi.all([args.metadata, args.service ?? {}]).apply(([metadata, service]) => {
          return {
            type: service.type ?? "ClusterIP",
            loadBalancerClass: service.loadBalancerClass,
            selector: {
              "cnpg.io/cluster": metadata.name ?? name,
              "cnpg.io/instanceRole": "primary",
              "cnpg.io/podRole": "instance",
            },
            ports: [{ name: "postgresql", protocol: "TCP", port: 5432, targetPort: "postgresql" }],
          };
        }),
        hostnames: pulumi.all([args.service ?? {}]).apply(([service]) => (service.hostname ? [service.hostname] : [])),
      },
      { ...k8sOpts, dependsOn: [this.cluster] },
    );

    this.hostnameInternal = pulumi.all([this.service.metadata]).apply(([m]) => `${m.name}.${m.namespace}.svc`);

    this.hostname = pulumi
      .all([args.service ?? {}, this.hostnameInternal])
      .apply(([service, hostnameInternal]) => service.hostname ?? hostnameInternal);

    this.registerOutputs();
  }
}
