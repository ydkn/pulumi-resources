import * as pulumi from "@pulumi/pulumi";
import * as hcloud from "@pulumi/hcloud";
import { ComponentResource } from "../common/index.js";
import * as node from "../node/index.js";
import * as netbird from "../netbird/index.js";

export interface NetbirdAgentArgs extends netbird.AgentArgs {
  clusterName: pulumi.Input<string>;
  nodes: pulumi.Input<pulumi.Input<node.Node>[]>;
  networkId: pulumi.Input<number>;
  routedSubnets?: pulumi.Input<pulumi.Input<string>[]>;
}

export class NetbirdAgent extends ComponentResource {
  private static readonly extraBlacklistInterfaces = ["enp7s0"];

  private readonly netbird: netbird.Agent;
  private readonly firewall: hcloud.Firewall;
  private readonly networkRoutes: pulumi.Output<hcloud.NetworkRoute[]>;

  /**
   * Create a new Netbird agent resource.
   * @param name The name of the Netbird agent resource.
   * @param args A bag of arguments to control the Netbird agent deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: NetbirdAgentArgs, opts?: pulumi.ComponentResourceOptions) {
    super("hcloud:netbird:agent", name, args, opts);

    const hcloudOpts = this.opts(hcloud.Provider);

    this.netbird = new netbird.Agent(
      name,
      { ...args, extraBlacklistInterfaces: NetbirdAgent.extraBlacklistInterfaces },
      { ...opts, parent: this },
    );

    const gatewayNodes = node.gatewayNodes(args.nodes);

    this.firewall = new hcloud.Firewall(
      name,
      {
        name,
        applyTos: [{ labelSelector: pulumi.interpolate`cluster=${args.clusterName}` }],
        rules: [
          {
            description: "wireguard",
            direction: "in",
            protocol: "udp",
            port: pulumi.interpolate`${this.netbird.interfacePort}`,
            sourceIps: ["0.0.0.0/0", "::/0"],
          },
        ],
      },
      hcloudOpts,
    );

    this.networkRoutes = pulumi.all([gatewayNodes, args.routedSubnets]).apply(([gatewayNodes, subnets]) =>
      pulumi.all(gatewayNodes ?? []).apply((gatewayNodes) => {
        const gatewayIPs = gatewayNodes.map((node) => node.privateIpv4Address!);

        if (gatewayIPs.length === 0) {
          return pulumi.output([] as hcloud.NetworkRoute[]);
        }

        return pulumi.all(subnets ?? []).apply((subnets) => {
          const peerIPRanges: string[] = [];

          for (const subnet of subnets) {
            if (/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/.exec(subnet)) {
              peerIPRanges.push(subnet);
            }
          }

          return peerIPRanges.map(
            (peerIPRange) =>
              new hcloud.NetworkRoute(
                `${name}-${peerIPRange}`,
                { networkId: args.networkId, destination: peerIPRange, gateway: gatewayIPs[0] },
                { ...hcloudOpts, deleteBeforeReplace: true },
              ),
          );
        });
      }),
    );

    this.registerOutputs();
  }
}
