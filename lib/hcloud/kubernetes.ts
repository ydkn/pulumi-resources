import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface KubernetesArgs {
  token: pulumi.Input<string>;
  network?: pulumi.Input<string>;
  podCIDR?: pulumi.Input<string>;
  kubeletDirPath?: pulumi.Input<string>;
}

export class Kubernetes extends ComponentResource {
  private static readonly namespace = "kube-system";

  private readonly secret: kubernetes.v1.Secret;
  private readonly cloudControllerManager: kubernetes.helm.v3.Release;
  private readonly csiDriver: kubernetes.helm.v3.Release;

  /**
   * Create a new Hetzner cloud kubernetes resource.
   * @param name The name of Hetzner cloud kubernetes resource.
   * @param args A bag of arguments to control the Hetzner cloud kubernetes creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: KubernetesArgs, opts?: pulumi.ComponentResourceOptions) {
    super("hcloud:kubernetes", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.secret = new kubernetes.v1.Secret(
      `${name}-hcloud-secret`,
      {
        metadata: { namespace: Kubernetes.namespace, name: "hcloud" },
        stringData: {
          token: args.token,
          network: args.network ?? "",
        },
      },
      opts,
    );

    this.cloudControllerManager = new kubernetes.helm.v3.Release(
      `${name}-hcloud-cloud-controller-manager`,
      {
        name: "hcloud-cloud-controller-manager",
        namespace: Kubernetes.namespace,
        repositoryOpts: { repo: "https://charts.hetzner.cloud" },
        chart: "hcloud-cloud-controller-manager",
        values: {
          nameOverride: "hcloud-cloud-controller-manager",
          args: {
            "secure-port": "11258",
            "configure-cloud-routes": "false",
          },
          env: {
            HCLOUD_TOKEN: { valueFrom: { secretKeyRef: { name: this.secret.metadata.name, key: "token" } } },
            HCLOUD_INSTANCES_ADDRESS_FAMILY: { value: "dualstack" },
          },
          networking: {
            enabled: args.network !== undefined && args.podCIDR !== undefined,
            clusterCIDR: args.podCIDR,
            network: {
              valueFrom: { secretKeyRef: { name: this.secret.metadata.name, key: "network" } },
            },
          },
          monitoring: {
            enabled: true,
            podMonitor: { enabled: true },
          },
          resources: {
            requests: { memory: "64Mi" },
            limits: { memory: "96Mi" },
          },
        },
      },
      opts,
    );

    this.csiDriver = new kubernetes.helm.v3.Release(
      `${name}-csi`,
      {
        name: "hcloud-csi-driver",
        namespace: Kubernetes.namespace,
        repositoryOpts: { repo: "https://charts.hetzner.cloud" },
        chart: "hcloud-csi",
        values: {
          fullnameOverride: "hcloud-csi",
          controller: {
            hcloudToken: {
              existingSecret: { name: this.secret.metadata.name, key: "token" },
            },
            tolerations: [
              { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
              { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
            ],
          },
          node: { kubeletDir: args.kubeletDirPath },
          metrics: { enabled: true, serviceMonitor: { enabled: true } },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
