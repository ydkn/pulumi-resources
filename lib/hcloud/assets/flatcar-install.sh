#!/bin/sh -e

DRIVE="${1}"
CONFIG="${2}"

apt-get update -y
apt-get install -y gawk

curl -sf -o /tmp/flatcar-install https://raw.githubusercontent.com/flatcar/init/flatcar-master/bin/flatcar-install
chmod +x /tmp/flatcar-install

/tmp/flatcar-install -d "${DRIVE}" -o hetzner -C stable -i "${CONFIG}"

asyncReboot() {
  sleep 2
  reboot
}

asyncReboot &
