#!/bin/sh -e

COREOS_INSTALLER_VERSION="0.22.1"

DRIVE="${1}"
CONFIG="${2}"

update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

apt-get update -y
apt-get install -y docker.io fuse-overlayfs

mkdir /tmp/coreos-installer
cd /tmp/coreos-installer
docker pull "quay.io/coreos/coreos-installer:v${COREOS_INSTALLER_VERSION}"
docker save "quay.io/coreos/coreos-installer:v${COREOS_INSTALLER_VERSION}" >image.tar
tar -xf image.tar
rm image.tar
for l in */layer.tar; do
  tar -xf "${l}"
done

./usr/sbin/coreos-installer install "${DRIVE}" -i "${CONFIG}"

asyncReboot() {
  sleep 2
  reboot
}

asyncReboot &
