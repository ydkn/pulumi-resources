import * as pulumi from "@pulumi/pulumi";
import * as hcloud from "@pulumi/hcloud";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as node from "../node/index.js";
import * as netbird from "../netbird/index.js";
import { DnsEndpoint, DnsEndpointsArgs } from "../externalDns/index.js";

export interface NetbirdManagementArgs extends netbird.ManagementArgs {
  nodes: pulumi.Input<pulumi.Input<node.Node>[]>;
}

export class NetbirdManagement extends ComponentResource {
  private readonly netbird: netbird.Management;
  private readonly firewall: hcloud.Firewall;
  private readonly dnsEndpoint: DnsEndpoint;
  public readonly namespace: pulumi.Output<string>;
  public readonly managementUrl: pulumi.Output<string>;
  public readonly adminUrl: pulumi.Output<string>;

  /**
   * Create a new Netbird management resource.
   * @param name The name of the Netbird management resource.
   * @param args A bag of arguments to control the Netbird management deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: NetbirdManagementArgs, opts?: pulumi.ComponentResourceOptions) {
    super("hcloud:netbird:mgmt", name, args, opts);

    const hcloudOpts = this.opts(hcloud.Provider);
    const k8sOpts = this.opts(k8s.Provider);

    const gatewayNodes = node.gatewayNodes(args.nodes);

    this.netbird = new netbird.Management(name, args, { ...opts, parent: this });
    this.namespace = this.netbird.namespace;
    this.managementUrl = this.netbird.managementUrl;
    this.adminUrl = this.netbird.adminUrl;

    this.firewall = new hcloud.Firewall(
      name,
      {
        name,
        applyTos: gatewayNodes.apply((nodes) =>
          pulumi.all(nodes).apply((nodes) =>
            nodes.map((node) => {
              return { server: parseInt(node.id!) };
            }),
          ),
        ),
        rules: [
          {
            description: "relay",
            direction: "in",
            protocol: "tcp",
            port: pulumi.interpolate`${netbird.Management.relayPort}`,
            sourceIps: ["0.0.0.0/0", "::/0"],
          },
        ],
      },
      hcloudOpts,
    );

    this.dnsEndpoint = new DnsEndpoint(
      name,
      {
        metadata: {
          name,
          namespace: this.netbird.namespace,
          labels: {
            "app.kubernetes.io/name": "netbird",
            "app.kubernetes.io/component": "management",
            "app.kubernetes.io/instance": name,
          },
        },
        spec: {
          endpoints: pulumi.all([args.relayHostname, gatewayNodes]).apply(([relayHostname, gatewayNodes]) =>
            pulumi.all(gatewayNodes).apply((nodes) => {
              const ipv4Endpoints: string[] = [];
              const ipv6Endpoints: string[] = [];

              for (const node of nodes) {
                if (node.ipv4Address !== undefined) {
                  ipv4Endpoints.push(node.ipv4Address);
                }

                if (node.ipv6Address !== undefined) {
                  ipv6Endpoints.push(node.ipv6Address);
                }
              }

              const endpoints: DnsEndpointsArgs[] = [];

              if (ipv4Endpoints.length > 0) {
                endpoints.push({ dnsName: relayHostname, recordType: "A", targets: ipv4Endpoints });
              }

              if (ipv6Endpoints.length > 0) {
                endpoints.push({ dnsName: relayHostname, recordType: "AAAA", targets: ipv6Endpoints });
              }

              return endpoints;
            }),
          ),
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}
