import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";

export interface HostPathProvisionerArgs {
  defaultClass?: pulumi.Input<boolean>;
  kubeletDirPath?: pulumi.Input<string>;
  path?: pulumi.Input<string>;
  createPath?: pulumi.Input<boolean>;
}

export class HostPathProvisioner extends ComponentResource {
  private static readonly branch = "main";
  public static readonly storageClassName = "host-path";

  private readonly yamls: k8s.yaml.ConfigFile[];
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new host path provisioner resource.
   * @param name The name of the host path provisioner resource.
   * @param args A bag of arguments to control the host path provisioner creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: HostPathProvisionerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("host-path-provisioner", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.defaultClass === undefined) {
      args.defaultClass = false;
    }

    if (args.kubeletDirPath === undefined) {
      args.kubeletDirPath = "/var/lib/kubelet";
    }

    if (args.path === undefined) {
      args.path = "/var/lib/hostpath-provisioner";
    }

    if (args.createPath === undefined) {
      args.createPath = true;
    }

    this.namespace = pulumi.output("kube-system");

    this.yamls = [
      this.yamlResource(
        `${name}-driver`,
        "csi-driver-hostpath-provisioner.yaml",
        args.defaultClass,
        args.kubeletDirPath,
        args.path,
        args.createPath,
        opts,
      ),
      this.yamlResource(
        `${name}-external-health-monitor-rbac`,
        "external-health-monitor-rbac.yaml",
        args.defaultClass,
        args.kubeletDirPath,
        args.path,
        args.createPath,
        opts,
      ),
      this.yamlResource(
        `${name}-external-provisioner-rbac`,
        "external-provisioner-rbac.yaml",
        args.defaultClass,
        args.kubeletDirPath,
        args.path,
        args.createPath,
        opts,
      ),
      this.yamlResource(
        `${name}-provisioner`,
        "csi-kubevirt-hostpath-provisioner.yaml",
        args.defaultClass,
        args.kubeletDirPath,
        args.path,
        args.createPath,
        opts,
      ),
      this.yamlResource(
        `${name}-storage-class`,
        "csi-sc.yaml",
        args.defaultClass,
        args.kubeletDirPath,
        args.path,
        args.createPath,
        opts,
      ),
    ];

    this.registerOutputs();
  }

  private yamlResource(
    name: string,
    file: string,
    defaultClass: pulumi.Input<boolean>,
    kubeletDirPath: pulumi.Input<string>,
    path: pulumi.Input<string>,
    createPath: pulumi.Input<boolean>,
    opts?: pulumi.ComponentResourceOptions,
  ): k8s.yaml.ConfigFile {
    return new k8s.yaml.ConfigFile(
      name,
      {
        file: [
          "https://raw.githubusercontent.com/kubevirt/hostpath-provisioner",
          HostPathProvisioner.branch,
          "deploy/csi",
          file,
        ].join("/"),
        transformations: [
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (o: any) => {
            /* eslint-disable @typescript-eslint/no-unsafe-member-access, security/detect-object-injection */
            if (
              o.metadata !== undefined &&
              (o.metadata.namespace !== undefined ||
                // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
                ["DaemonSet", "ServiceAccount", "Role", "RoleBinding"].includes(o.kind))
            ) {
              o.metadata.namespace = this.namespace;
            }

            if (o.subjects !== undefined) {
              for (const subject of o.subjects) {
                if (subject.namespace !== undefined) {
                  subject.namespace = this.namespace;
                }
              }
            }

            if (o.kind === "CSIDriver" && o.spec !== undefined) {
              o.spec.volumeLifecycleModes = ["Ephemeral", "Persistent"];
              o.spec.fsGroupPolicy = "File";
            }

            if (o.kind === "DaemonSet" && o.metadata !== undefined && o.spec !== undefined) {
              o.metadata.name = "hostpath-csi";

              if (
                o.spec.template !== undefined &&
                o.spec.template.spec !== undefined &&
                o.spec.template.spec.containers !== undefined &&
                o.spec.template.spec.volumes !== undefined
              ) {
                for (const container of o.spec.template.spec.containers) {
                  container.imagePullPolicy = "Always";

                  if (container.env !== undefined) {
                    for (const env of container.env) {
                      if (env.name === "PV_DIR") {
                        env.value = path;
                      }
                    }
                  }

                  if (container.volumeMounts !== undefined) {
                    for (const volumeMount of container.volumeMounts) {
                      switch (volumeMount.name) {
                        case "mountpoint-dir":
                          volumeMount.mountPath = pulumi.interpolate`${kubeletDirPath}/pods`;
                          break;
                        case "plugins-dir":
                          volumeMount.mountPath = pulumi.interpolate`${kubeletDirPath}/plugins`;
                          break;
                      }
                    }
                  }

                  if (container.args !== undefined) {
                    for (const i in container.args) {
                      switch (
                        container.args[i].split("=") // eslint-disable-line @typescript-eslint/no-unsafe-call
                      ) {
                        case "--datadir":
                          container.args[i] = JSON.stringify([
                            { name: HostPathProvisioner.storageClassName, path: "/csi-data-dir" },
                          ]);
                          break;
                        case "--kubelet-registration-path":
                          container.args[i] = pulumi.interpolate`${kubeletDirPath}/plugins/csi-hostpath/csi.sock`;
                          break;
                      }
                    }
                  }
                }

                for (const volume of o.spec.template.spec.volumes) {
                  switch (volume.name) {
                    case "socket-dir":
                      volume.hostPath.path = pulumi.interpolate`${kubeletDirPath}/plugins/csi-hostpath`;
                      break;
                    case "mountpoint-dir":
                      volume.hostPath.path = pulumi.interpolate`${kubeletDirPath}/pods`;
                      break;
                    case "registration-dir":
                      volume.hostPath.path = pulumi.interpolate`${kubeletDirPath}/plugins_registry`;
                      break;
                    case "plugins-dir":
                      volume.hostPath.path = pulumi.interpolate`${kubeletDirPath}/plugins`;
                      break;
                    case "csi-data-dir":
                      volume.hostPath.path = path;
                      volume.hostPath.type = createPath ? "DirectoryOrCreate" : "Directory";
                      break;
                  }
                }
              }
            }

            if (o.kind === "StorageClass" && o.metadata !== undefined) {
              o.metadata.name = HostPathProvisioner.storageClassName;

              if (o.metadata.annotations === undefined) {
                o.metadata.annotations = {};
              }

              o.metadata.annotations["storageclass.kubernetes.io/is-default-class"] = defaultClass ? "true" : "false";

              o.reclaimPolicy = "Delete";

              if (o.parameters === undefined) {
                o.parameters = {};
              }
              o.parameters.storagePool = HostPathProvisioner.storageClassName;
            }

            return o; // eslint-disable-line @typescript-eslint/no-unsafe-return
          },
        ],
      },
      opts,
    );
  }
}
