import * as pulumi from "@pulumi/pulumi";

export interface IgnitionUserArgs {
  name: pulumi.Input<string>;
  groups?: pulumi.Input<pulumi.Input<string>[]>;
  sshAuthorizedKeys?: pulumi.Input<pulumi.Input<string>[]>;
  passwordHash?: pulumi.Input<string>;
}

export interface IgnitionPartitionArgs {
  label: pulumi.Input<string>;
  size: pulumi.Input<number>;
  start?: pulumi.Input<number>;
  number?: pulumi.Input<number>;
  resize?: pulumi.Input<boolean>;
}

export interface IgnitionDiskArgs {
  device: pulumi.Input<string>;
  partitions: pulumi.Input<pulumi.Input<IgnitionPartitionArgs>[]>;
  wipeTable?: pulumi.Input<boolean>;
}

export interface IgnitionFilesystemArgs {
  device: pulumi.Input<string>;
  path?: pulumi.Input<string>;
  format: pulumi.Input<string>;
  label?: pulumi.Input<string>;
}

export interface IgnitionFileArgs {
  path: pulumi.Input<string>;
  contents: pulumi.Input<string>;
  mode?: pulumi.Input<number>;
  overwrite?: pulumi.Input<boolean>;
}

export interface IgnitionLinkArgs {
  path: pulumi.Input<string>;
  target: pulumi.Input<string>;
}

export interface IgnitionSystemdUnitArgs {
  name: pulumi.Input<string>;
  enabled?: pulumi.Input<boolean>;
  mask?: pulumi.Input<boolean>;
  contents?: pulumi.Input<string>;
}

export interface IgnitionArgs {
  users?: pulumi.Input<pulumi.Input<IgnitionUserArgs>[]>;
  disks?: pulumi.Input<pulumi.Input<IgnitionDiskArgs>[]>;
  filesystems?: pulumi.Input<pulumi.Input<IgnitionFilesystemArgs>[]>;
  files?: pulumi.Input<pulumi.Input<IgnitionFileArgs>[]>;
  links?: pulumi.Input<pulumi.Input<IgnitionLinkArgs>[]>;
  systemdUnits?: pulumi.Input<pulumi.Input<IgnitionSystemdUnitArgs>[]>;
}
