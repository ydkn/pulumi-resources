import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import { DnsEndpointsArgs, DnsEndpoint } from "../externalDns/index.js";
import { Node } from "./node.js";

export interface KubernetesArgs {
  namespace?: pulumi.Input<string>;
  nodes: pulumi.Input<pulumi.Input<Node>[]>;
  clusterName: pulumi.Input<string>;
  domain: pulumi.Input<string>;
  ipType?: pulumi.Input<string>;
}

export class Kubernetes extends ComponentResource {
  private readonly api: pulumi.Output<DnsEndpoint>;
  private readonly nodes: pulumi.Output<DnsEndpoint[]>;

  /**
   * Create a new node dns endpoints resource.
   * @param name The name of the node dns endpoints resource.
   * @param args A bag of arguments to control the node dns endpoints creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: KubernetesArgs, opts?: pulumi.ComponentResourceOptions) {
    super("node:kubernetes", name, args, opts);

    opts = this.opts(k8s.Provider);

    const namespace = args.namespace ?? "kube-system";
    const ipType = args.ipType ?? "private";

    this.api = pulumi.all([args.nodes, args.domain, ipType]).apply(
      ([nodes, domain, ipType]) =>
        new DnsEndpoint(
          `${name}-api`,
          {
            metadata: { namespace, name: `${name}-api` },
            spec: {
              endpoints: pulumi.all(nodes).apply((nodes) => {
                const dnsName = pulumi.interpolate`api.${domain}`;
                const ipv4Endpoints: pulumi.Input<string>[] = [];
                const ipv6Endpoints: pulumi.Input<string>[] = [];
                const endpoints: DnsEndpointsArgs[] = [];

                for (const node of nodes) {
                  if ((node.roles ?? []).includes("controller")) {
                    const nodeIPs = this.nodeIPs(node, ipType);

                    ipv4Endpoints.push(...nodeIPs.ipv4Adresses);
                    ipv6Endpoints.push(...nodeIPs.ipv6Addresses);
                  }
                }

                if (ipv4Endpoints.length > 0) {
                  endpoints.push({ dnsName, recordType: "A", targets: ipv4Endpoints });
                }

                if (ipv6Endpoints.length > 0) {
                  endpoints.push({ dnsName, recordType: "AAAA", targets: ipv6Endpoints });
                }

                return endpoints;
              }),
            },
          },
          opts,
        ),
    );

    this.nodes = pulumi
      .all([args.nodes, args.domain, args.clusterName, ipType])
      .apply(([nodes, domain, clusterName, ipType]) =>
        nodes.map((node) => {
          const dnsName = pulumi.interpolate`${node.hostname.replace(`${clusterName}-`, "")}.${domain}`;
          const endpoints: DnsEndpointsArgs[] = [];

          const nodeIPs = this.nodeIPs(node, ipType);

          if (nodeIPs.ipv4Adresses.length > 0) {
            endpoints.push({ dnsName, recordType: "A", targets: nodeIPs.ipv4Adresses });
          }

          if (nodeIPs.ipv6Addresses.length > 0) {
            endpoints.push({ dnsName, recordType: "AAAA", targets: nodeIPs.ipv6Addresses });
          }

          return new DnsEndpoint(
            `${name}-${node.hostname}`,
            {
              metadata: { namespace, name: `${name}-${node.hostname}` },
              spec: { endpoints },
            },
            opts,
          );
        }),
      );

    this.registerOutputs();
  }

  private nodeIPs(
    node: Node,
    ipType: string,
  ): { ipv4Adresses: pulumi.Input<string>[]; ipv6Addresses: pulumi.Input<string>[] } {
    const ipv4Adresses: pulumi.Input<string>[] = [];
    const ipv6Addresses: pulumi.Input<string>[] = [];

    if (ipType === "private") {
      if (node.privateIpv4Address !== undefined) {
        ipv4Adresses.push(node.privateIpv4Address);
      }

      if (node.privateIpv6Address !== undefined) {
        ipv6Addresses.push(node.privateIpv6Address);
      }
    } else {
      if (node.ipv4Address !== undefined) {
        ipv4Adresses.push(node.ipv4Address);
      }

      if (node.ipv6Address !== undefined) {
        ipv6Addresses.push(node.ipv6Address);
      }
    }

    return { ipv4Adresses, ipv6Addresses };
  }
}
