import * as pulumi from "@pulumi/pulumi";

export interface Node {
  id?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  roles?: pulumi.Input<pulumi.Input<string>[]>;
  ipv4Address?: pulumi.Input<string>;
  privateIpv4Address?: pulumi.Input<string>;
  ipv6Address?: pulumi.Input<string>;
  privateIpv6Address?: pulumi.Input<string>;
  labels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  taints?: pulumi.Input<
    pulumi.Input<{ effect: pulumi.Input<string>; key: pulumi.Input<string>; value: pulumi.Input<string> }>[]
  >;
  operatingSystem?: pulumi.Input<string>;
  username?: pulumi.Input<string>;
  privateKey?: pulumi.Input<string>;
  disabled?: pulumi.Input<boolean>;
  ignitionConfig?: pulumi.Input<string>;
}

export function filterNodes(
  nodes: pulumi.Input<pulumi.Input<Node>[]>,
  selector: pulumi.Input<{ [key: string]: pulumi.Input<string> }>,
): pulumi.Output<pulumi.Output<Node>[]> {
  return pulumi.all([nodes, selector]).apply(([nodes, selector]) =>
    pulumi.all(selector).apply((selector) =>
      pulumi.all(nodes ?? []).apply((nodes) => {
        return nodes
          .filter((n) => {
            if (n.id === undefined || n.labels === undefined) {
              return false;
            }

            for (const [labelName, labelValue] of Object.entries(selector)) {
              // eslint-disable-next-line security/detect-object-injection
              if (n.labels[labelName] === undefined || n.labels[labelName] !== labelValue) {
                return false;
              }
            }

            return true;
          })
          .map((n) => pulumi.output(n));
      }),
    ),
  );
}
