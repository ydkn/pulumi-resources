import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface ProxyArgs {
  namespace?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
}

export class Proxy extends ComponentResource {
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly configmap: k8s.core.v1.ConfigMap;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: kubernetes.v1.Service;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new proxy resource.
   * @param name The name of the proxy resource.
   * @param args A bag of arguments to control the proxy creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ProxyArgs, opts?: pulumi.ComponentResourceOptions) {
    super("proxy", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "proxy",
        "app.kubernetes.io/component": "proxy",
        "app.kubernetes.io/instance": name,
      },
    };

    this.configmap = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata,
        data: {
          config: [
            "user-manual /usr/share/doc/privoxy/user-manual/",
            "confdir /etc/privoxy",
            "logdir /var/log/privoxy",
            "logfile privoxy.log",
            "debug 0",
            "listen-address :8118",
            "toggle 0",
            "enable-remote-toggle 0",
            "enable-remote-http-toggle 0",
            "enable-edit-actions 0",
            "enforce-blocks 0",
            "buffer-limit 4096",
            "enable-proxy-authentication-forwarding 0",
            "forwarded-connect-retries 0",
            "accept-intercepted-requests 0",
            "allow-cgi-request-crunching 0",
            "split-large-forms 0",
            "keep-alive-timeout 10",
            "tolerate-pipelining 1",
            "socket-timeout 300",
            "max-client-connections 256",
          ].join("\n"),
        },
      },
      opts,
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          replicas: args.replicas,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              containers: [
                {
                  name: "privoxy",
                  image: "sebastianalbers/privoxy:latest",
                  imagePullPolicy: "Always",
                  ports: [{ name: "proxy", protocol: "TCP", containerPort: 8118 }],
                  volumeMounts: [{ name: "config", mountPath: "/etc/privoxy" }],
                  resources: {
                    requests: { memory: "48Mi" },
                    limits: { memory: "64Mi" },
                  },
                },
              ],
              volumes: [{ name: "config", configMap: { name: this.configmap.metadata.name } }],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      weight: 100,
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      opts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      opts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        hostnames: args.hostname ? [args.hostname] : undefined,
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          type: args.serviceType ?? "ClusterIP",
          selector: metadata.labels,
          ports: [{ name: "proxy", protocol: "TCP", port: 8118 }],
        },
      },
      { ...opts, dependsOn: [this.deployment] },
    );

    this.registerOutputs();
  }
}
