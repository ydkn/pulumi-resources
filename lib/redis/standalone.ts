import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";

export interface StandaloneArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  version?: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  storage?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  protect?: boolean;
}

export class Standalone extends ComponentResource {
  private static readonly defaultVersion = "7.2.7";
  private static readonly defaultExporterVersion = "1.48.0";

  private readonly config: k8s.core.v1.ConfigMap;
  private readonly standalone: k8s.apiextensions.CustomResource;
  public readonly hostnameInternal: pulumi.Output<string>;

  /**
   * Create a new Redis standalone resource.
   * @param name The name of the Redis standalone resource.
   * @param args A bag of arguments to control the Redis standalone creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: StandaloneArgs, opts?: pulumi.ComponentResourceOptions) {
    super("redis:standalone", name, args, opts);

    if (args.version === undefined) {
      args.version = Standalone.defaultVersion;
    }

    if (args.resources === undefined) {
      args.resources = {
        requests: { cpu: "200m", memory: "512Mi" },
        limits: { cpu: "1", memory: "640Mi" },
      };
    }

    const k8sOpts = this.opts(k8s.Provider);

    this.config = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return { ...metadata, name: `${metadata.name ?? name}-config` };
        }),
        data: {
          "defaults.conf": [
            "stop-writes-on-bgsave-error no",
            "rdbcompression yes",
            "rdbchecksum yes",
            "rdb-del-sync-files yes",
            "appendonly no",
          ].join("\n"),
        },
      },
      k8sOpts,
    );

    this.standalone = new k8s.apiextensions.CustomResource(
      name,
      {
        apiVersion: "redis.redis.opstreelabs.in/v1beta2",
        kind: "Redis",
        metadata: args.metadata,
        spec: {
          securityContext: {
            runAsUser: 1000,
          },
          podSecurityContext: {
            runAsUser: 1000,
            fsGroup: 1000,
          },
          kubernetesConfig: {
            image: pulumi.all([args.version]).apply(([version]) => `quay.io/opstree/redis:v${version}`),
            imagePullPolicy: "Always",
            resources: args.resources,
          },
          redisConfig: {
            additionalRedisConfig: this.config.metadata.name,
          },
          redisExporter: {
            enabled: true,
            image: `quay.io/opstree/redis-exporter:v${Standalone.defaultExporterVersion}`,
            imagePullPolicy: "Always",
            resources: {
              requests: { cpu: "100m", memory: "64Mi" },
              limits: { cpu: "200m", memory: "96Mi" },
            },
          },
          storage: args.storage
            ? {
                volumeClaimTemplate: {
                  spec: {
                    accessModes: ["ReadWriteOnce"],
                    storageClassName: args.storageClassName,
                    resources: {
                      requests: {
                        storage: args.storage,
                      },
                    },
                  },
                },
              }
            : undefined,
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.hostnameInternal = pulumi
      .all([this.standalone.metadata])
      .apply(([metadata]) => `${metadata.name}.${metadata.namespace}.svc`);

    this.registerOutputs();
  }
}
