import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface MetalLBArgs {
  namespace?: pulumi.Input<string>;
  loadBalancerClass?: pulumi.Input<string>;
}

export class MetalLB extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new MetalLB resource.
   * @param name The name of MetalLB resource.
   * @param args A bag of arguments to control the MetalLB creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: MetalLBArgs, opts?: pulumi.ComponentResourceOptions) {
    super("metallb", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://metallb.github.io/metallb" },
        chart: "metallb",
        values: {
          fullnameOverride: name,
          loadBalancerClass: args.loadBalancerClass ?? "",
          prometheus: {
            serviceAccount: "default",
            namespace: this.namespace,
            serviceMonitor: {
              enabled: true,
              metricRelabelings: [
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: ["go_.*"].join("|"),
                },
              ],
            },
            prometheusRule: { enabled: false },
          },
          controller: {
            affinity: {
              podAntiAffinity: {
                preferredDuringSchedulingIgnoredDuringExecution: [
                  {
                    weight: 100,
                    podAffinityTerm: {
                      labelSelector: {
                        matchExpressions: [
                          { key: "app.kubernetes.io/name", operator: "In", values: ["metallb"] },
                          { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                          { key: "app.kubernetes.io/component", operator: "In", values: ["controller"] },
                        ],
                      },
                      topologyKey: "kubernetes.io/hostname",
                    },
                  },
                ],
              },
            },
            tolerations: [
              { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
              { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
            ],
            resources: {
              requests: { memory: "64Mi" },
              limits: { memory: "96Mi" },
            },
          },
          speaker: {
            tolerateMaster: false,
            frr: { enabled: false },
            resources: {
              requests: { memory: "48Mi" },
              limits: { memory: "64Mi" },
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
