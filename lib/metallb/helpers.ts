import * as pulumi from "@pulumi/pulumi";
import { IPAddressPoolSpecArgs, IPAddressPool } from "./ipAddressPool.js";
import { L2AdvertisementSpecArgs, L2Advertisement } from "./l2Advertisement.js";

export interface IPAddressPoolAndL2AdvertisementArgs extends IPAddressPoolSpecArgs, L2AdvertisementSpecArgs {}

export type IPAddressPoolAndL2AdvertisementConfig = pulumi.Input<{
  [key: string]: pulumi.Input<IPAddressPoolAndL2AdvertisementArgs>;
}>;

export const ipAdressPoolAndL2AdvertisementFromConfig = (
  config?: IPAddressPoolAndL2AdvertisementConfig,
  namespace?: pulumi.Input<string>,
  opts?: pulumi.CustomResourceOptions,
): pulumi.Output<{ [key: string]: pulumi.Output<{ [key: string]: pulumi.Output<string> }> }> => {
  return pulumi.all([config ?? {}]).apply(([config]) => {
    const serviceLabels: { [key: string]: pulumi.Output<{ [key: string]: pulumi.Output<string> }> } = {};

    for (const addressPoolName in config) {
      const addressPool = config[addressPoolName]; // eslint-disable-line security/detect-object-injection

      const metalLBAddressPool = new IPAddressPool(
        addressPoolName,
        {
          metadata: { name: addressPoolName, namespace },
          spec: pulumi.all([addressPool]).apply(([addressPool]) => {
            return {
              addresses: addressPool.addresses,
              autoAssign: addressPool.autoAssign,
              avoidBuggyIPs: addressPool.avoidBuggyIPs,
              serviceAllocation: addressPool.serviceAllocation,
            };
          }),
        },
        opts,
      );

      new L2Advertisement(
        addressPoolName,
        {
          metadata: { name: addressPoolName, namespace },
          spec: pulumi.all([addressPool]).apply(([addressPool]) => {
            return {
              ipAddressPools: [metalLBAddressPool.metadata.name],
              interfaces: addressPool.interfaces,
            };
          }),
        },
        opts,
      );

      if (
        addressPool.serviceAllocation?.serviceSelectors !== undefined &&
        addressPool.serviceAllocation?.serviceSelectors.length > 0 &&
        addressPool.serviceAllocation?.serviceSelectors[0].matchLabels !== undefined
      ) {
        // eslint-disable-next-line security/detect-object-injection
        serviceLabels[addressPoolName] = pulumi
          .all([addressPool.serviceAllocation?.serviceSelectors[0].matchLabels])
          .apply(([matchLabels]) => {
            const labels: { [key: string]: pulumi.Output<string> } = {};

            for (const key in matchLabels) {
              // eslint-disable-next-line security/detect-object-injection
              labels[key] = pulumi.output(matchLabels[key]);
            }

            return labels;
          });
      }
    }

    return serviceLabels;
  });
};
