import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface ServiceAllocationArgs {
  serviceSelectors?: pulumi.Input<pulumi.Input<k8s.types.input.meta.v1.LabelSelector>[]>;
}

export interface IPAddressPoolSpecArgs {
  addresses: pulumi.Input<pulumi.Input<string>[]>;
  autoAssign?: pulumi.Input<boolean>;
  avoidBuggyIPs?: pulumi.Input<boolean>;
  serviceAllocation?: pulumi.Input<ServiceAllocationArgs>;
}

export interface IPAddressPoolArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<IPAddressPoolSpecArgs>;
}

export class IPAddressPool extends k8s.apiextensions.CustomResource {
  /**
   * Create a new IP address pool resource.
   * @param name The name of the IP address pool resource.
   * @param args A bag of arguments to control the IP address pool creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: IPAddressPoolArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        apiVersion: "metallb.io/v1beta1",
        kind: "IPAddressPool",
        metadata: args.metadata,
        spec: args.spec,
      },
      opts,
    );
  }
}
