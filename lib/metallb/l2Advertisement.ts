import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface L2AdvertisementSpecArgs {
  ipAddressPoolSelectors?: pulumi.Input<pulumi.Input<k8s.types.input.meta.v1.LabelSelector>[]>;
  ipAddressPools?: pulumi.Input<pulumi.Input<string>[]>;
  nodeSelectors?: pulumi.Input<pulumi.Input<k8s.types.input.meta.v1.LabelSelector>[]>;
  interfaces?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface L2AdvertisementArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<L2AdvertisementSpecArgs>;
}

export class L2Advertisement extends k8s.apiextensions.CustomResource {
  /**
   * Create a new L2 advertisement resource.
   * @param name The name of the L2 advertisement resource.
   * @param args A bag of arguments to control the L2 advertisement creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: L2AdvertisementArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        apiVersion: "metallb.io/v1beta1",
        kind: "L2Advertisement",
        metadata: args.metadata,
        spec: args.spec,
      },
      opts,
    );
  }
}
