import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import { AvahiService } from "./avahiService.js";

export interface CupsPrinterArgs {
  name: pulumi.Input<string>;
  description: pulumi.Input<string>;
}

export interface CupsArgs {
  namespace?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  avahiLabelName?: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  printers: pulumi.Input<{ [key: string]: pulumi.Input<CupsPrinterArgs> }>;
  protect?: boolean;
}

export class Cups extends ComponentResource {
  private readonly adminSecret: k8s.core.v1.Secret;
  private readonly volumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly service: kubernetes.v1.Service;
  private readonly ingress: kubernetes.v1.SecureIngress;
  private readonly avahiServices?: pulumi.Output<AvahiService[]>;
  public readonly adminPassword: pulumi.Output<string>;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new CUPS resource.
   * @param name The name of the CUPS resource.
   * @param args A bag of arguments to control the CUPS deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: CupsArgs, opts?: pulumi.ComponentResourceOptions) {
    super("cups", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    this.adminPassword = pulumi.secret(
      new random.RandomPassword(`${name}-admin-password`, { length: 24, special: true }, emptyOpts).result,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "cups",
        "app.kubernetes.io/component": "cups",
        "app.kubernetes.io/instance": name,
      },
    };

    this.adminSecret = new k8s.core.v1.Secret(
      name,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-admin` },
        stringData: {
          password: this.adminPassword,
        },
      },
      k8sOpts,
    );

    const volumeClaimName = pulumi.interpolate`${name}-data`;

    this.volumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: { ...metadata, name: volumeClaimName },
        spec: {
          storageClassName: args.storageClassName,
          accessModes: ["ReadWriteOnce"],
          resources: { requests: { storage: "1Gi" } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              nodeSelector: args.nodeSelector,
              hostNetwork: true,
              containers: [
                {
                  name: "cups",
                  image: "docker.io/ydkn/cups:latest",
                  imagePullPolicy: "Always",
                  env: [
                    {
                      name: "ADMIN_PASSWORD",
                      valueFrom: { secretKeyRef: { name: this.adminSecret.metadata.name, key: "password" } },
                    },
                  ],
                  volumeMounts: [{ name: "data", mountPath: "/etc/cups" }],
                  ports: [{ name: "cups", containerPort: 631, hostPort: 631 }],
                  resources: {
                    requests: { memory: "128Mi" },
                    limits: { memory: "192Mi" },
                  },
                },
              ],
              volumes: [{ name: "data", persistentVolumeClaim: { claimName: volumeClaimName } }],
            },
          },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata,
        spec: {
          selector: metadata.labels,
          ports: [{ name: "https", port: 631, targetPort: "cups" }],
        },
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.ingress = new kubernetes.v1.SecureIngress(
      name,
      {
        metadata,
        spec: {
          ingressClassName: args.ingressClassName,
          rules: [
            {
              host: args.hostname,
              http: {
                paths: [
                  {
                    path: "/",
                    pathType: "Prefix",
                    backend: {
                      service: { name: this.service.metadata.name, port: { name: this.service.spec.ports[0].name } },
                    },
                  },
                ],
              },
            },
          ],
        },
      },
      k8sOpts,
    );

    if (args.avahiLabelName !== undefined) {
      this.avahiServices = pulumi.all([args.printers, args.avahiLabelName]).apply(([printers, avahiLabelName]) => {
        const avahiServices: AvahiService[] = [];

        for (const printerName in printers) {
          // eslint-disable-next-line security/detect-object-injection
          const printer = printers[printerName];

          avahiServices.push(
            new AvahiService(
              `${name}-${printerName}`,
              {
                metadata: { ...metadata, name: `${name}-${printerName}` },
                labelName: avahiLabelName,
                name: printer.name,
                description: printer.description,
                hostname: args.hostname,
              },
              k8sOpts,
            ),
          );
        }

        return avahiServices;
      });
    }

    this.registerOutputs();
  }
}
