import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as avahi from "../avahi/index.js";

export interface AvahiServiceArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  name: pulumi.Input<string>;
  description?: pulumi.Input<string>;
  labelName: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
}

export class AvahiService extends avahi.AvahiService {
  /**
   * Create a new CUPS avahi service resource.
   * @param name The name of the CUPS avahi service resource.
   * @param args A bag of arguments to control the CUPS avahi service deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AvahiServiceArgs, opts?: pulumi.ResourceOptions) {
    super(
      name,
      {
        metadata: args.metadata,
        labelName: args.labelName,
        name: args.name,
        services: [
          {
            type: "_ipp._tcp",
            subtype: "_universal._sub._ipp._tcp",
            port: 631,
            hostname: args.hostname,
            txtRecords: [
              "txtvers=1",
              "qtotal=1",
              "Transparent=T",
              "URF=none",
              pulumi.interpolate`rp=printers/${args.name}`,
              pulumi.interpolate`note=${args.description ?? name}`,
              "product=(GPL Ghostscript)",
              "printer-state=3",
              "printer-type=0x2900c",
              "pdl=" +
                [
                  "application/octet-stream",
                  "application/pdf",
                  "application/postscript",
                  "application/vnd.cups-raster",
                  "image/gif",
                  "image/jpeg",
                  "image/png",
                  "image/tiff",
                  "image/urf",
                  "text/html",
                  "text/plain",
                  "application/vnd.adobe-reader-postscript",
                  "application/vnd.cups-pdf",
                ].join(","),
            ],
          },
        ],
      },
      opts,
    );
  }
}
