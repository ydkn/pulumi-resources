import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as crypto from "crypto";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as localPathProvisioner from "../localPathProvisioner/index.js";
import * as rook from "../rook/index.js";
import * as traefik from "../traefik/index.js";

interface volume {
  id: pulumi.Output<string>;
  persistentVolumeClaimName: pulumi.Output<string>;
  subPath?: pulumi.Output<string>;
}

export interface VolumeArgs {
  id: pulumi.Input<string>;
  filesystemSubPath?: pulumi.Input<string>;
}

export interface PVCVolumeArgs extends VolumeArgs {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface NFSVolumeArgs extends VolumeArgs, kubernetes.v1.NfsVolumeConfig {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface TransmissionArgs {
  namespace?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  blocklist?: pulumi.Input<string>;
  speedLimitDown: pulumi.Input<number>;
  altSpeedDown: pulumi.Input<number>;
  downloadQueueSize: pulumi.Input<number>;
  peerLimitGlobal?: pulumi.Input<number>;
  peerLimitTorrent?: pulumi.Input<number>;
  uid: pulumi.Input<number>;
  gid: pulumi.Input<number>;
  storageClassName?: pulumi.Input<string>;
  pvcVolumes?: pulumi.Input<pulumi.Input<PVCVolumeArgs>[]>;
  nfsVolumes?: pulumi.Input<pulumi.Input<NFSVolumeArgs>[]>;
  sharedFilesystemVolumes?: pulumi.Input<pulumi.Input<VolumeArgs>[]>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  protect?: boolean;
}

export class Transmission extends ComponentResource {
  public static sharedFilesystemsConfigs?: pulumi.Input<{
    [key: string]: pulumi.Input<kubernetes.v1.SharedFilesystemConfig>;
  }>;

  private readonly credentials: kubernetes.v1.Secret;
  private readonly settings: kubernetes.v1.Secret;
  private readonly configVolumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly service: k8s.core.v1.Service;
  private readonly middleware: traefik.Middleware;
  private readonly ingress: kubernetes.v1.SecureIngress;
  public readonly username: pulumi.Output<string>;
  public readonly password: pulumi.Output<string>;
  public readonly namespace: pulumi.Output<string>;
  public readonly hostname: pulumi.Output<string>;

  /**
   * Create a new Transmission resource.
   * @param name The name of Transmission resource.
   * @param args A bag of arguments to control the Transmission creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: TransmissionArgs, opts?: pulumi.ComponentResourceOptions) {
    super("transmission", name, args, opts);

    const k8sOpts = this.opts(k8s.Provider);

    opts = this.opts();

    this.username = pulumi.secret("transmission");
    this.password = pulumi.secret(new random.RandomPassword("password", { length: 24, special: false }, opts).result);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "transmission",
        "app.kubernetes.io/component": "transmission",
        "app.kubernetes.io/instance": name,
      },
    };

    this.credentials = new kubernetes.v1.Secret(
      `${name}-credentials`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-credentials` },
        stringData: {
          username: this.username,
          password: this.password,
        },
      },
      k8sOpts,
    );

    this.settings = new kubernetes.v1.Secret(
      `${name}-settings`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-settings` },
        stringData: {
          "settings.json": pulumi
            .all([this.username, this.password, args.blocklist])
            .apply(([username, password, blocklist]) =>
              JSON.stringify(
                {
                  "blocklist-enabled": blocklist ? true : false,
                  "blocklist-url": blocklist ?? "",
                  "download-dir": "/downloads",
                  encryption: 2,
                  "incomplete-dir": "/downloads",
                  "incomplete-dir-enabled": false,
                  "max-peers-global": args.peerLimitGlobal ?? 256,
                  "peer-limit-global": args.peerLimitGlobal ?? 256,
                  "peer-limit-per-torrent": args.peerLimitTorrent ?? 48,
                  "port-forwarding-enabled": false,
                  "rpc-authentication-required": true,
                  "rpc-username": username,
                  "rpc-password": password,
                  "rpc-whitelist": "*",
                  "rpc-whitelist-enabled": true,
                  "speed-limit-down": args.speedLimitDown,
                  "speed-limit-down-enabled": true,
                  "speed-limit-up": 0,
                  "speed-limit-up-enabled": true,
                  "alt-speed-down": args.altSpeedDown,
                  "alt-speed-up": 0,
                  "download-queue-enabled": true,
                  "download-queue-size": args.downloadQueueSize,
                  "queue-stalled-enabled": true,
                  "queue-stalled-minutes": 60,
                  "start-added-torrents": false,
                  "trash-original-torrent-files": true,
                  umask: 18,
                  "utp-enabled": true,
                  "watch-dir-enabled": false,
                  "rename-partial-files": true,
                },
                null,
                2,
              ),
            ),
        },
      },
      k8sOpts,
    );

    const configVolumeName = pulumi.interpolate`${name}-config`;

    this.configVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      `${name}-config`,
      {
        metadata: { ...metadata, name: configVolumeName },
        spec: {
          storageClassName: args.storageClassName,
          accessModes: ["ReadWriteOnce"],
          resources: { requests: { storage: "256Mi" } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    const volumes = pulumi
      .all([
        args.pvcVolumes,
        args.nfsVolumes,
        args.sharedFilesystemVolumes,
        Transmission.sharedFilesystemsConfigs ?? {},
      ])
      .apply(([pvcVolumes, nfsVolumes, sharedFilesystemVolumes, sharedFilesystemsConfigs]) => {
        return pulumi.all(pvcVolumes ?? []).apply((pvcVolumes) => {
          return pulumi.all(nfsVolumes ?? []).apply((nfsVolumes) => {
            return pulumi.all(sharedFilesystemVolumes ?? []).apply((sharedFilesystemVolumes) => {
              const volumes: volume[] = [];

              for (const pvcVolume of pvcVolumes) {
                const pvc = new k8s.core.v1.PersistentVolumeClaim(
                  `${name}-pvc-${pvcVolume.id}`,
                  {
                    metadata: { ...metadata, name: pvcVolume.id },
                    spec: {
                      storageClassName: pvcVolume.storageClassName,
                      accessModes: pvcVolume.accessModes ?? ["ReadWriteOnce"],
                      resources: { requests: { storage: pvcVolume.size ?? "10Gi" } },
                    },
                  },
                  k8sOpts,
                );

                volumes.push({
                  id: pulumi.output(pvcVolume.id),
                  persistentVolumeClaimName: pvc.metadata.name,
                });
              }

              for (const nfsVolume of nfsVolumes) {
                const volume = kubernetes.v1.NfsVolume.fromConfig(
                  `${name}-nfs-${nfsVolume.id}`,
                  { metadata: { ...metadata, name: nfsVolume.id }, config: nfsVolume },
                  opts,
                );

                volumes.push({
                  id: pulumi.output(nfsVolume.id),
                  persistentVolumeClaimName: volume.persistentVolumeClaim.metadata.name,
                });
              }

              for (const sharedFilesystemVolume of sharedFilesystemVolumes) {
                const filesystem = sharedFilesystemsConfigs[sharedFilesystemVolume.id];

                if (filesystem === undefined) {
                  continue;
                }

                let persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim | undefined;

                switch (filesystem.type) {
                  case rook.SharedFilesystemType: {
                    const rookFilesystem = filesystem as rook.SharedFilesystemConfig;

                    persistentVolumeClaim = new rook.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      rook.sharedFilesystemArgsFromSharedFilesystemConfig(rookFilesystem, {
                        ...metadata,
                        name: sharedFilesystemVolume.id,
                      }),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                  case localPathProvisioner.SharedFilesystemType: {
                    const localPathProvisionerFilesystem = filesystem as localPathProvisioner.SharedFilesystemConfig;

                    persistentVolumeClaim = new localPathProvisioner.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      localPathProvisioner.sharedFilesystemArgsFromSharedFilesystemConfig(
                        localPathProvisionerFilesystem,
                        {
                          ...metadata,
                          name: sharedFilesystemVolume.id,
                        },
                      ),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                }

                if (persistentVolumeClaim !== undefined) {
                  volumes.push({
                    id: pulumi.output(sharedFilesystemVolume.id),
                    persistentVolumeClaimName: persistentVolumeClaim.metadata.name,
                    subPath: sharedFilesystemVolume.filesystemSubPath
                      ? pulumi.output(sharedFilesystemVolume.filesystemSubPath)
                      : undefined,
                  });
                }
              }

              return volumes;
            });
          });
        });
      });

    const config = pulumi.all([volumes]).apply(([volumes]) => {
      const deploymentVolumes: k8s.types.input.core.v1.Volume[] = [];
      const deploymentVolumeMounts: k8s.types.input.core.v1.VolumeMount[] = [];

      for (const volume of volumes) {
        const volumeName = crypto.createHash("sha1").update(volume.id).digest("hex");

        deploymentVolumes.push({
          name: volumeName,
          persistentVolumeClaim: { claimName: volume.persistentVolumeClaimName },
        });

        deploymentVolumeMounts.push({
          name: volumeName,
          mountPath: pulumi.interpolate`/mnt/${volume.id}`,
          subPath: volume.subPath,
        });
      }

      return { volumes: deploymentVolumes, volumeMounts: deploymentVolumeMounts };
    });

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: {
              labels: metadata.labels,
              annotations: { "backup.velero.io/backup-volumes": "config" },
            },
            spec: {
              nodeSelector: args.nodeSelector,
              initContainers: [
                {
                  name: "permissions",
                  image: "busybox:latest",
                  command: ["/bin/sh", "-c", pulumi.interpolate`chown -R ${args.uid}:${args.gid} /config`],
                  resources: {
                    requests: { memory: "8Mi" },
                    limits: { memory: "12Mi" },
                  },
                  volumeMounts: [{ name: "config", mountPath: "/config" }],
                },
                {
                  name: "settings",
                  image: "busybox:latest",
                  command: ["/bin/sh", "-c", "cp /settings/settings.json /config/settings.json"],
                  resources: {
                    requests: { memory: "8Mi" },
                    limits: { memory: "12Mi" },
                  },
                  volumeMounts: [
                    { name: "settings", mountPath: "/settings" },
                    { name: "config", mountPath: "/config" },
                  ],
                },
              ],
              containers: [
                {
                  name: "transmission",
                  image: "lscr.io/linuxserver/transmission:latest",
                  imagePullPolicy: "Always",
                  env: [
                    { name: "PUID", value: pulumi.interpolate`${args.uid}` },
                    { name: "PGID", value: pulumi.interpolate`${args.gid}` },
                    {
                      name: "USER",
                      valueFrom: { secretKeyRef: { name: this.credentials.metadata.name, key: "username" } },
                    },
                    {
                      name: "PASS",
                      valueFrom: { secretKeyRef: { name: this.credentials.metadata.name, key: "password" } },
                    },
                    { name: "WHITELIST", value: "*" },
                  ],
                  ports: [{ name: "transmission", containerPort: 9091, protocol: "TCP" }],
                  readinessProbe: {
                    periodSeconds: 10,
                    timeoutSeconds: 2,
                    successThreshold: 2,
                    failureThreshold: 4,
                    tcpSocket: { port: "transmission" },
                  },
                  livenessProbe: {
                    periodSeconds: 30,
                    timeoutSeconds: 2,
                    successThreshold: 1,
                    failureThreshold: 10,
                    tcpSocket: { port: "transmission" },
                  },
                  resources: {
                    requests: { memory: "512Mi" },
                    limits: { memory: "640Mi" },
                  },
                  volumeMounts: pulumi.all([config]).apply(([config]) => {
                    return (config.volumeMounts as k8s.types.input.core.v1.VolumeMount[]).concat([
                      { name: "config", mountPath: "/config", subPath: "transmission" },
                    ]);
                  }),
                },
                {
                  name: "flood",
                  image: "jesec/flood:latest",
                  imagePullPolicy: "Always",
                  securityContext: { runAsUser: args.uid, runAsGroup: args.gid },
                  ports: [{ name: "http", containerPort: 3001, protocol: "TCP" }],
                  env: [
                    { name: "HOME", value: "/config" },
                    { name: "FLOOD_OPTION_auth", value: "none" },
                    { name: "FLOOD_OPTION_port", value: "3001" },
                    { name: "FLOOD_OPTION_trurl", value: "http://localhost:9091/transmission/rpc" },
                    {
                      name: "FLOOD_OPTION_truser",
                      valueFrom: { secretKeyRef: { name: this.credentials.metadata.name, key: "username" } },
                    },
                    {
                      name: "FLOOD_OPTION_trpass",
                      valueFrom: { secretKeyRef: { name: this.credentials.metadata.name, key: "password" } },
                    },
                  ],
                  readinessProbe: {
                    periodSeconds: 10,
                    timeoutSeconds: 2,
                    successThreshold: 2,
                    failureThreshold: 4,
                    httpGet: { port: "http" },
                  },
                  livenessProbe: {
                    periodSeconds: 30,
                    timeoutSeconds: 2,
                    successThreshold: 1,
                    failureThreshold: 10,
                    httpGet: { port: "http" },
                  },
                  resources: {
                    requests: { memory: "192Mi" },
                    limits: { memory: "256Mi" },
                  },
                  volumeMounts: pulumi.all([config]).apply(([config]) => {
                    return (config.volumeMounts as k8s.types.input.core.v1.VolumeMount[]).concat([
                      { name: "config", mountPath: "/config", subPath: "flood" },
                    ]);
                  }),
                },
              ],
              volumes: pulumi.all([config]).apply(([config]) => {
                return (config.volumes as k8s.types.input.core.v1.Volume[]).concat([
                  { name: "settings", secret: { secretName: this.settings.metadata.name } },
                  { name: "config", persistentVolumeClaim: { claimName: configVolumeName } },
                ]);
              }),
              terminationGracePeriodSeconds: 120,
            },
          },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata,
        spec: {
          selector: metadata.labels,
          ports: [
            { name: "transmission", port: 9091, targetPort: "transmission" },
            { name: "http", port: 3001, targetPort: "http" },
          ],
        },
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.hostname = pulumi.interpolate`${this.service.metadata.name}.${this.service.metadata.namespace}.svc`;

    this.middleware = new traefik.Middleware(
      name,
      {
        metadata,
        spec: {
          headers: {
            customRequestHeaders: {
              Authorization: pulumi
                .all([this.username, this.password])
                .apply(([username, password]) => `Basic ${Buffer.from(username + ":" + password).toString("base64")}`),
            },
          },
        },
      },
      k8sOpts,
    );

    this.ingress = new kubernetes.v1.SecureIngress(
      name,
      {
        metadata: {
          ...metadata,
          annotations: {
            "traefik.ingress.kubernetes.io/router.middlewares": this.middleware.metadata.apply(
              (m) => `${m.namespace}-${m.name}@kubernetescrd`,
            ),
          },
        },
        spec: {
          ingressClassName: args.ingressClassName,
          rules: [
            {
              host: args.hostname,
              http: {
                paths: [
                  {
                    path: "/transmission/",
                    pathType: "Prefix",
                    backend: {
                      service: {
                        name: this.service.metadata.name,
                        port: { name: this.service.spec.ports[0].name },
                      },
                    },
                  },
                  {
                    path: "/",
                    pathType: "Prefix",
                    backend: {
                      service: {
                        name: this.service.metadata.name,
                        port: { name: this.service.spec.ports[1].name },
                      },
                    },
                  },
                ],
              },
            },
          ],
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}
