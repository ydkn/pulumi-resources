import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as crypto from "crypto";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as localPathProvisioner from "../localPathProvisioner/index.js";
import * as rook from "../rook/index.js";
import * as certManager from "../certManager/index.js";
import * as avahi from "../avahi/index.js";

interface volume {
  id: pulumi.Output<string>;
  persistentVolumeClaimName: pulumi.Output<string>;
}

export interface VolumeArgs {
  id: pulumi.Input<string>;
}

export interface PVCVolumeArgs extends VolumeArgs {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface NFSVolumeArgs extends VolumeArgs, kubernetes.v1.NfsVolumeConfig {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface MediaServerArgs {
  namespace?: pulumi.Input<string>;
  name: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  storage?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  uid: pulumi.Input<number>;
  gid: pulumi.Input<number>;
  pvcVolumes?: pulumi.Input<pulumi.Input<PVCVolumeArgs>[]>;
  nfsVolumes?: pulumi.Input<pulumi.Input<NFSVolumeArgs>[]>;
  sharedFilesystemVolumes?: pulumi.Input<pulumi.Input<VolumeArgs>[]>;
  clusterIssuer: pulumi.Input<string>;
  avahiLabelName?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  claimToken?: pulumi.Input<string>;
  protect?: boolean;
}

export class MediaServer extends ComponentResource {
  public static sharedFilesystemsConfigs?: pulumi.Input<{
    [key: string]: pulumi.Input<kubernetes.v1.SharedFilesystemConfig>;
  }>;

  private readonly configVolumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly certificatePassword: k8s.core.v1.Secret;
  private readonly certificate: certManager.Certificate;
  private readonly claimToken: k8s.core.v1.Secret;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: kubernetes.v1.Service;
  private readonly avahiService?: avahi.AvahiService;
  public readonly namespace: pulumi.Output<string>;
  public readonly pkcs12Password: pulumi.Output<string>;
  public readonly url: pulumi.Output<string>;

  /**
   * Create a new Plex media server resource.
   * @param name The name of the Plex media server resource.
   * @param args A bag of arguments to control the Plex media server creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: MediaServerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("plex:media-server", name, args, opts);

    if (args.storage === undefined) {
      args.storage = "100Gi";
    }

    if (args.resources === undefined) {
      args.resources = { requests: { memory: "512Mi" }, limits: { memory: "768Mi" } };
    }

    const k8sOpts = this.opts(k8s.Provider);

    opts = this.opts();

    this.url = pulumi.interpolate`https://${args.hostname}`;

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "plex",
        "app.kubernetes.io/component": "media-server",
        "app.kubernetes.io/instance": name,
      },
    };

    const configVolumeClaimName = pulumi.interpolate`${name}-config`;

    this.configVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      `${name}-config`,
      {
        metadata: { ...metadata, name: configVolumeClaimName },
        spec: {
          storageClassName: args.storageClassName,
          accessModes: ["ReadWriteOnce"],
          resources: { requests: { storage: args.storage } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.pkcs12Password = pulumi.secret(
      new random.RandomPassword(`${name}-pkcs12-password`, { length: 64, special: false }, opts).result,
    );

    this.certificatePassword = new k8s.core.v1.Secret(
      `${name}-pkcs12-password`,
      {
        metadata: { ...metadata, name: `${name}-pkcs12-password` },
        stringData: {
          password: this.pkcs12Password,
        },
      },
      k8sOpts,
    );

    this.certificate = new certManager.Certificate(
      name,
      {
        metadata,
        spec: {
          dnsNames: [args.hostname],
          privateKey: { rotationPolicy: "Always" },
          issuerRef: { name: args.clusterIssuer, kind: "ClusterIssuer" },
          keystores: {
            pkcs12: {
              create: true,
              passwordSecretRef: { name: this.certificatePassword.metadata.name, key: "password" },
            },
          },
        },
      },
      k8sOpts,
    );

    this.claimToken = new k8s.core.v1.Secret(
      `${name}-claim-token`,
      {
        metadata: { ...metadata, name: `${name}-claim-token` },
        stringData: {
          claimToken: args.claimToken ?? "",
        },
      },
      k8sOpts,
    );

    const volumes = pulumi
      .all([args.pvcVolumes, args.nfsVolumes, args.sharedFilesystemVolumes, MediaServer.sharedFilesystemsConfigs ?? {}])
      .apply(([pvcVolumes, nfsVolumes, sharedFilesystemVolumes, sharedFilesystemsConfigs]) => {
        return pulumi.all(pvcVolumes ?? []).apply((pvcVolumes) => {
          return pulumi.all(nfsVolumes ?? []).apply((nfsVolumes) => {
            return pulumi.all(sharedFilesystemVolumes ?? []).apply((sharedFilesystemVolumes) => {
              const volumes: volume[] = [];

              for (const pvcVolume of pvcVolumes) {
                const pvc = new k8s.core.v1.PersistentVolumeClaim(
                  `${name}-pvc-${pvcVolume.id}`,
                  {
                    metadata: { ...metadata, name: pvcVolume.id },
                    spec: {
                      storageClassName: pvcVolume.storageClassName,
                      accessModes: pvcVolume.accessModes ?? ["ReadWriteOnce"],
                      resources: { requests: { storage: pvcVolume.size ?? "10Gi" } },
                    },
                  },
                  opts,
                );

                volumes.push({
                  id: pulumi.output(pvcVolume.id),
                  persistentVolumeClaimName: pvc.metadata.name,
                });
              }

              for (const nfsVolume of nfsVolumes) {
                const volume = kubernetes.v1.NfsVolume.fromConfig(
                  `${name}-nfs-${nfsVolume.id}`,
                  { metadata: { ...metadata, name: nfsVolume.id }, config: nfsVolume },
                  opts,
                );

                volumes.push({
                  id: pulumi.output(nfsVolume.id),
                  persistentVolumeClaimName: volume.persistentVolumeClaim.metadata.name,
                });
              }

              for (const sharedFilesystemVolume of sharedFilesystemVolumes) {
                const filesystem = sharedFilesystemsConfigs[sharedFilesystemVolume.id];

                if (filesystem === undefined) {
                  continue;
                }

                let persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim | undefined;

                switch (filesystem.type) {
                  case rook.SharedFilesystemType: {
                    const rookFilesystem = filesystem as rook.SharedFilesystemConfig;

                    persistentVolumeClaim = new rook.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      rook.sharedFilesystemArgsFromSharedFilesystemConfig(rookFilesystem, {
                        ...metadata,
                        name: sharedFilesystemVolume.id,
                      }),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                  case localPathProvisioner.SharedFilesystemType: {
                    const localPathProvisionerFilesystem = filesystem as localPathProvisioner.SharedFilesystemConfig;

                    persistentVolumeClaim = new localPathProvisioner.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      localPathProvisioner.sharedFilesystemArgsFromSharedFilesystemConfig(
                        localPathProvisionerFilesystem,
                        {
                          ...metadata,
                          name: sharedFilesystemVolume.id,
                        },
                      ),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                }

                if (persistentVolumeClaim !== undefined) {
                  volumes.push({
                    id: pulumi.output(sharedFilesystemVolume.id),
                    persistentVolumeClaimName: persistentVolumeClaim.metadata.name,
                  });
                }
              }

              return volumes;
            });
          });
        });
      });

    const config = pulumi.all([volumes]).apply(([volumes]) => {
      const deploymentVolumes: k8s.types.input.core.v1.Volume[] = [];
      const deploymentVolumeMounts: k8s.types.input.core.v1.VolumeMount[] = [];

      for (const volume of volumes) {
        const volumeName = crypto.createHash("sha1").update(volume.id).digest("hex");

        deploymentVolumes.push({
          name: volumeName,
          persistentVolumeClaim: { claimName: volume.persistentVolumeClaimName },
        });

        deploymentVolumeMounts.push({ name: volumeName, mountPath: pulumi.interpolate`/mnt/${volume.id}` });
      }

      return { volumes: deploymentVolumes, volumeMounts: deploymentVolumeMounts };
    });

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: {
              labels: metadata.labels,
              annotations: { "backup.velero.io/backup-volumes": "backups" },
            },
            spec: {
              nodeSelector: args.nodeSelector,
              containers: [
                {
                  name: "plex-media-server",
                  image: "lscr.io/linuxserver/plex:latest",
                  imagePullPolicy: "Always",
                  env: [
                    { name: "HOSTNAME", value: args.name },
                    { name: "VERSION", value: "docker" },
                    { name: "TZ", value: "Etc/UTC" },
                    { name: "ADVERTISE_IP", value: this.url },
                    { name: "PUID", value: pulumi.all([args.uid]).apply(([uid]) => `${uid}`) },
                    { name: "PGID", value: pulumi.all([args.gid]).apply(([gid]) => `${gid}`) },
                    {
                      name: "PLEX_CLAIM",
                      valueFrom: { secretKeyRef: { name: this.claimToken.metadata.name, key: "claimToken" } },
                    },
                  ],
                  ports: [{ name: "plex", containerPort: 32400 }],
                  livenessProbe: {
                    periodSeconds: 30,
                    timeoutSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 8,
                    httpGet: { path: "/identity", port: "plex", scheme: "HTTPS" },
                  },
                  readinessProbe: {
                    periodSeconds: 30,
                    timeoutSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3,
                    httpGet: { path: "/identity", port: "plex", scheme: "HTTPS" },
                  },
                  resources: args.resources,
                  volumeMounts: pulumi.all([config]).apply(([config]) => {
                    return (config.volumeMounts as k8s.types.input.core.v1.VolumeMount[]).concat([
                      { name: "tls", mountPath: "/tls" },
                      { name: "config", mountPath: "/config" },
                      { name: "transcode", mountPath: "/transcode" },
                      { name: "backups", mountPath: "/backups" },
                    ]);
                  }),
                },
              ],
              terminationGracePeriodSeconds: 120,
              volumes: pulumi.all([config]).apply(([config]) => {
                return (config.volumes as k8s.types.input.core.v1.Volume[]).concat([
                  { name: "tls", secret: { secretName: this.certificate.secretName } },
                  { name: "config", persistentVolumeClaim: { claimName: configVolumeClaimName } },
                  { name: "transcode", emptyDir: {} },
                  { name: "backups", emptyDir: {} },
                ]);
              }),
            },
          },
        },
      },
      k8sOpts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          selector: metadata.labels,
          type: "LoadBalancer",
          ports: [{ name: "https", port: 443, targetPort: "plex" }],
        },
        hostnames: [args.hostname],
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    if (args.avahiLabelName !== undefined) {
      this.avahiService = new avahi.AvahiService(
        name,
        {
          metadata,
          labelName: args.avahiLabelName,
          name,
          services: [{ type: "_plexmediasvr._tcp", port: 443, hostname: args.hostname }],
        },
        k8sOpts,
      );
    }

    this.registerOutputs();
  }
}
