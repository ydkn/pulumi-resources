import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";

export interface PlexArgs {
  serverURL: pulumi.Input<string>;
  token: pulumi.Input<string>;
}

export interface PlaylistConfigArgs {
  name: pulumi.Input<string>;
  libraries: pulumi.Input<pulumi.Input<string>[]>;
  genres?: pulumi.Input<pulumi.Input<string>[]>;
  labels?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface PlaylistArgs {
  namespace?: pulumi.Input<string>;
  plex: pulumi.Input<PlexArgs>;
  playlist: pulumi.Input<PlaylistConfigArgs>;
  maxDuration?: pulumi.Input<string>;
  schedule: pulumi.Input<string>;
}

export class Playlist extends ComponentResource {
  private readonly plexConfig: k8s.core.v1.Secret;
  private readonly cronjob: k8s.batch.v1.CronJob;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new Plex playlists resource.
   * @param name The name of the Plex playlists resource.
   * @param args A bag of arguments to control the Plex playlists creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: PlaylistArgs, opts?: pulumi.ComponentResourceOptions) {
    super("plex:playlists", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "plex",
        "app.kubernetes.io/component": "playlists",
        "app.kubernetes.io/instance": name,
      },
    };

    this.plexConfig = new k8s.core.v1.Secret(
      `${name}-plex`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${metadata.name}-plex` },
        stringData: pulumi.all([args.plex]).apply(([plex]) => {
          return {
            URL: plex.serverURL,
            TOKEN: plex.token,
          };
        }),
      },
      opts,
    );

    this.cronjob = new k8s.batch.v1.CronJob(
      name,
      {
        metadata,
        spec: {
          schedule: args.schedule,
          successfulJobsHistoryLimit: 0,
          failedJobsHistoryLimit: 1,
          jobTemplate: {
            metadata: { labels: metadata.labels },
            spec: {
              template: {
                spec: {
                  restartPolicy: "OnFailure",
                  containers: [
                    {
                      name: "plex-random-playlists",
                      image: "registry.gitlab.com/ydkn/plex-random-playlists:latest",
                      imagePullPolicy: "Always",
                      command: ["plex-random-playlists"],
                      args: pulumi.all([args.maxDuration, args.playlist]).apply(([maxDuration, playlist]) => {
                        const args = ["--playlist", playlist.name, "--max-duration", maxDuration ?? "48h"];

                        for (const library of playlist.libraries) {
                          args.push(`--libraries`, library);
                        }

                        for (const genre of playlist.genres ?? []) {
                          args.push(`--genres`, genre);
                        }

                        for (const label of playlist.labels ?? []) {
                          args.push(`--labels`, label);
                        }

                        return args;
                      }),
                      envFrom: [{ secretRef: { name: this.plexConfig.metadata.name }, prefix: "PLEX_" }],
                      resources: {
                        requests: { memory: "64Mi" },
                        limits: { memory: "96Mi" },
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
