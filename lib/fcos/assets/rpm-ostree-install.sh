#!/bin/sh

PACKAGES=$(ls /etc/rpm-ostree-install/)
INSTALLED_PACKAGES=$(/usr/bin/rpm-ostree status | grep LayeredPackages: | awk '{for (i=2; i<=NF; i++) print $i}')

for package in ${PACKAGES}; do
  installed="false"

  for installed_package in ${INSTALLED_PACKAGES}; do
    if [ "${package}" = "${installed_package}" ]; then
      installed="true"
    fi
  done

  if [ "${installed}" = "false" ]; then
    /usr/bin/rpm-ostree install --apply-live --allow-inactive "${package}"
  fi
done

exit 0
