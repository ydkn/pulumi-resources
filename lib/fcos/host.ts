import * as pulumi from "@pulumi/pulumi";
import * as tls from "@pulumi/tls";
import { ComponentResource } from "../common/index.js";
import { IgnitionUserArgs } from "../node/index.js";
import { ConfigArgs, Config } from "./config.js";

export interface HostUserArgs {
  name: pulumi.Input<string>;
  groups?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface HostArgs extends ConfigArgs {
  sshUsers?: pulumi.Input<pulumi.Input<HostUserArgs>[]>;
}

export class Host extends ComponentResource {
  private static readonly defaultModules: string[] = ["wireguard"];
  private static readonly defaultPackages: string[] = [
    "netcat",
    "tcpdump",
    "iperf3",
    "htop",
    "golang-gvisor",
    "container-selinux",
    "screen",
  ];

  public readonly sshPrivateKeys: pulumi.Output<{ [key: string]: tls.PrivateKey }>;
  public readonly ignitionConfig: pulumi.Output<string>;

  /**
   * Create a new FCOS host resource.
   * @param name The name of the FCOS host resource.
   * @param args A bag of arguments to control the FCOS host creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: HostArgs, opts?: pulumi.ComponentResourceOptions) {
    super("fcos:host", name, args, opts);

    const emptyOpts = this.opts();

    this.sshPrivateKeys = pulumi.all([args.sshUsers]).apply(([sshUsers]) => {
      const sshPrivateKeys: { [key: string]: tls.PrivateKey } = {};

      for (const sshUser of sshUsers ?? []) {
        sshPrivateKeys[sshUser.name] = new tls.PrivateKey(
          `${name}-${sshUser.name}`,
          { algorithm: "ED25519" },
          emptyOpts,
        );
      }

      return sshPrivateKeys;
    });

    this.ignitionConfig = new Config(
      name,
      {
        ...args,
        files: pulumi.all([args.files ?? []]).apply(([files]) =>
          files.concat(
            Host.defaultModules
              .map((module) => ({
                path: `/etc/modules-load.d/${module}.conf`,
                contents: module,
                mode: 0o644,
              }))
              .concat(
                Host.defaultPackages.map((pkg) => ({
                  path: `/etc/rpm-ostree-install/${pkg}`,
                  contents: "",
                  mode: 0o644,
                })),
              ),
          ),
        ),
        users: pulumi
          .all([args.users, args.sshUsers, this.sshPrivateKeys])
          .apply(([users, sshUsers, sshPrivateKeys]) => {
            const allUsers: pulumi.Input<IgnitionUserArgs>[] = [];

            for (const user of users ?? []) {
              allUsers.push(user);
            }

            for (const sshUser of sshUsers ?? []) {
              allUsers.push({
                name: sshUser.name,
                groups: sshUser.groups,
                sshAuthorizedKeys: sshPrivateKeys[sshUser.name].publicKeyOpenssh.apply((key) => [key.trim()]),
              });
            }

            return allUsers;
          }),
        systemdUnits: pulumi
          .all([args.systemdUnits])
          .apply(([systemdUnits]) =>
            (systemdUnits ?? []).concat({ mask: true, name: "docker.service" }, { mask: true, name: "docker.socket" }),
          ),
      },
      emptyOpts,
    ).ignitionConfig;

    this.registerOutputs();
  }
}
