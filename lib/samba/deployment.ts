import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as crypto from "crypto";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as localPathProvisioner from "../localPathProvisioner/index.js";
import * as rook from "../rook/index.js";
import { AvahiService } from "./avahiService.js";

interface volume {
  name: pulumi.Output<string>;
  comment?: pulumi.Output<string>;
  persistentVolumeClaimName: pulumi.Output<string>;
  timeMachine?: pulumi.Input<boolean>;
}

export interface ShareArgs {
  id: pulumi.Input<string>;
  shareName: pulumi.Input<string>;
  comment?: pulumi.Input<string>;
  timeMachine?: pulumi.Input<boolean>;
}

export interface PVCVolumeArgs extends ShareArgs {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface NFSVolumeArgs extends ShareArgs, kubernetes.v1.NfsVolumeConfig {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface SambaArgs {
  namespace?: pulumi.Input<string>;
  serverName: pulumi.Input<string>;
  workgroup: pulumi.Input<string>;
  domain?: pulumi.Input<string>;
  domainNameservers?: pulumi.Input<pulumi.Input<string>[]>;
  domainJoinUsername?: pulumi.Input<string>;
  domainJoinPassword?: pulumi.Input<string>;
  deviceModel?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  pvcVolumes?: pulumi.Input<pulumi.Input<PVCVolumeArgs>[]>;
  nfsVolumes?: pulumi.Input<pulumi.Input<NFSVolumeArgs>[]>;
  sharedFilesystemVolumes?: pulumi.Input<pulumi.Input<ShareArgs>[]>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  avahiLabelName?: pulumi.Input<string>;
  avahiServiceName?: pulumi.Input<string>;
  storageClassName?: pulumi.Input<string>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  calicoIpReservations?: pulumi.Input<pulumi.Input<string>[]>;
  cleanupSchedule?: pulumi.Input<string>;
  protect?: boolean;
}

export class Samba extends ComponentResource {
  public static sharedFilesystemsConfigs?: pulumi.Input<{
    [key: string]: pulumi.Input<kubernetes.v1.SharedFilesystemConfig>;
  }>;
  private static readonly vetoFiles = [
    ".apdisk",
    ".DS_Store",
    "._.DS_Store",
    ".TemporaryItems",
    "._.TemporaryItems",
    ".Trashes",
    "desktop.ini",
    "ehthumbs.db",
    "Network Trash Folder",
    "Temporary Items",
    "Thumbs.db",
  ];

  private readonly settings: k8s.core.v1.ConfigMap;
  private readonly domainJoinCredentials: k8s.core.v1.Secret;
  private readonly calicoIpReservations?: k8s.apiextensions.CustomResource;
  private readonly dataVolumeClaim: k8s.core.v1.PersistentVolumeClaim;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: k8s.core.v1.Service;
  private readonly timeMachineUuid?: pulumi.Output<string>;
  private readonly avahiService?: AvahiService;
  private readonly cronjob: k8s.batch.v1.CronJob;
  public readonly namespace: pulumi.Output<string>;
  public readonly hostname: pulumi.Output<string>;

  /**
   * Create a new Samba resource.
   * @param name The name of the Samba resource.
   * @param args A bag of arguments to control the Samba creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: SambaArgs, opts?: pulumi.ComponentResourceOptions) {
    super("samba", name, args, opts);

    if (args.serviceType === undefined) {
      args.serviceType = "LoadBalancer";
    }

    if (args.cleanupSchedule === undefined) {
      args.cleanupSchedule = "0 4 * * *";
    }

    const k8sOpts = this.opts(k8s.Provider);

    opts = this.opts();

    this.hostname = pulumi.output(args.hostname);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "samba",
        "app.kubernetes.io/component": "samba",
        "app.kubernetes.io/instance": name,
      },
    };

    const volumes = pulumi
      .all([args.pvcVolumes, args.nfsVolumes, args.sharedFilesystemVolumes, Samba.sharedFilesystemsConfigs ?? {}])
      .apply(([pvcVolumes, nfsVolumes, sharedFilesystemVolumes, sharedFilesystemsConfigs]) => {
        return pulumi.all(pvcVolumes ?? []).apply((pvcVolumes) => {
          return pulumi.all(nfsVolumes ?? []).apply((nfsVolumes) => {
            return pulumi.all(sharedFilesystemVolumes ?? []).apply((sharedFilesystemVolumes) => {
              const volumes: volume[] = [];

              for (const pvcVolume of pvcVolumes) {
                const pvc = new k8s.core.v1.PersistentVolumeClaim(
                  `${name}-pvc-${pvcVolume.id}`,
                  {
                    metadata: { ...metadata, name: pvcVolume.id },
                    spec: {
                      storageClassName: pvcVolume.storageClassName,
                      accessModes: pvcVolume.accessModes ?? ["ReadWriteOnce"],
                      resources: { requests: { storage: pvcVolume.size ?? "10Gi" } },
                    },
                  },
                  { ...k8sOpts, protect: args.protect },
                );

                volumes.push({
                  name: pulumi.output(pvcVolume.shareName),
                  comment: pvcVolume.comment ? pulumi.output(pvcVolume.comment) : undefined,
                  persistentVolumeClaimName: pvc.metadata.name,
                  timeMachine: pvcVolume.timeMachine,
                });
              }

              for (const nfsVolume of nfsVolumes) {
                const volume = kubernetes.v1.NfsVolume.fromConfig(
                  `${name}-nfs-${nfsVolume.id}`,
                  { metadata: { ...metadata, name: nfsVolume.id }, config: nfsVolume },
                  opts,
                );

                volumes.push({
                  name: pulumi.output(nfsVolume.shareName),
                  comment: nfsVolume.comment ? pulumi.output(nfsVolume.comment) : undefined,
                  persistentVolumeClaimName: volume.persistentVolumeClaim.metadata.name,
                  timeMachine: nfsVolume.timeMachine,
                });
              }

              for (const sharedFilesystemVolume of sharedFilesystemVolumes) {
                const filesystem = sharedFilesystemsConfigs[sharedFilesystemVolume.id];

                if (filesystem === undefined) {
                  continue;
                }

                let persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim | undefined;

                switch (filesystem.type) {
                  case rook.SharedFilesystemType: {
                    const rookFilesystem = filesystem as rook.SharedFilesystemConfig;

                    persistentVolumeClaim = new rook.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      rook.sharedFilesystemArgsFromSharedFilesystemConfig(rookFilesystem, {
                        ...metadata,
                        name: sharedFilesystemVolume.id,
                      }),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                  case localPathProvisioner.SharedFilesystemType: {
                    const localPathProvisionerFilesystem = filesystem as localPathProvisioner.SharedFilesystemConfig;

                    persistentVolumeClaim = new localPathProvisioner.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      localPathProvisioner.sharedFilesystemArgsFromSharedFilesystemConfig(
                        localPathProvisionerFilesystem,
                        {
                          ...metadata,
                          name: sharedFilesystemVolume.id,
                        },
                      ),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                }

                if (persistentVolumeClaim !== undefined) {
                  volumes.push({
                    name: pulumi.output(sharedFilesystemVolume.shareName),
                    comment: sharedFilesystemVolume.comment ? pulumi.output(sharedFilesystemVolume.comment) : undefined,
                    persistentVolumeClaimName: persistentVolumeClaim.metadata.name,
                    timeMachine: sharedFilesystemVolume.timeMachine,
                  });
                }
              }

              return volumes;
            });
          });
        });
      });

    const config = pulumi
      .all([args.workgroup, args.domain, args.serverName, args.deviceModel, volumes])
      .apply(([workgroup, domain, serverName, deviceModel, volumes]) => {
        const sambaConfig = [
          "[global]",
          `workgroup = ${workgroup}`,
          `server string = ${serverName}`,
          `netbios name = ${serverName}`,
          "log file = /dev/stdout",
          "max log size = 50",
          "dns proxy = no",
          "map to guest = bad user",
          "usershare allow guests = no",
          "obey pam restrictions = yes",
          "pam password change = no",
          "unix password sync = yes",
          "passwd program = /usr/bin/passwd %u",
          "panic action = /usr/share/samba/panic-action %d",
          "client ipc min protocol = SMB2",
          "client ipc max protocol = SMB3",
          "client min protocol = SMB2",
          "client max protocol = SMB3",
          "server min protocol = SMB2",
          "server max protocol = SMB3",
          "follow symlinks = yes",
          "load printers = no",
          "printing = bsd",
          "printcap name = /dev/null",
          "disable spoolss = yes",
          "smb compression = yes",
          "server multi channel support = yes",
          "smb encrypt = off",
          "socket options = TCP_NODELAY IPTOS_THROUGHPUT SO_KEEPALIVE SO_RCVBUF=65536 SO_SNDBUF=65536",
          "min receivefile size = 16384",
          "use sendfile = yes",
          "aio max threads = 128",
          "aio read size = 16384",
          "aio write size = 16384",
          "aio write behind = yes",
          "posix locking = no",
          "strict locking = no",
          "unix extensions = yes",
          "wide links = no",
          "mangled names = no",
          "unix charset = UTF-8",
          "acl allow execute always = yes",
          "vfs objects = catia fruit streams_xattr acl_xattr",
          "map acl inherit = yes",
          "store dos attributes = yes",
          "inherit acls = yes",
          "fruit:aapl = yes",
          "fruit:veto_appledouble = yes",
          "fruit:delete_empty_adfiles = yes",
          "fruit:advertise_fullsync = yes",
          "fruit:wipe_intentionally_left_blank_rfork = yes",
          `fruit:model = ${deviceModel}`,
          "fruit:locking = netatalk",
          "fruit:metadata = stream",
          "fruit:posix_rename = yes",
          "fruit:zero_file_id = yes",
          "fruit:resource = xattr",
          "fruit:nfs_aces = no",
          "ea support = yes",
          "durable handles = yes",
          "kernel share modes = no",
          "kernel oplocks = no",
          "write cache size = 2097152",
          "getwd cache = yes",
          "winbind nss info = rfc2307",
          "winbind use default domain = yes",
          "winbind enum users = yes",
          "winbind enum groups = yes",
          "winbind nested groups = yes",
          "winbind refresh tickets = yes",
          "winbind offline logon = yes",
        ];

        if (args.domainJoinUsername !== undefined && (args.domainJoinPassword !== undefined && domain) !== undefined) {
          sambaConfig.push(
            `realm = ${domain}`,
            "server role = member server",
            "security = ADS",
            `idmap config ${workgroup} : backend = ad`,
            `idmap config ${workgroup} : schema_mode = rfc2307`,
            `idmap config ${workgroup} : range = 2000-999999`,
            `idmap config ${workgroup} : unix_primary_group = yes`,
            `idmap config ${workgroup} : unix_nss_info = yes`,
            "idmap config *: range = 1000000-1999999",
            "idmap config * : backend = tdb",
          );
        } else {
          sambaConfig.push("server role = standalone server", "security = USER");
        }

        const deploymentVolumes: k8s.types.input.core.v1.Volume[] = [];
        const deploymentVolumeMounts: k8s.types.input.core.v1.VolumeMount[] = [];

        for (const volume of volumes) {
          const volumeName = crypto.createHash("sha1").update(volume.name).digest("hex");

          deploymentVolumes.push({
            name: volumeName,
            persistentVolumeClaim: { claimName: volume.persistentVolumeClaimName },
          });

          const mountPath = `/shares/${volumeName}`;

          deploymentVolumeMounts.push({ name: volumeName, mountPath });

          sambaConfig.push(
            `[${volume.name}]`,
            `comment = ${volume.comment ?? volume.name}`,
            `path = ${mountPath}`,
            "browseable = yes",
            "read only = no",
            "writable = yes",
            "guest ok = no",
            `veto files = /${Samba.vetoFiles.join("/")}/`,
            "delete veto files = yes",
          );

          if (volume.timeMachine) {
            sambaConfig.push("fruit:time machine = yes");
          }
        }

        return { smbConf: sambaConfig.join("\n"), volumes: deploymentVolumes, volumeMounts: deploymentVolumeMounts };
      });

    this.settings = new k8s.core.v1.ConfigMap(
      `${name}-config`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-config` },
        data: {
          "smb.conf": config.smbConf,
        },
      },
      k8sOpts,
    );

    this.domainJoinCredentials = new k8s.core.v1.Secret(
      `${name}-domain-join-credentials`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-domain-join-credentials` },
        stringData: {
          username: args.domainJoinUsername ?? "",
          password: args.domainJoinPassword ?? "",
        },
      },
      k8sOpts,
    );

    if (args.calicoIpReservations !== undefined) {
      this.calicoIpReservations = new k8s.apiextensions.CustomResource(
        name,
        {
          apiVersion: "crd.projectcalico.org/v1",
          kind: "IPReservation",
          metadata: { name },
          spec: {
            reservedCIDRs: args.calicoIpReservations,
          },
        },
        k8sOpts,
      );
    }

    const dataVolumeClaimName = pulumi.interpolate`${name}-data`;

    this.dataVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      `${name}-data`,
      {
        metadata: { ...metadata, name: dataVolumeClaimName },
        spec: {
          storageClassName: args.storageClassName,
          accessModes: ["ReadWriteOnce"],
          resources: { requests: { storage: "128Mi" } },
        },
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: {
              labels: metadata.labels,
              annotations: {
                "backup.velero.io/backup-volumes": "data",
                "cni.projectcalico.org/ipAddrs": pulumi.all([args.calicoIpReservations]).apply(([ipReservations]) => {
                  if (ipReservations === undefined) {
                    return pulumi.output("");
                  }

                  return pulumi
                    .all(ipReservations)
                    .apply(
                      (ipReservations) => `[${ipReservations.map((ipReservation) => `"${ipReservation}"`).join(",")}]`,
                    );
                }),
              },
            },
            spec: {
              nodeSelector: args.nodeSelector,
              hostname: pulumi.all([args.serverName]).apply(([serverName]) => serverName.toLowerCase()),
              dnsPolicy: args.domainNameservers ? "None" : undefined,
              dnsConfig: {
                nameservers: args.domainNameservers,
                searches: args.domain ? [args.domain] : undefined,
              },
              containers: [
                {
                  name: "samba",
                  image: "ydkn/samba:latest",
                  imagePullPolicy: "Always",
                  env: [
                    { name: "WORKGROUP", value: args.workgroup },
                    { name: "REALM", value: args.domain ?? "" },
                  ],
                  ports: [
                    { containerPort: 137, protocol: "UDP", name: "netbios-ns" },
                    { containerPort: 138, protocol: "UDP", name: "netbios-dgm" },
                    { containerPort: 139, protocol: "TCP", name: "netbios-ssn" },
                    { containerPort: 445, protocol: "TCP", name: "smb" },
                  ],
                  readinessProbe: {
                    exec: { command: ["smbclient", "-L", "\\\\localhost", "-U", "%", "-m", "SMB3"] },
                    periodSeconds: 20,
                    failureThreshold: 3,
                    successThreshold: 1,
                    timeoutSeconds: 10,
                  },
                  livenessProbe: {
                    exec: { command: ["smbclient", "-L", "\\\\localhost", "-U", "%", "-m", "SMB3"] },
                    periodSeconds: 20,
                    failureThreshold: 3,
                    successThreshold: 1,
                    timeoutSeconds: 10,
                  },
                  resources: {
                    requests: { memory: "512Mi" },
                    limits: { memory: "768Mi" },
                  },
                  volumeMounts: pulumi.all([config]).apply(([config]) => {
                    return (config.volumeMounts as k8s.types.input.core.v1.VolumeMount[]).concat([
                      { name: "config", mountPath: "/config" },
                      { name: "domain-join-credentials", mountPath: "/run/secrets/samba-dc-join-credentials" },
                      { name: "data", mountPath: "/var/lib/samba" },
                    ]);
                  }),
                },
              ],
              volumes: pulumi.all([config]).apply(([config]) => {
                return (config.volumes as k8s.types.input.core.v1.Volume[]).concat([
                  { name: "config", configMap: { name: this.settings.metadata.name } },
                  { name: "domain-join-credentials", secret: { secretName: this.domainJoinCredentials.metadata.name } },
                  { name: "data", persistentVolumeClaim: { claimName: dataVolumeClaimName } },
                ]);
              }),
            },
          },
        },
      },
      k8sOpts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          type: args.serviceType,
          loadBalancerClass: args.serviceLoadBalancerClass,
          selector: metadata.labels,
          ports: [{ name: "smb", protocol: "TCP", port: 445, targetPort: "smb" }],
        },
        hostnames: [args.hostname],
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    if (args.avahiLabelName !== undefined) {
      this.avahiService = new AvahiService(
        name,
        {
          labelName: args.avahiLabelName,
          metadata,
          name: args.avahiServiceName ?? args.serverName,
          hostname: args.hostname,
          deviceModel: args.deviceModel,
          timeMachineShareName: pulumi
            .all([volumes])
            .apply(([volumes]) => volumes.find((volume) => volume.timeMachine)?.name),
        },
        k8sOpts,
      );
    }

    this.cronjob = new k8s.batch.v1.CronJob(
      name,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-cleanup` },
        spec: {
          schedule: args.cleanupSchedule,
          concurrencyPolicy: "Forbid",
          successfulJobsHistoryLimit: 0,
          failedJobsHistoryLimit: 1,
          jobTemplate: {
            metadata: { labels: metadata.labels },
            spec: {
              template: {
                spec: {
                  restartPolicy: "OnFailure",
                  containers: [
                    {
                      name: "cleanup",
                      image: "alpine:latest",
                      imagePullPolicy: "Always",
                      command: ["sh", "-c", `find /shares/ -name ".smbdelete*" -type f -delete`],
                      resources: {
                        requests: { memory: "64Mi" },
                        limits: { memory: "64Mi" },
                      },
                      volumeMounts: config.volumeMounts,
                    },
                  ],
                  volumes: config.volumes,
                },
              },
              backoffLimit: 10,
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
