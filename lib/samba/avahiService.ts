import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as avahi from "../avahi/index.js";

export interface AvahiServiceArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  name: pulumi.Input<string>;
  labelName: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  deviceModel?: pulumi.Input<string>;
  timeMachineShareName?: pulumi.Input<string | undefined>;
}

export class AvahiService extends avahi.AvahiService {
  private static readonly defaultDeviceModel = "Macmini9,1";

  /**
   * Create a new Samba avahi service resource.
   * @param name The name of the Samba avahi service resource.
   * @param args A bag of arguments to control the Samba avahi service deployment.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AvahiServiceArgs, opts?: pulumi.ResourceOptions) {
    super(
      name,
      {
        metadata: args.metadata,
        labelName: args.labelName,
        name: args.name,
        services: pulumi
          .all([args.deviceModel, args.timeMachineShareName])
          .apply(([deviceModel, timeMachineShareName]) => {
            const services: avahi.AvahiServiceDefinitionArgs[] = [
              {
                type: "_smb._tcp",
                port: 445,
                hostname: args.hostname,
              },
              {
                type: "_device-info._tcp",
                port: 0,
                txtRecords: [`model=${deviceModel ?? AvahiService.defaultDeviceModel}`],
                hostname: args.hostname,
              },
            ];

            if (timeMachineShareName !== undefined) {
              services.push({
                type: "_adisk._tcp",
                txtRecords: ["sys=waMa=0,adVF=0x100", `dk0=adVN=${timeMachineShareName},adVF=0x82`],
                hostname: args.hostname,
              });
            }

            return services;
          }),
      },
      opts,
    );
  }
}
