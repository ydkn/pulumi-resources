import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k0s from "@ydkn/pulumi-k0s";
import * as os from "os";
import * as fs from "fs";
import * as path from "path";
import * as yaml from "js-yaml";
import { ComponentResource } from "../common/index.js";
import { Node } from "../node/index.js";

export type NetworkArgs = k0s.types.input.K0sNetworkArgs;

export interface ClusterArgs {
  name?: pulumi.Input<string>;
  nodes: pulumi.Input<pulumi.Input<Node>[]>;
  k0s?: pulumi.Input<k0s.types.input.ClusterK0sArgs>;
  hooks?: pulumi.Input<k0s.types.input.ClusterHooksArgs>;
  cloudProvider?: pulumi.Input<boolean>;
  protect?: boolean;
}

export class Cluster extends ComponentResource {
  private readonly clusterProvider: k0s.Provider;
  private readonly cluster: k0s.Cluster;
  public readonly name: pulumi.Output<string>;
  public readonly nodes: pulumi.Output<pulumi.Output<Node>[]>;
  public readonly spec: pulumi.Output<k0s.types.output.ClusterSpec>;
  public readonly kubeconfig: pulumi.Output<string>;
  public readonly kubeletDirPath: pulumi.Output<string>;

  /**
   * Create a new cluster resource.
   * @param name The name of the cluster resource.
   * @param args A bag of arguments to control the cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("k0s:cluster", name, args, opts);

    const emptyOpts = this.opts();

    if (args.name === undefined) {
      this.name = new random.RandomId(name, { byteLength: 3 }, { ...emptyOpts, protect: args.protect }).hex;
    } else {
      this.name = pulumi.output(args.name);
    }

    this.nodes = pulumi.all([args.nodes]).apply(([nodes]) => {
      const outputNodes: pulumi.Output<Node>[] = [];

      for (const node of nodes) {
        outputNodes.push(pulumi.output(node));
      }

      return outputNodes;
    });

    this.clusterProvider = new k0s.Provider(name, { noDrain: true, concurrency: 2 }, emptyOpts);

    this.cluster = new k0s.Cluster(
      name,
      {
        metadata: { name: this.name },
        spec: pulumi
          .all([this.nodes, args.k0s, args.hooks, args.cloudProvider])
          .apply(([nodes, k0s, hooks, cloudProvider]) => {
            return {
              hosts: nodes.map((node) => {
                const installFlags: string[] = [];

                let isController = false;
                let isWorker = false;
                for (const role of node.roles ?? []) {
                  if (role === "controller") {
                    isController = true;
                  } else if (role === "worker") {
                    isWorker = true;
                  }
                }

                if (!cloudProvider && isController) {
                  installFlags.push("--enable-k0s-cloud-provider=true");
                }

                if (isWorker) {
                  installFlags.push("--enable-cloud-provider=true");
                }

                if (node.labels === undefined) {
                  node.labels = {};
                }

                const labels: string[] = [];

                for (const key in node.labels) {
                  // eslint-disable-next-line security/detect-object-injection
                  labels.push(`${key}=${node.labels[key]}`);
                }

                if (labels.length > 0) {
                  installFlags.push(`--labels=${labels.join(",")}`);
                }

                let noTaints: boolean | undefined = undefined;

                if (node.taints !== undefined) {
                  const taints: string[] = [];

                  for (const taint of node.taints) {
                    taints.push(`${taint.key}=${taint.value}:${taint.effect}`);
                  }

                  if (taints.length > 0) {
                    installFlags.push(`--taints=${taints.join(",")}`);
                  } else if (isController) {
                    noTaints = true;
                  }
                }

                return Cluster.k0sNode(node, installFlags, node.privateKey, hooks, noTaints);
              }),
              k0s: pulumi.all([k0s]).apply(([k0s]) => {
                if (k0s === undefined) {
                  k0s = {};
                }

                if (k0s.config === undefined) {
                  k0s.config = {};
                }

                if (k0s.config.spec === undefined) {
                  k0s.config.spec = {};
                }

                if (k0s.config.spec.api === undefined) {
                  k0s.config.spec.api = {};
                }

                if (k0s.config.spec.api.address === undefined) {
                  const controllerNodes = nodes.filter((node) => node.roles?.includes("controller"));

                  if (controllerNodes.length > 0) {
                    k0s.config.spec.api.address =
                      controllerNodes[0].privateIpv4Address ?? controllerNodes[0].ipv4Address;
                  }
                }

                if (k0s.config.spec.network === undefined) {
                  k0s.config.spec.network = {};
                }

                if (k0s.config.spec.network.kubeProxy === undefined) {
                  k0s.config.spec.network.kubeProxy = {};
                }

                k0s.config.spec.network.kubeProxy.mode = "nftables";

                return k0s;
              }),
            };
          }),
      },
      { ...emptyOpts, provider: this.clusterProvider },
    );

    this.spec = this.cluster.spec.apply((spec) => spec!);
    this.kubeconfig = pulumi.secret(
      Cluster.transformKubeconfig(
        this.cluster.kubeconfig,
        pulumi
          .all([args.k0s, this.cluster.spec])
          .apply(([k0s, spec]) => [
            ...(k0s?.config?.spec?.api?.sans ?? []),
            ...(spec?.k0s?.config?.spec?.api?.sans ?? []),
          ]),
      ),
    );

    this.kubeletDirPath = pulumi.output("/var/lib/k0s/kubelet");

    if (process.env.UPDATE_LOCAL_KUBECONFIG === "true") {
      Cluster.updateLocalKubeconfig(this.name, this.kubeconfig);
    }

    this.registerOutputs();
  }

  public static k0sRole(roles?: pulumi.Input<pulumi.Input<string>[]>): pulumi.Output<string> {
    return pulumi.all([roles]).apply(([roles]) => {
      let role = "worker";
      let foundControllerRole = false;
      let foundWorkerRole = false;

      for (const r of roles ?? []) {
        if (r === "controller") {
          foundControllerRole = true;
          role = "controller";
        } else if (r === "worker") {
          foundWorkerRole = true;
          role = "worker";
        }

        if (foundControllerRole && foundWorkerRole) {
          role = "controller+worker";
          break;
        }
      }

      return role;
    });
  }

  public static k0sNode(
    node: Node,
    installFlags: string[],
    key?: string,
    hooks?: pulumi.UnwrappedObject<k0s.types.input.ClusterHooksArgs>,
    noTaints?: boolean,
  ): k0s.types.input.ClusterHostArgs {
    return {
      hostname: node.hostname,
      privateAddress: node.privateIpv4Address,
      role: Cluster.k0sRole(node.roles),
      openSSH: { address: node.ipv4Address as string, user: node.username, port: 22, key, disableMultiplexing: true },
      installFlags,
      uploadBinary: process.env.K0S_UPLOAD_BINARY === "true",
      hooks: {
        apply: {
          before: (hooks?.apply?.before ?? []).concat(
            "sudo sh -c 'if [ ! -L /var/lib/kubelet ]; then ln -s /var/lib/k0s/kubelet /var/lib/kubelet; fi'",
          ),
          after: hooks?.apply?.after ?? [],
        },
        reset: {
          before: hooks?.reset?.before ?? [],
          after: (hooks?.reset?.after ?? []).concat(
            "sudo sh -c 'if [ -L /var/lib/kubelet ]; then rm -rf /var/lib/kubelet; fi'",
          ),
        },
        backup: {
          before: hooks?.backup?.before ?? [],
          after: hooks?.backup?.after ?? [],
        },
      },
      noTaints,
    };
  }

  public static rolesHostname(roles?: pulumi.Input<pulumi.Input<string>[]>): pulumi.Output<string> {
    const role = this.k0sRole(roles);

    return role.apply((role) => {
      if (role !== undefined && (role === "controller" || role === "controller+worker")) {
        return "controller";
      }

      return "worker";
    });
  }

  private static transformKubeconfig(
    kubeconfig: pulumi.Input<string>,
    sans?: pulumi.Input<pulumi.Input<string>[] | undefined>,
  ): pulumi.Output<string> {
    return pulumi.all([kubeconfig, sans]).apply(([kubeconfig, sans]) => {
      /* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment */
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const transformedKubeconfig = yaml.load(kubeconfig, { schema: yaml.FAILSAFE_SCHEMA }) as any;

      if (sans !== undefined && sans.length > 0) {
        if (transformedKubeconfig !== undefined && transformedKubeconfig.clusters.length > 0) {
          if (transformedKubeconfig.clusters[0].cluster !== undefined) {
            transformedKubeconfig.clusters[0].cluster.server = `https://${sans[0]}:6443`;
          }
        }
      }

      return yaml.dump(transformedKubeconfig);
    });
  }

  private static updateLocalKubeconfig(name: pulumi.Input<string>, kubeconfig: pulumi.Input<string>) {
    pulumi.all([name, kubeconfig]).apply(([name, kubeconfig]) => {
      const kubeconfigPath = path.join(os.homedir(), ".kube", "config");

      // eslint-disable-next-line security/detect-non-literal-fs-filename
      if (!fs.existsSync(kubeconfigPath)) {
        return;
      }

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const kubeconfigData = yaml.load(kubeconfig, { schema: yaml.FAILSAFE_SCHEMA }) as any;

      // eslint-disable-next-line security/detect-non-literal-fs-filename
      const localData = yaml.load(fs.readFileSync(path.join(os.homedir(), ".kube", "config"), "utf-8"), {
        schema: yaml.FAILSAFE_SCHEMA,
      }) as any; // eslint-disable-line @typescript-eslint/no-explicit-any

      let updated = false;

      for (const cluster of localData.clusters) {
        if (cluster.name !== name) {
          continue;
        }

        if (JSON.stringify(cluster.cluster) !== JSON.stringify(kubeconfigData.clusters[0].cluster)) {
          cluster.cluster = kubeconfigData.clusters[0].cluster;

          updated = true;
        }
      }

      for (const user of localData.users) {
        if (user.name !== name) {
          continue;
        }

        if (JSON.stringify(user.user) !== JSON.stringify(kubeconfigData.users[0].user)) {
          user.user = kubeconfigData.users[0].user;

          updated = true;
        }
      }

      if (updated) {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        fs.writeFileSync(kubeconfigPath, yaml.dump(localData));

        console.log("Updated local kubeconfig");
      }
    });
  }
}
