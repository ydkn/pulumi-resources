import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as certManager from "../certManager/index.js";

export interface ClusterSpecArgs {
  version?: pulumi.Input<string>;
  featureCompatibilityVersion?: pulumi.Input<string>;
  members?: pulumi.Input<number>;
}

export interface VolumeArgs {
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface VolumesArgs {
  data?: pulumi.Input<VolumeArgs>;
  logs?: pulumi.Input<VolumeArgs>;
}

export interface UserArgs {
  name: pulumi.Input<string>;
}

export interface ClusterArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<ClusterSpecArgs>;
  volumes?: pulumi.Input<VolumesArgs>;
  users?: pulumi.Input<pulumi.Input<UserArgs>[]>;
  hostname?: pulumi.Input<string>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  clusterDomain?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  protect?: boolean;
}

export class Cluster extends ComponentResource {
  private static readonly defaultVersion = "7.0";
  private static readonly defaultFeatureCompatibilityVersion = "7.0";

  private readonly adminSecret: kubernetes.v1.Secret;
  private readonly issuer: certManager.Issuer;
  private readonly caIssuer: certManager.Issuer;
  private readonly caCertificate: certManager.Certificate;
  private readonly certificate: certManager.Certificate;
  private readonly cluster: k8s.apiextensions.CustomResource;
  private readonly services: kubernetes.v1.Service[];
  public readonly adminPassword: pulumi.Output<string>;
  public readonly users: pulumi.Input<{ [key: string]: pulumi.Output<string> }>;

  /**
   * Create a new MongoDB cluster resource.
   * @param name The name of the MongoDB cluster resource.
   * @param args A bag of arguments to control the MongoDB cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("mongodb:cluster", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    if (args.resources === undefined) {
      args.resources = {
        requests: { cpu: "1000m", memory: "1Gi" },
        limits: { cpu: "2000m", memory: "1Gi" },
      };
    }

    args.spec = pulumi.all([args.spec]).apply(([spec]) => {
      if (spec.version === undefined) {
        spec.version = Cluster.defaultVersion;
      }

      if (spec.featureCompatibilityVersion === undefined) {
        spec.featureCompatibilityVersion = Cluster.defaultFeatureCompatibilityVersion;
      }

      if (spec.members === undefined) {
        spec.members = 1;
      }

      return spec;
    });

    this.adminPassword = pulumi.secret(
      new random.RandomPassword(
        `${name}-admin-password`,
        { length: 48, special: false },
        { ...emptyOpts, protect: args.protect },
      ).result,
    );

    this.adminSecret = new kubernetes.v1.Secret(
      `${name}-admin`,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return { ...metadata, name: `${metadata.name ?? name}-admin-credentials` };
        }),
        stringData: {
          username: "admin",
          password: this.adminPassword,
        },
      },
      k8sOpts,
    );

    this.users = pulumi.all([args.users]).apply(([users]) => {
      const usersMap: { [key: string]: pulumi.Output<string> } = {};

      for (const user of users ?? []) {
        if (user.name === "admin") {
          continue;
        }

        const password = pulumi.secret(
          new random.RandomPassword(`${name}-${user.name}-password`, { length: 48, special: false }, emptyOpts).result,
        );

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const secret = new kubernetes.v1.Secret(
          `${name}-${user.name}`,
          {
            metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
              return { ...metadata, name: `${metadata.name ?? name}-${user.name}-credentials` };
            }),
            stringData: {
              username: user.name,
              password,
            },
          },
          k8sOpts,
        );

        usersMap[user.name] = password;
      }

      return usersMap;
    });

    this.issuer = new certManager.Issuer(
      name,
      {
        metadata: args.metadata,
        spec: {
          selfSigned: {},
        },
      },
      k8sOpts,
    );

    const commonName = pulumi
      .all([args.metadata, args.clusterDomain])
      .apply(
        ([metadata, clusterDomain]) =>
          `*.${metadata.name ?? name}.${metadata.namespace}.svc.${clusterDomain ?? "cluster.local"}`,
      );
    const dnsNames = pulumi
      .all([args.metadata, args.hostname, args.clusterDomain])
      .apply(([metadata, hostname, clusterDomain]) => {
        const dnsNames = [
          `${metadata.name ?? name}.${metadata.namespace}.svc.${clusterDomain ?? "cluster.local"}`,
          `${metadata.name ?? name}.${metadata.namespace}.svc`,
          `${metadata.name ?? name}.${metadata.namespace}`,
          `${metadata.name ?? name}`,
          `*.${metadata.name ?? name}-svc.${metadata.namespace}.svc.${clusterDomain ?? "cluster.local"}`,
          `*.${metadata.name ?? name}-svc.${metadata.namespace}.svc`,
          `*.${metadata.name ?? name}-svc.${metadata.namespace}`,
          `*.${metadata.name ?? name}-svc`,
        ];

        if (hostname !== undefined) {
          dnsNames.push(hostname, `*.${hostname}`);
        }

        return dnsNames;
      });

    this.caCertificate = new certManager.Certificate(
      `${name}-ca`,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return { ...metadata, name: `${metadata.name ?? name}-ca` };
        }),
        spec: {
          isCA: true,
          commonName,
          duration: "87600h0m0s",
          renewBefore: "8760h0m0s",
          privateKey: { algorithm: "ECDSA", size: 256 },
          issuerRef: { name: this.issuer.metadata.name, kind: this.issuer.kind },
        },
      },
      k8sOpts,
    );

    this.caIssuer = new certManager.Issuer(
      `${name}-ca`,
      {
        metadata: pulumi.all([args.metadata]).apply(([metadata]) => {
          return { ...metadata, name: `${metadata.name ?? name}-ca` };
        }),
        spec: {
          ca: { secretName: this.caCertificate.secretName },
        },
      },
      k8sOpts,
    );

    this.certificate = new certManager.Certificate(
      name,
      {
        metadata: args.metadata,
        spec: {
          subject: { organizations: [name] },
          commonName,
          dnsNames,
          duration: "87600h0m0s",
          renewBefore: "8760h0m0s",
          privateKey: { algorithm: "ECDSA", size: 256 },
          issuerRef: { name: this.caIssuer.metadata.name, kind: this.caIssuer.kind },
          usages: ["server auth", "client auth"],
        },
      },
      k8sOpts,
    );

    this.cluster = new k8s.apiextensions.CustomResource(
      name,
      {
        apiVersion: "mongodbcommunity.mongodb.com/v1",
        kind: "MongoDBCommunity",
        metadata: args.metadata,
        spec: pulumi
          .all([args.metadata, args.spec, args.volumes, args.hostname, this.users])
          .apply(([metadata, spec, volumes, hostname, users]) => {
            const usersConfig = [
              {
                name: "admin",
                scramCredentialsSecretName: pulumi.interpolate`${name}-admin`,
                passwordSecretRef: { name: this.adminSecret.metadata.name },
                roles: [
                  { name: "clusterAdmin", db: "admin" },
                  { name: "userAdminAnyDatabase", db: "admin" },
                  { name: "readWriteAnyDatabase", db: "admin" },
                ],
              },
            ];

            for (const username in users ?? {}) {
              usersConfig.push({
                name: username,
                scramCredentialsSecretName: pulumi.interpolate`${name}-${username}`,
                passwordSecretRef: { name: pulumi.interpolate`${name}-${username}-credentials` },
                roles: [{ name: "dbOwner", db: username }],
              });
            }

            const replicaSetHorizons = [{ horizon: pulumi.interpolate`${args.hostname}:27017` }];

            for (let i = 0; i < spec.members!; i++) {
              replicaSetHorizons.push({
                horizon: pulumi.interpolate`${metadata.name ?? name}-${i}.${args.hostname}`,
              });
            }

            return {
              ...spec,
              type: "ReplicaSet",
              security: {
                tls:
                  hostname === undefined
                    ? { enabled: false }
                    : {
                        enabled: true,
                        optional: true,
                        caCertificateSecretRef: { name: this.caCertificate.secretName },
                        certificateKeySecretRef: { name: this.certificate.secretName },
                      },
                authentication: {
                  modes: ["SCRAM-SHA-256"],
                },
              },
              replicaSetHorizons,
              users: usersConfig,
              additionalMongodConfig: {
                "storage.wiredTiger.engineConfig.journalCompressor": "zlib",
              },
              statefulSet: {
                spec: {
                  template: {
                    spec: {
                      nodeSelector: args.nodeSelector,
                      containers: [
                        {
                          name: "mongod",
                          resources: args.resources,
                        },
                        {
                          name: "mongodb-agent",
                          resources: {
                            requests: { cpu: "100m", memory: "256Mi" },
                            limits: { cpu: "300m", memory: "512Mi" },
                          },
                        },
                      ],
                    },
                  },
                  volumeClaimTemplates: [
                    {
                      metadata: { name: "data-volume" },
                      spec: {
                        accessModes: ["ReadWriteOnce"],
                        storageClassName: volumes?.data?.storageClassName,
                        resources: { requests: { storage: volumes?.data?.size ?? "10Gi" } },
                      },
                    },
                    {
                      metadata: { name: "logs-volume" },
                      spec: {
                        accessModes: ["ReadWriteOnce"],
                        storageClassName: volumes?.logs?.storageClassName,
                        resources: { requests: { storage: volumes?.logs?.size ?? "10Gi" } },
                      },
                    },
                  ],
                },
              },
            };
          }),
      },
      { ...k8sOpts, protect: args.protect },
    );

    this.services = [];

    this.services.push(
      new kubernetes.v1.Service(
        name,
        {
          metadata: pulumi.all([args.metadata, args.serviceLabels]).apply(([metadata, labels]) => {
            return { ...metadata, labels: { ...metadata.labels, ...labels } };
          }),
          spec: {
            type: args.serviceType ?? "ClusterIP",
            loadBalancerClass: args.serviceLoadBalancerClass,
            selector: {
              app: pulumi.all([args.metadata]).apply(([metadata]) => `${metadata.name ?? name}-svc`),
            },
            ports: [{ name: "mongodb", port: 27017, targetPort: 27017 }],
          },
          hostnames: args.hostname ? [args.hostname] : undefined,
        },
        k8sOpts,
      ),
    );

    if (args.hostname !== undefined) {
      pulumi.all([args.metadata, args.spec, args.serviceLabels]).apply(([metadata, spec, labels]) => {
        for (let i = 0; i < spec.members!; i++) {
          this.services.push(
            new kubernetes.v1.Service(
              `${name}-${i}`,
              {
                metadata: {
                  ...metadata,
                  name: `${metadata.name ?? name}-${i}`,
                  labels: { ...metadata.labels, ...labels },
                },
                spec: {
                  loadBalancerClass: args.serviceLoadBalancerClass,
                  type: args.serviceType ?? "ClusterIP",
                  selector: {
                    app: `${metadata.name ?? name}-svc`,
                    "statefulset.kubernetes.io/pod-name": `${metadata.name ?? name}-${i}`,
                  },
                  ports: [{ name: "mongodb", port: 27017, targetPort: 27017 }],
                },
                hostnames: [pulumi.interpolate`${metadata.name ?? name}-${i}.${args.hostname}`],
              },
              k8sOpts,
            ),
          );
        }
      });
    }

    this.registerOutputs();
  }
}
