import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface OperatorArgs {
  namespace?: pulumi.Input<string>;
  clusterDomain?: pulumi.Input<string>;
}

export class Operator extends ComponentResource {
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new MongoDB operator resource.
   * @param name The name of the MongoDB operator resource.
   * @param args A bag of arguments to control the MongoDB operator creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: OperatorArgs, opts?: pulumi.ComponentResourceOptions) {
    super("mongodb:operator", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        namespace: this.namespace,
        repositoryOpts: { repo: "https://mongodb.github.io/helm-charts" },
        chart: "community-operator",
        values: {
          fullnameOverride: name,
          operator: {
            name,
            deploymentName: name,
            watchNamespace: "*",
            extraEnvs: [{ name: "CLUSTER_DOMAIN", value: args.clusterDomain ?? "cluster.local" }],
            resources: {
              requests: { cpu: "10m", memory: "256Mi" },
              limits: { cpu: "200m", memory: "320Mi" },
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
