import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as sharedFilesystem from "../kubernetes/v1/sharedFilesystem.js";
import { LocalPathProvisioner } from "./deployment.js";

interface SharedFilesystemProperties {
  hostPath: pulumi.Input<string>;
  nodeAffinity: pulumi.Input<k8s.types.output.core.v1.VolumeNodeAffinity>;
}

export const SharedFilesystemType = "local-path-provisioner";

export interface SharedFilesystemConfig extends sharedFilesystem.SharedFilesystemConfig, SharedFilesystemProperties {}

export interface SharedFilesystemArgs extends sharedFilesystem.SharedFilesystemArgs, SharedFilesystemProperties {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
}

export class SharedFilesystem extends ComponentResource {
  public static readonly defaultStorageClassName = LocalPathProvisioner.storageClassName;
  public static readonly defaultSize = "10Gi";
  public static readonly accessModes = ["ReadWriteOnce"];

  public readonly persistentVolume: k8s.core.v1.PersistentVolume;
  public readonly persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim;

  /**
   * Create a new local shared filesystem resource.
   * @param name The name of the local shared filesystem resource.
   * @param args A bag of arguments to control the local shared filesystem creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: SharedFilesystemArgs, opts?: pulumi.ComponentResourceOptions) {
    super("local-path-provisioner:shared-filesystem", name, args, opts);

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    const persistentVolumeUUID = new random.RandomUuid(name, {}, emptyOpts);
    const volumeName = pulumi.all([persistentVolumeUUID.result]).apply(([uuid]) => `fs-${uuid}`);

    this.persistentVolume = new k8s.core.v1.PersistentVolume(
      name,
      {
        metadata: pulumi.all([args.metadata, volumeName]).apply(([metadata, volumeName]) => {
          return { ...metadata, name: volumeName, namespace: undefined };
        }),
        spec: {
          storageClassName: args.storageClassName,
          accessModes: SharedFilesystem.accessModes,
          capacity: { storage: args.storage ?? SharedFilesystem.defaultSize },
          persistentVolumeReclaimPolicy: "Retain",
          volumeMode: "Filesystem",
          hostPath: { path: args.hostPath, type: "Directory" },
          nodeAffinity: args.nodeAffinity,
        },
      },
      k8sOpts,
    );

    this.persistentVolumeClaim = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: args.metadata,
        spec: {
          accessModes: SharedFilesystem.accessModes,
          storageClassName: args.storageClassName,
          volumeName: this.persistentVolume.metadata.name,
          resources: { requests: { storage: args.storage ?? SharedFilesystem.defaultSize } },
          volumeMode: "Filesystem",
        },
      },
      k8sOpts,
    );

    this.registerOutputs();
  }
}

export interface SharedFilesystemFromConfigArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
  protect?: pulumi.Input<boolean>;
}

export function sharedFilesystemConfigFromConfig(
  name: string,
  args: pulumi.Input<SharedFilesystemFromConfigArgs>,
  opts?: pulumi.ComponentResourceOptions,
): pulumi.Output<SharedFilesystemConfig> {
  return pulumi.all([args]).apply(([args]) => {
    const fsName = args.metadata.name ?? name;

    const filesystem = new k8s.core.v1.PersistentVolumeClaim(
      name,
      {
        metadata: { ...args.metadata, name: fsName },
        spec: {
          accessModes: SharedFilesystem.accessModes,
          resources: { requests: { storage: args.size ?? SharedFilesystem.defaultSize } },
          storageClassName: args.storageClassName ?? SharedFilesystem.defaultStorageClassName,
          volumeMode: "Filesystem",
        },
      },
      { ...opts, protect: args.protect ?? opts?.protect },
    );

    const job = new k8s.batch.v1.Job(
      name,
      {
        metadata: { ...args.metadata, name: pulumi.interpolate`provision-${fsName}`, labels: { pvc: fsName } },
        spec: {
          parallelism: 1,
          completions: 1,
          ttlSecondsAfterFinished: 60,
          template: {
            metadata: { labels: { pvc: fsName } },
            spec: {
              restartPolicy: "Never",
              containers: [
                {
                  name: "alpine",
                  image: "alpine:latest",
                  imagePullPolicy: "Always",
                  command: ["sh", "-c", "date"],
                  resources: { requests: { memory: "8Mi" }, limits: { memory: "8Mi" } },
                  volumeMounts: [{ name: "volume", mountPath: "/volume" }],
                },
              ],
              volumes: [{ name: "volume", persistentVolumeClaim: { claimName: filesystem.metadata.name } }],
            },
          },
        },
      },
      opts,
    );

    return filesystem.spec.volumeName.apply((volumeName) => {
      const volume = k8s.core.v1.PersistentVolume.get(volumeName, volumeName, { ...opts, dependsOn: job });

      return pulumi.secret({
        type: SharedFilesystemType,
        id: volume.metadata.name,
        storageClassName: volume.spec.storageClassName,
        storage: volume.spec.capacity.storage,
        hostPath: volume.spec.hostPath.path,
        nodeAffinity: volume.spec.nodeAffinity,
      });
    });
  });
}

export function sharedFilesystemArgsFromSharedFilesystemConfig(
  filesystemConfig: pulumi.Input<SharedFilesystemConfig>,
  metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>,
): SharedFilesystemArgs {
  const filesystemConfigApply = pulumi.all([filesystemConfig]);

  return {
    metadata: metadata ?? {},
    storageClassName: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.storageClassName!),
    storage: filesystemConfigApply.apply(
      ([filesystemConfig]) => filesystemConfig.storage ?? SharedFilesystem.defaultSize,
    ),
    hostPath: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.hostPath),
    nodeAffinity: filesystemConfigApply.apply(([filesystemConfig]) => filesystemConfig.nodeAffinity),
  };
}
