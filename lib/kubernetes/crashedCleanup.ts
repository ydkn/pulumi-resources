import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as path from "path";
import * as fs from "fs";
import * as url from "url";
import { ComponentResource } from "../common/index.js";

export interface CrashedCleanupArgs {
  namespace?: pulumi.Input<string>;
  schedule?: pulumi.Input<string>;
}

export class CrashedCleanup extends ComponentResource {
  private readonly serviceAccount: k8s.core.v1.ServiceAccount;
  private readonly clusterRole: k8s.rbac.v1.ClusterRole;
  private readonly clusterRoleBinding: k8s.rbac.v1.ClusterRoleBinding;
  private readonly script: k8s.core.v1.ConfigMap;
  private readonly cronjob: k8s.batch.v1.CronJob;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new crashed cleanup resource.
   * @param name The name of the crashed cleanup resource.
   * @param args A bag of arguments to control the crashed cleanup creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: CrashedCleanupArgs, opts?: pulumi.ComponentResourceOptions) {
    super("crashed-cleanup", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.schedule === undefined) {
      args.schedule = "11 */4 * * *";
    }

    this.namespace = args.namespace ? pulumi.output(args.namespace) : pulumi.output("kube-system");

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "crashed-cleanup",
        "app.kubernetes.io/component": "crashed-cleanup",
        "app.kubernetes.io/instance": name,
      },
    };

    this.serviceAccount = new k8s.core.v1.ServiceAccount(name, { metadata }, opts);

    this.clusterRole = new k8s.rbac.v1.ClusterRole(
      name,
      {
        metadata,
        rules: [
          { apiGroups: [""], resources: ["namespaces"], verbs: ["list"] },
          { apiGroups: [""], resources: ["pods"], verbs: ["list", "get", "delete", "watch"] },
          { apiGroups: ["batch"], resources: ["jobs"], verbs: ["list", "get", "delete", "watch"] },
        ],
      },
      opts,
    );

    this.clusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding(
      name,
      {
        metadata,
        subjects: [
          {
            kind: this.serviceAccount.kind,
            namespace: this.serviceAccount.metadata.namespace,
            name: this.serviceAccount.metadata.name,
          },
        ],
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: this.clusterRole.kind,
          name: this.clusterRole.metadata.name,
        },
      },
      opts,
    );

    this.script = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata,
        data: {
          "crashed-cleanup.sh": fs.readFileSync(
            path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "crashed-cleanup.sh"),
            "utf-8",
          ),
        },
      },
      opts,
    );

    this.cronjob = new k8s.batch.v1.CronJob(
      name,
      {
        metadata,
        spec: {
          schedule: args.schedule,
          concurrencyPolicy: "Forbid",
          successfulJobsHistoryLimit: 0,
          failedJobsHistoryLimit: 1,
          jobTemplate: {
            metadata: { labels: metadata.labels },
            spec: {
              template: {
                spec: {
                  serviceAccountName: this.serviceAccount.metadata.name,
                  restartPolicy: "OnFailure",
                  containers: [
                    {
                      name: "crashed-cleanup",
                      image: "bitnami/kubectl:latest",
                      imagePullPolicy: "Always",
                      command: ["/opt/bin/crashed-cleanup.sh"],
                      volumeMounts: [
                        { name: "script", mountPath: "/opt/bin/crashed-cleanup.sh", subPath: "crashed-cleanup.sh" },
                      ],
                      resources: {
                        requests: { memory: "128Mi" },
                        limits: { memory: "192Mi" },
                      },
                    },
                  ],
                  volumes: [{ name: "script", configMap: { name: this.script.metadata.name, defaultMode: 0o755 } }],
                },
              },
              backoffLimit: 3,
              activeDeadlineSeconds: 1800,
            },
          },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
