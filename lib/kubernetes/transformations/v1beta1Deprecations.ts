import * as pulumi from "@pulumi/pulumi";

export const v1beta1Deprecations = (o: any, opts: pulumi.CustomResourceOptions): void => {
  if (o === undefined || o.apiVersion === undefined || o.kind === undefined) {
    return;
  }

  if (o.apiVersion === "apiregistration.k8s.io/v1beta1") {
    if (o.kind === "APIService") {
      o.apiVersion = "apiregistration.k8s.io/v1";
    }
  }

  if (o.apiVersion === "storage.k8s.io/v1beta1") {
    if (o.kind === "CSIDriver") {
      o.apiVersion = "storage.k8s.io/v1";
    }
  }

  if (o.apiVersion === "rbac.authorization.k8s.io/v1beta1") {
    if (["ClusterRole", "ClusterRoleBinding", "Role", "RoleBinding"].includes(o.kind)) {
      o.apiVersion = "rbac.authorization.k8s.io/v1";
    }
  }

  if (o.apiVersion === "apiextensions.k8s.io/v1beta1") {
    if (o.kind === "CustomResourceDefinition") {
      o.apiVersion = "apiextensions.k8s.io/v1";
    }
  }

  if (["extensions/v1beta1", "networking.k8s.io/v1beta1"].includes(o.apiVersion)) {
    if (o.kind === "Ingress") {
      o.apiVersion = "networking.k8s.io/v1";

      if (o.spec !== undefined && o.spec.rules) {
        for (const rule of o.spec.rules) {
          if (rule.http !== undefined && rule.http.paths !== undefined) {
            for (const path of rule.http.paths) {
              const backend = path.backend;

              if (backend.serviceName !== undefined && backend.servicePort !== undefined) {
                path.backend = {
                  service: {
                    name: backend.serviceName,
                    port: { number: backend.servicePort },
                  },
                };
              }

              if (path.pathType === undefined) {
                path.pathType = "Prefix";
              }
            }
          }
        }
      }
    }
  }
};
