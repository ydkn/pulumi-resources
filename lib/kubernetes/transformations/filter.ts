import * as pulumi from "@pulumi/pulumi";

export interface FilterArgs {
  group: string;
  kind: string;
}

export const filter = (filters: FilterArgs[], only?: boolean) => {
  if (only === undefined) {
    only = false;
  }

  return (o: any, opts: pulumi.CustomResourceOptions): void => {
    if (o === undefined || o.apiVersion === undefined || o.kind === undefined) {
      return;
    }

    const apiVersionSplit = o.apiVersion.split("/", 2) as string[];
    const objectGroup = (apiVersionSplit.length > 1 ? apiVersionSplit[0] : "").toLowerCase();
    const objectKind = o.kind.toLowerCase();

    for (const filter of filters) {
      const filterGroup = filter.group.toLowerCase();
      const filterKind = filter.kind.toLowerCase();

      if (only === false && objectGroup === filterGroup && objectKind === filterKind) {
        o.apiVersion = "v1";
        o.kind = "List";
        o.items = [];
      } else if (only === true && (objectGroup !== filterGroup || objectKind !== filterKind)) {
        o.apiVersion = "v1";
        o.kind = "List";
        o.items = [];
      }
    }
  };
};
