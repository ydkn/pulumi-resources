import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface ReleaseArgs extends k8s.helm.v3.ReleaseArgs {}

export class Release extends k8s.helm.v3.Release {
  /**
   * Create a Release resource with the given unique name, arguments, and options.
   *
   * @param name The _unique_ name of the resource.
   * @param args The arguments to use to populate this resource's properties.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ReleaseArgs, opts?: pulumi.ComponentResourceOptions) {
    if (args.maxHistory === undefined) {
      args.maxHistory = 3;
    }

    super(name, args, opts);
  }
}
