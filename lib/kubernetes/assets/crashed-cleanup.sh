#!/usr/bin/env bash

PHASES=(
  "kube-system"
  "wireguard netbird-agent"
  "metallb netbird rook local-path-provisioner"
  "nats mongodb postgresql redis"
  "!*"
  "grafana-alloy velero"
)

function process_namespace {
  namespace="${1}"

  if ! [ "$(kubectl get pod --namespace="${namespace}" 2>/dev/null | wc -l)" -eq "0" ]; then
    for resource in $(kubectl get pod --namespace="${namespace}" -o jsonpath='{range .items[*]}{.metadata.name}{" "}{end}'); do
      for restarts in $(kubectl get pod --namespace="${namespace}" "${resource}" -o jsonpath='{range .status.containerStatuses[*]}{.restartCount}{" "}{end}'); do
        if [ "${restarts}" -gt "0" ]; then
          kubectl delete pod --namespace="${namespace}" "${resource}"

          sleep 30

          break
        fi
      done
    done
  fi

  if ! [ "$(kubectl get job --namespace="${namespace}" 2>/dev/null | wc -l)" -eq "0" ]; then
    for resource in $(kubectl get job --namespace="${namespace}" -o jsonpath='{range .items[*]}{.metadata.name}{" "}{end}'); do
      if [ "$(kubectl get job --namespace="${namespace}" "${resource}" -o jsonpath='{.status.ready}')" -eq "0" ]; then
        kubectl delete job --namespace="${namespace}" "${resource}"
      fi
    done
  fi
}

defined_namespaces=()

for namespaces in "${PHASES[@]}"; do
  for namespace in ${namespaces}; do
    if [ ! "${namespace}" == "!*" ]; then
      defined_namespaces+=("${namespace}")
    fi
  done
done

processed_namespaces=()

for namespaces in "${PHASES[@]}"; do
  for namespace in ${namespaces}; do
    namespace_already_processed="false"

    for ns in "${processed_namespaces[@]}"; do
      if [ "${ns}" == "${namespace}" ]; then
        namespace_already_processed="true"

        break
      fi
    done

    if [ "${namespace_already_processed}" == "true" ]; then
      continue
    fi

    if [ "${namespace}" == "!*" ]; then
      for namespace2 in $(kubectl get namespaces -o jsonpath='{range .items[*]}{.metadata.name}{" "}{end}'); do
        skip_namespace="false"

        for ns in "${defined_namespaces[@]}"; do
          if [ "${ns}" == "${namespace2}" ]; then
            skip_namespace="true"

            break
          fi
        done

        if [ "${skip_namespace}" == "true" ]; then
          continue
        fi

        process_namespace "${namespace2}"

        processed_namespaces+=("${namespace2}")
      done
    else
      process_namespace "${namespace}"

      processed_namespaces+=("${namespace}")
    fi
  done
done

exit 0
