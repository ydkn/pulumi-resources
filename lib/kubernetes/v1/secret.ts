import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export class Secret extends k8s.core.v1.Secret {
  /**
   * Create a new secret resource.
   * @param name The name of the secret resource.
   * @param args A bag of arguments to control the secret creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: k8s.core.v1.SecretArgs, opts?: pulumi.CustomResourceOptions) {
    const data = pulumi.all([args.stringData, args.data]).apply(([stringData, data]) => {
      data = data ?? {};

      for (const [key, value] of Object.entries(stringData ?? {})) {
        data[key] = Buffer.from(value).toString("base64");
      }

      return data;
    });

    super(name, { metadata: args.metadata, type: args.type, data }, opts);
  }
}
