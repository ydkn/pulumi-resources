import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { Certificate } from "../../certManager/index.js";

export interface SecureIngressArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<k8s.types.input.networking.v1.IngressSpec>;
  issuer?: pulumi.Input<string>;
  clusterIssuer?: pulumi.Input<string>;
}

export class SecureIngress extends k8s.networking.v1.Ingress {
  /**
   * Create a new secure ingress resource.
   * @param name The name of the secure ingress resource.
   * @param args A bag of arguments to control the secure ingress creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: SecureIngressArgs, opts?: pulumi.CustomResourceOptions) {
    const ingressArgs = pulumi
      .all([args.metadata, args.spec, args.issuer, args.clusterIssuer])
      .apply(([metadataWrapped, specWrapped, issuer, clusterIssuer]) => {
        const metadata = metadataWrapped as k8s.types.input.meta.v1.ObjectMeta;
        const spec = specWrapped as k8s.types.input.networking.v1.IngressSpec;

        if (metadata.annotations === undefined) {
          metadata.annotations = {};
        }

        const annotations: { [key: string]: pulumi.Input<string> } = {
          "traefik.ingress.kubernetes.io/router.entrypoints": "websecure",
          "traefik.ingress.kubernetes.io/router.tls": "true",
        };

        if (issuer !== undefined) {
          annotations["cert-manager.io/issuer"] = issuer;
        } else if (clusterIssuer !== undefined) {
          annotations["cert-manager.io/cluster-issuer"] = clusterIssuer;
        } else {
          annotations["cert-manager.io/cluster-issuer"] = Certificate.DefaultClusterIssuer;
        }

        metadata.annotations = pulumi
          .all([annotations, metadata.annotations])
          .apply(([annotations, metadataAnnotations]) => {
            return { ...annotations, ...metadataAnnotations };
          });

        if (spec.tls === undefined) {
          spec.tls = [{ hosts: SecureIngress.hosts(spec.rules), secretName: pulumi.interpolate`${name}-tls` }];
        }

        return { metadata, spec };
      });

    super(name, ingressArgs, opts);
  }

  private static hosts(rules?: pulumi.Input<pulumi.Input<k8s.types.input.networking.v1.IngressRule>[]>): string[] {
    if (rules === undefined) {
      return [];
    }

    return (
      (rules as k8s.types.input.networking.v1.IngressRule[])
        .map((r) => r.host)
        .filter((h) => h !== undefined) as string[]
    ).filter((value, index, self) => self.indexOf(value) === index);
  }
}
