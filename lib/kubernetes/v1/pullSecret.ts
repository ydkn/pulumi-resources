import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface PullSecretArgs {
  metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  registry: pulumi.Input<string>;
  username: pulumi.Input<string>;
  password: pulumi.Input<string>;
}

export class PullSecret extends k8s.core.v1.Secret {
  /**
   * Create a new pull secret resource.
   * @param name The name of the pull secret resource.
   * @param args A bag of arguments to control the pull secret creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: PullSecretArgs, opts?: pulumi.CustomResourceOptions) {
    const dockerConfigJSON = pulumi.all([args.registry, args.username, args.password]).apply((r) => {
      const auths: { [key: string]: { [key: string]: string } } = {};

      auths[r[0]] = { auth: Buffer.from(r[1] + ":" + r[2]).toString("base64") };

      return Buffer.from(JSON.stringify({ auths })).toString("base64");
    });

    super(
      name,
      {
        metadata: args.metadata,
        type: "kubernetes.io/dockerconfigjson",
        data: { ".dockerconfigjson": dockerConfigJSON },
      },
      opts
    );
  }
}
