import * as pulumi from "@pulumi/pulumi";

export interface SharedFilesystemArgs {
  storageClassName?: pulumi.Input<string>;
  storage?: pulumi.Input<string>;
}

export interface SharedFilesystemConfig extends SharedFilesystemArgs {
  type: string;
  id: pulumi.Input<string>;
}
