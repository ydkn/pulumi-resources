import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as path from "path";
import * as fs from "fs";
import * as url from "url";
import template from "just-template";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as certManager from "../certManager/index.js";

export interface LdapArgs {
  hostname: pulumi.Input<string>;
  username: pulumi.Input<string>;
  password: pulumi.Input<string>;
  baseDN: pulumi.Input<string>;
}

export interface FreeRadiusServerArgs {
  namespace?: pulumi.Input<string>;
  hostname: pulumi.Input<string>;
  domain: pulumi.Input<string>;
  ldap: pulumi.Input<LdapArgs>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  protect?: boolean;
}

export class FreeRadiusServer extends ComponentResource {
  private readonly localSecret: pulumi.Output<string>;
  private readonly configs: kubernetes.v1.Secret;
  private readonly issuer: certManager.Issuer;
  private readonly caCertificate: certManager.Certificate;
  private readonly caIssuer: certManager.Issuer;
  private readonly certificate: certManager.Certificate;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: kubernetes.v1.Service;
  public readonly namespace: pulumi.Output<string>;
  public readonly clientSecret: pulumi.Output<string>;
  public readonly ip: pulumi.Output<string>;

  /**
   * Create a new FreeRadius server resource.
   * @param name The name of the FreeRadius server resource.
   * @param args A bag of arguments to control the FreeRadius server creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: FreeRadiusServerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("freeradius", name, args, opts);

    if (args.resources === undefined) {
      args.resources = { requests: { memory: "96Mi" }, limits: { memory: "128Mi" } };
    }

    const emptyOpts = this.opts();
    const k8sOpts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "freeradius",
        "app.kubernetes.io/component": "freeradius",
        "app.kubernetes.io/instance": name,
      },
    };

    this.localSecret = pulumi.secret(
      new random.RandomString(`${name}-local-secret`, { special: false, length: 48 }, emptyOpts).result,
    );

    this.clientSecret = pulumi.secret(
      new random.RandomString(
        `${name}-client-secret`,
        { special: false, length: 48 },
        { ...emptyOpts, protect: args.protect },
      ).result,
    );

    this.configs = new kubernetes.v1.Secret(
      `${name}-configs`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-configs` },
        stringData: pulumi
          .all([args.domain, args.ldap, this.localSecret, this.clientSecret])
          .apply(([domain, ldap, localSecret, clientSecret]) => {
            const vars = {
              ldap: { hostname: ldap.hostname, username: ldap.username, password: ldap.password, baseDN: ldap.baseDN },
              domain,
              localSecret,
              clientSecret,
            };

            return {
              default: FreeRadiusServer.renderAssetTemplate("default", vars),
              eap: FreeRadiusServer.renderAssetTemplate("eap", vars),
              "inner-tunnel": FreeRadiusServer.renderAssetTemplate("inner-tunnel", vars),
              ldap: FreeRadiusServer.renderAssetTemplate("ldap", vars),
              "proxy.conf": FreeRadiusServer.renderAssetTemplate("proxy.conf", vars),
              "clients.conf": FreeRadiusServer.renderAssetTemplate("clients.conf", vars),
            };
          }),
      },
      k8sOpts,
    );

    this.issuer = new certManager.Issuer(name, { metadata, spec: { selfSigned: {} } }, k8sOpts);

    this.caCertificate = new certManager.Certificate(
      `${name}-ca`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-ca` },
        spec: {
          isCA: true,
          commonName: "RADIUS",
          dnsNames: [args.hostname],
          duration: "876000h0m0s", // 100y
          renewBefore: "87600h0m0s", // 10y
          privateKey: { algorithm: "RSA", size: 4096, rotationPolicy: "Always" },
          issuerRef: { kind: this.issuer.kind, name: this.issuer.metadata.name },
        },
      },
      k8sOpts,
    );

    this.caIssuer = new certManager.Issuer(
      `${name}-ca`,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-ca` },
        spec: { ca: { secretName: this.caCertificate.secretName } },
      },
      k8sOpts,
    );

    this.certificate = new certManager.Certificate(
      name,
      {
        metadata,
        spec: {
          isCA: true,
          commonName: args.hostname,
          dnsNames: [args.hostname],
          duration: "87600h0m0s", // 10y
          renewBefore: "2160h0m0s", // 90d
          privateKey: { algorithm: "RSA", size: 4096, rotationPolicy: "Always" },
          issuerRef: { kind: this.caIssuer.kind, name: this.caIssuer.metadata.name },
        },
      },
      k8sOpts,
    );

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: {
              labels: metadata.labels,
              annotations: { "backup.velero.io/backup-volumes": "data" },
            },
            spec: {
              containers: [
                {
                  name: "freeradius",
                  image: "riyangarma33/freeradius-server:latest",
                  imagePullPolicy: "Always",
                  ports: [
                    { name: "radius", containerPort: 1812, protocol: "UDP" },
                    { name: "radius-acct", containerPort: 1813, protocol: "UDP" },
                  ],
                  resources: args.resources,
                  volumeMounts: [
                    { name: "configs", mountPath: "/etc/freeradius/sites-enabled/default", subPath: "default" },
                    { name: "configs", mountPath: "/etc/freeradius/mods-enabled/eap", subPath: "eap" },
                    {
                      name: "configs",
                      mountPath: "/etc/freeradius/sites-enabled/inner-tunnel",
                      subPath: "inner-tunnel",
                    },
                    { name: "configs", mountPath: "/etc/freeradius/mods-enabled/ldap", subPath: "ldap" },
                    { name: "configs", mountPath: "/etc/freeradius/proxy.conf", subPath: "proxy.conf" },
                    { name: "configs", mountPath: "/etc/freeradius/clients.conf", subPath: "clients.conf" },

                    { name: "ca-tls", mountPath: "/etc/freeradius/certs/ca.crt", subPath: "tls.crt" },
                    { name: "tls", mountPath: "/etc/freeradius/certs/server.crt", subPath: "tls.crt" },
                    { name: "tls", mountPath: "/etc/freeradius/certs/server.key", subPath: "tls.key" },
                  ],
                },
              ],
              volumes: [
                { name: "configs", secret: { secretName: this.configs.metadata.name } },
                { name: "ca-tls", secret: { secretName: this.caCertificate.secretName } },
                { name: "tls", secret: { secretName: this.certificate.secretName } },
              ],
            },
          },
        },
      },
      k8sOpts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          type: args.serviceType ?? "ClusterIP",
          selector: metadata.labels,
          ports: [
            { name: "radius", port: 1812, targetPort: "radius", protocol: "UDP" },
            { name: "radius-acct", port: 1813, targetPort: "radius-acct", protocol: "UDP" },
          ],
        },
        hostnames: [args.hostname],
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.ip = pulumi.all([args.serviceType]).apply(([serviceType]) => {
      if (serviceType === "LoadBalancer") {
        return this.service.status.loadBalancer.ingress[0].ip;
      } else {
        return this.service.spec.clusterIP;
      }
    });

    this.registerOutputs();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static renderAssetTemplate(filename: string, data: { [key: string]: any }): pulumi.Output<string> {
    return pulumi.output(
      template(
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        fs
          .readFileSync(path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", `${filename}.tmpl`))
          .toString("utf8"),
        data,
      ),
    );
  }
}
