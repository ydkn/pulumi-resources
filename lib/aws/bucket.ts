import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as aws from "@pulumi/aws";
import { ComponentResource } from "../common/index.js";
import { AccessKey } from "./accessKey.js";

export interface BucketArgs extends aws.s3.BucketArgs {
  name?: pulumi.Input<string>;
  protect?: boolean;
}

export class Bucket extends ComponentResource {
  private static readonly sseAlgorithm = "AES256";

  private readonly id: pulumi.Output<string>;
  public readonly bucket: aws.s3.Bucket;
  public readonly accessKey: AccessKey;
  public readonly userPolicy: aws.iam.UserPolicy;

  /**
   * Create a new AWS bucket resource.
   * @param name The name of the AWS bucket resource.
   * @param args A bag of arguments to control the AWS bucket creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: BucketArgs, opts?: pulumi.ComponentResourceOptions) {
    super("aws:bucket", name, args, opts);

    const emptyOpts = this.opts();
    const awsOpts = this.opts(aws.Provider);

    this.id = new random.RandomId(
      `${name}-bucket`,
      { byteLength: 6, prefix: pulumi.interpolate`${args.name ?? name}-` },
      { ...emptyOpts, protect: args.protect },
    ).hex;

    this.bucket = new aws.s3.Bucket(
      name,
      {
        ...args,
        bucket: this.id,
        serverSideEncryptionConfiguration: {
          rule: { bucketKeyEnabled: true, applyServerSideEncryptionByDefault: { sseAlgorithm: Bucket.sseAlgorithm } },
        },
      },
      { ...awsOpts, protect: args.protect },
    );

    this.accessKey = new AccessKey(name, { name: this.id }, awsOpts);

    this.userPolicy = new aws.iam.UserPolicy(
      name,
      {
        user: this.accessKey.user.name,
        policy: {
          Version: "2012-10-17",
          Statement: [
            {
              Effect: "Allow",
              Action: ["s3:ListBucket"],
              Resource: [pulumi.interpolate`arn:aws:s3:::${this.bucket.id}`],
            },
            {
              Effect: "Allow",
              Action: ["s3:*"],
              Resource: [pulumi.interpolate`arn:aws:s3:::${this.bucket.id}/*`],
            },
          ],
        },
      },
      awsOpts,
    );

    this.registerOutputs();
  }
}
