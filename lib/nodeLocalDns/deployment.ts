import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as crypto from "crypto";
import { ComponentResource } from "../common/index.js";
import { PodMonitor } from "../prometheus/index.js";

export interface NodeLocalDnsArgs {
  namespace?: pulumi.Input<string>;
  clusterDomain?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export class NodeLocalDns extends ComponentResource {
  private static readonly repo = "kubernetes/kubernetes";
  private static readonly branch = "master";
  private static readonly path = "cluster/addons/dns/nodelocaldns/nodelocaldns.yaml";

  private readonly clusterDomain: pulumi.Output<string>;
  private readonly kubeDnsService: k8s.core.v1.Service;
  private readonly deployment: k8s.yaml.ConfigFile;
  private readonly podMonitor: PodMonitor;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new node-loca-dns resource.
   * @param name The name of the node-loca-dns resource.
   * @param args A bag of arguments to control the node-loca-dns creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: NodeLocalDnsArgs, opts?: pulumi.ComponentResourceOptions) {
    super("node-loca-dns", name, args, opts);

    if (args.resources === undefined) {
      args.resources = { requests: { memory: "48Mi" }, limits: { memory: "64Mi" } };
    }

    opts = this.opts(k8s.Provider);

    this.namespace = pulumi.output(args.namespace ?? "kube-system");

    this.clusterDomain = pulumi.interpolate`${args.clusterDomain ?? "cluster.local"}`;
    this.kubeDnsService = k8s.core.v1.Service.get(`${name}-kube-dns`, "kube-system/kube-dns", opts);

    const configHash = pulumi
      .all([this.clusterDomain, this.kubeDnsService.spec.clusterIPs])
      .apply(([clusterDomain, dnsServiceIPs]) =>
        crypto
          .createHash("sha1")
          .update(`${clusterDomain}|${dnsServiceIPs.join(",")}`)
          .digest("hex"),
      );

    this.deployment = new k8s.yaml.ConfigFile(
      name,
      {
        file: `https://raw.githubusercontent.com/${NodeLocalDns.repo}/${NodeLocalDns.branch}/${NodeLocalDns.path}`,
        transformations: [
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (o: any) => {
            /* eslint-disable @typescript-eslint/no-unsafe-member-access */
            if (o.metadata !== undefined) {
              o.metadata.namespace = this.namespace;
            }

            if (o.data !== undefined && o.data.Corefile !== undefined) {
              o.data.Corefile = this.transformPlaceholders(o.data.Corefile as string, " ");
            }

            if (o.spec !== undefined && o.spec.template !== undefined) {
              if (o.spec.template.metadata !== undefined && o.spec.template.metadata.labels !== undefined) {
                o.spec.template.metadata.labels["config-hash"] = configHash;
              }

              if ((o.spec.template.spec !== undefined && o.spec.template.spec.containers) !== undefined) {
                for (const c of o.spec.template.spec.containers) {
                  c.resources = args.resources;

                  for (const i in c.args ?? []) {
                    // eslint-disable-next-line security/detect-object-injection
                    c.args[i] = this.transformPlaceholders(c.args[i] as string, ",");
                  }

                  if (c.livenessProbe !== undefined && c.livenessProbe.httpGet !== undefined) {
                    c.livenessProbe.httpGet.host = pulumi
                      .all([c.livenessProbe.httpGet.host, this.kubeDnsService.spec.clusterIP])
                      .apply(([host, dnsServiceIP]) =>
                        (host as string).replaceAll("__PILLAR__LOCAL__DNS__", dnsServiceIP),
                      );
                  }
                }
              }
            }

            if (o.kind === "Service" && o.spec !== undefined) {
              o.spec.ipFamilyPolicy = "PreferDualStack";
            }
          },
        ],
      },
      opts,
    );

    this.podMonitor = new PodMonitor(
      name,
      {
        metadata: { name, namespace: this.namespace },
        spec: {
          selector: { matchLabels: { "k8s-app": "node-local-dns" } },
          podMetricsEndpoints: [
            {
              targetPort: "metrics",
              honorLabels: true,
              metricRelabelings: [
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: ["go_.*"].join("|"),
                },
              ],
            },
          ],
        },
      },
      opts,
    );

    this.registerOutputs();
  }

  private transformPlaceholders(data: pulumi.Input<string>, ipSeperator: pulumi.Input<string>): pulumi.Output<string> {
    return pulumi
      .all([data, this.clusterDomain, ipSeperator, this.kubeDnsService.spec.clusterIPs])
      .apply(([data, clusterDomain, ipSeperator, dnsServiceIPs]) =>
        data
          .replaceAll("__PILLAR__DNS__DOMAIN__", clusterDomain)
          .replaceAll("__PILLAR__LOCAL__DNS__,__PILLAR__DNS__SERVER__", "__PILLAR__DNS__SERVER__")
          .replaceAll("__PILLAR__LOCAL__DNS__", "")
          .replaceAll("__PILLAR__DNS__SERVER__", dnsServiceIPs.join(ipSeperator)),
      );
  }
}
