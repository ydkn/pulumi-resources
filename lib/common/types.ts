import * as pulumi from "@pulumi/pulumi";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TransformationFunc = (o: any, opts: pulumi.CustomResourceOptions) => void;
