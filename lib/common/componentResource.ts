import * as pulumi from "@pulumi/pulumi";

export class ComponentResource extends pulumi.ComponentResource {
  private readonly _opts: pulumi.ComponentResourceOptions;

  /**
   * Creates and registers a new component resource. [type] is the fully qualified type token and
   * [name] is the "name" part to use in creating a stable and globally unique URN for the object.
   * [opts.parent] is the optional parent for this component, and [opts.dependsOn] is an optional
   * list of other resources that this resource depends on, controlling the order in which we
   * perform resource operations.
   *
   * @param type The type of the resource.
   * @param name The _unique_ name of the resource.
   * @param args Information passed to [initialize] method.
   * @param opts A bag of options that control this resource's behavior.
   * @param remote True if this is a remote component resource.
   */
  constructor(
    type: string,
    name: string,
    args?: pulumi.Inputs,
    opts?: pulumi.ComponentResourceOptions,
    remote?: boolean,
  ) {
    super(type, name, args, opts, remote);

    if (opts === undefined) {
      opts = {};
    }

    this._opts = { ...opts, parent: this };
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  opts(provider?: any, opts?: pulumi.ComponentResourceOptions): pulumi.ComponentResourceOptions {
    return ComponentResource.opts({ ...this._opts, ...opts }, provider);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static opts(opts?: pulumi.ComponentResourceOptions, provider?: any): pulumi.ComponentResourceOptions {
    opts = { ...opts };

    delete opts.provider;
    delete opts.providers;

    const providers: pulumi.ProviderResource[] = [];

    if (opts.provider !== undefined) {
      providers.push(opts.provider);
    }

    if (opts.providers !== undefined) {
      const providersRecords = opts.providers as { [key: string]: pulumi.ProviderResource };

      if (providersRecords != null) {
        for (const p in providersRecords) {
          // eslint-disable-next-line security/detect-object-injection
          providers.push(providersRecords[p]);
        }
      }

      const providersList = opts.providers as pulumi.ProviderResource[];

      if (providersList != null) {
        for (const p of providersList) {
          providers.push(p);
        }
      }
    }

    if (provider !== undefined) {
      for (const p of providers) {
        if (p instanceof provider) {
          opts.provider = p;

          break;
        }
      }
    }

    return opts;
  }
}
