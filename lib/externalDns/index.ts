import { ExternalDnsArgs, ExternalDns } from "./operator.js";
import {
  ProviderSpecificArgs,
  DnsEndpointsArgs,
  DnsEndpointSpecArgs,
  DnsEndpointArgs,
  DnsEndpoint,
} from "./dnsEndpoint.js";

export {
  ProviderSpecificArgs,
  DnsEndpointsArgs,
  DnsEndpointSpecArgs,
  DnsEndpointArgs,
  DnsEndpoint,
  ExternalDnsArgs,
  ExternalDns,
};
