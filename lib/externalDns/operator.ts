import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface CloudflareArgs {
  apiToken: pulumi.Input<string>;
  proxied?: pulumi.Input<boolean>;
}

export interface ExternalDnsArgs {
  namespace?: pulumi.Input<string>;
  clusterName: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  cloudflare?: pulumi.Input<CloudflareArgs>;
}

export class ExternalDns extends ComponentResource {
  private static crdBranch = "master";

  private readonly crd: k8s.yaml.ConfigFile;
  private readonly providerSecret?: k8s.core.v1.Secret;
  private readonly helmRelease: kubernetes.helm.v3.Release;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new external-dns resource.
   * @param name The name of external-dns resource.
   * @param args A bag of arguments to control the external-dns creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ExternalDnsArgs, opts?: pulumi.ComponentResourceOptions) {
    super("external-dns", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.crd = new k8s.yaml.ConfigFile(
      name,
      {
        file: [
          "https://raw.githubusercontent.com/kubernetes-sigs/external-dns",
          ExternalDns.crdBranch,
          "docs/sources/crd/crd-manifest.yaml",
        ].join("/"),
        transformations: [
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (o: any) => {
            /* eslint-disable @typescript-eslint/no-unsafe-member-access */
            if (o.metadata !== undefined && o.metadata.annotations !== undefined) {
              o.metadata.annotations["api-approved.kubernetes.io"] =
                "https://github.com/kubernetes-sigs/external-dns/pull/2007";
            }

            return o; // eslint-disable-line @typescript-eslint/no-unsafe-return
          },
        ],
      },
      opts,
    );

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const helmValues = {
      fullnameOverride: name,
      logLevel: "info",
      logFormat: "json",
      policy: "sync",
      registry: "txt",
      txtOwnerId: args.clusterName,
      txtPrefix: "_external-dns.",
      sources: ["crd", "service", "ingress"],
      interval: "5m",
      triggerLoopOnEvent: true,
      extraArgs: ["--publish-internal-services", "--publish-host-ip"] as pulumi.Input<pulumi.Input<string>[]>,
      env: [] as pulumi.Input<pulumi.Input<k8s.types.input.core.v1.EnvVar>[]>,
      provider: undefined as string | undefined,
      service: { ipFamilyPolicy: "PreferDualStack" },
      serviceMonitor: { enabled: true },
      affinity: {
        podAntiAffinity: {
          preferredDuringSchedulingIgnoredDuringExecution: [
            {
              weight: 100,
              podAffinityTerm: {
                labelSelector: {
                  matchExpressions: [
                    { key: "app.kubernetes.io/name", operator: "In", values: ["external-dns"] },
                    { key: "app.kubernetes.io/instance", operator: "In", values: [name] },
                  ],
                },
                topologyKey: "kubernetes.io/hostname",
              },
            },
          ],
        },
      },
      resources: {
        requests: { memory: "96Mi" },
        limits: { memory: "128Mi" },
      },
    };

    if (args.cloudflare !== undefined) {
      this.providerSecret = new kubernetes.v1.Secret(
        name,
        {
          metadata: { namespace: this.namespace, name: pulumi.interpolate`${name}-cloudflare` },
          stringData: {
            cloudflare_api_token: pulumi.all([args.cloudflare]).apply(([cloudflare]) => cloudflare.apiToken),
          },
        },
        opts,
      );

      helmValues.provider = "cloudflare";

      helmValues.extraArgs = pulumi.all([helmValues.extraArgs, args.cloudflare]).apply(([extraArgs, cloudflare]) => {
        extraArgs.push("--cloudflare-dns-records-per-page=5000");

        if (cloudflare.proxied) {
          extraArgs.push("--cloudflare-proxied");
        }

        return extraArgs;
      });

      helmValues.env = pulumi.all([helmValues.env, this.providerSecret.metadata.name]).apply(([env, secretName]) => {
        if (this.providerSecret !== undefined) {
          env.push({
            name: "CF_API_TOKEN",
            valueFrom: {
              secretKeyRef: {
                name: secretName,
                key: "cloudflare_api_token",
              },
            },
          });
        }

        return env;
      });
    }

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://kubernetes-sigs.github.io/external-dns" },
        chart: "external-dns",
        values: helmValues,
      },
      opts,
    );

    this.registerOutputs();
  }
}
