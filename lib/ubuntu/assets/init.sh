#!/bin/bash -e

export DEBIAN_FRONTEND="noninteractive"

function init_bg() {
  # setup kubernetes env
  for var in $(compgen -e); do
    if [[ "${var}" = KUBERNETES_* ]]; then
      echo "${var}=\"${!var}\"" >>/etc/environment
    fi
  done

  # upgrade packages
  apt-get -qq -y dist-upgrade

  # install additional packages
  if [ -f /run/k8s/init/packages.txt ]; then
    read -ra packages </run/k8s/init/packages.txt || true
    apt-get -qq -y install "${packages[@]}"
  fi
}

# setup apt
apt-get -qq -y update

# run additional stuff in background
init_bg &

# create privilege separation directory
mkdir -p /var/run/sshd
chmod 755 /var/run/sshd

# setup ssh host keys
rm -f /etc/ssh/ssh_host_*_key
rm -f /etc/ssh/ssh_host_*_key.pub
cp /run/k8s/ssh/ssh_host_*_key /run/k8s/ssh/ssh_host_*_key.pub /etc/ssh/
chmod 600 /etc/ssh/ssh_host_*_key
chmod 644 /etc/ssh/ssh_host_*_key.pub

# install authorized keys for root
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cp /run/k8s/ssh/authorized_keys /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

# start sshd
exec /usr/sbin/sshd -D
