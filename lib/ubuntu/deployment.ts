import * as pulumi from "@pulumi/pulumi";
import * as tls from "@pulumi/tls";
import * as k8s from "@pulumi/kubernetes";
import * as path from "path";
import * as fs from "fs";
import * as url from "url";
import * as crypto from "crypto";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as localPathProvisioner from "../localPathProvisioner/index.js";
import * as rook from "../rook/index.js";

interface volume {
  mountPath: pulumi.Output<string>;
  persistentVolumeClaimName: pulumi.Output<string>;
}

export interface VolumeArgs {
  id: pulumi.Input<string>;
  mountPath: pulumi.Input<string>;
}

export interface PVCVolumeArgs extends VolumeArgs {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface NFSVolumeArgs extends VolumeArgs, kubernetes.v1.NfsVolumeConfig {
  accessModes?: pulumi.Input<pulumi.Input<string>[]>;
  storageClassName?: pulumi.Input<string>;
  size?: pulumi.Input<string>;
}

export interface UbuntuArgs {
  namespace?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  authorizedKeys?: pulumi.Input<pulumi.Input<string>[]>;
  packages?: pulumi.Input<pulumi.Input<string>[]>;
  pvcVolumes?: pulumi.Input<pulumi.Input<PVCVolumeArgs>[]>;
  nfsVolumes?: pulumi.Input<pulumi.Input<NFSVolumeArgs>[]>;
  sharedFilesystemVolumes?: pulumi.Input<pulumi.Input<VolumeArgs>[]>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  replicas?: pulumi.Input<number>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
  hostNetwork?: pulumi.Input<boolean>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
}

export class Ubuntu extends ComponentResource {
  private static readonly sshContainerPort = 22;
  public static sharedFilesystemsConfigs?: pulumi.Input<{
    [key: string]: pulumi.Input<kubernetes.v1.SharedFilesystemConfig>;
  }>;

  private readonly sshPrivateKeysRSA: tls.PrivateKey;
  private readonly sshPrivateKeysECDSA: tls.PrivateKey;
  private readonly sshPrivateKeysED25519: tls.PrivateKey;
  private readonly serviceAccount: k8s.core.v1.ServiceAccount;
  private readonly clusterRole: k8s.rbac.v1.ClusterRole;
  private readonly clusterRoleBinding: k8s.rbac.v1.ClusterRoleBinding;
  private readonly initScript: k8s.core.v1.ConfigMap;
  private readonly sshSecret: kubernetes.v1.Secret;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: kubernetes.v1.Service;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new Ubuntu resource.
   * @param name The name of the Ubuntu resource.
   * @param args A bag of arguments to control the Ubuntu creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: UbuntuArgs, opts?: pulumi.ComponentResourceOptions) {
    super("ubuntu", name, args, opts);

    const k8sOpts = this.opts(k8s.Provider);

    opts = this.opts();

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    if (args.resources === undefined) {
      args.resources = {
        requests: { memory: "256Mi" },
        limits: { memory: "384Mi" },
      };
    }

    this.sshPrivateKeysRSA = new tls.PrivateKey(`${name}-rsa`, { algorithm: "RSA", rsaBits: 8192 }, opts);
    this.sshPrivateKeysECDSA = new tls.PrivateKey(`${name}-ecdsa`, { algorithm: "ECDSA", ecdsaCurve: "P521" }, opts);
    this.sshPrivateKeysED25519 = new tls.PrivateKey(`${name}-ed25519`, { algorithm: "ED25519" }, opts);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "ubuntu",
        "app.kubernetes.io/component": "ubuntu",
        "app.kubernetes.io/instance": name,
      },
    };

    this.serviceAccount = new k8s.core.v1.ServiceAccount(name, { metadata }, k8sOpts);

    this.clusterRole = new k8s.rbac.v1.ClusterRole(
      name,
      {
        metadata,
        rules: [{ apiGroups: ["*"], resources: ["*"], verbs: ["*"] }],
      },
      k8sOpts,
    );

    this.clusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding(
      name,
      {
        metadata,
        subjects: [
          {
            kind: this.serviceAccount.kind,
            namespace: this.serviceAccount.metadata.namespace,
            name: this.serviceAccount.metadata.name,
          },
        ],
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: this.clusterRole.kind,
          name: this.clusterRole.metadata.name,
        },
      },
      k8sOpts,
    );

    this.initScript = new k8s.core.v1.ConfigMap(
      name,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-init` },
        data: {
          "init.sh": fs.readFileSync(
            path.join(path.dirname(url.fileURLToPath(import.meta.url)), "assets", "init.sh"),
            "utf-8",
          ),
          "packages.txt": pulumi
            .all([args.packages])
            .apply(([packages]) => pulumi.all(packages ?? []).apply((packages) => packages.join(" "))),
        },
      },
      k8sOpts,
    );

    this.sshSecret = new kubernetes.v1.Secret(
      name,
      {
        metadata: { ...metadata, name: pulumi.interpolate`${name}-ssh` },
        stringData: {
          ssh_host_rsa_key: this.sshPrivateKeysRSA.privateKeyOpenssh,
          "ssh_host_rsa_key.pub": this.sshPrivateKeysRSA.publicKeyOpenssh,
          ssh_host_ecdsa_key: this.sshPrivateKeysECDSA.privateKeyOpenssh,
          "ssh_host_ecdsa_key.pub": this.sshPrivateKeysECDSA.publicKeyOpenssh,
          ssh_host_ed25519_key: this.sshPrivateKeysED25519.privateKeyOpenssh,
          "ssh_host_ed25519_key.pub": this.sshPrivateKeysED25519.publicKeyOpenssh,
          authorized_keys: pulumi
            .all([args.authorizedKeys])
            .apply(([authorizedKeys]) =>
              pulumi.all(authorizedKeys ?? []).apply((authorizedKeys) => authorizedKeys.join("\n")),
            ),
        },
      },
      k8sOpts,
    );

    const volumes = pulumi
      .all([args.pvcVolumes, args.nfsVolumes, args.sharedFilesystemVolumes, Ubuntu.sharedFilesystemsConfigs ?? {}])
      .apply(([pvcVolumes, nfsVolumes, sharedFilesystemVolumes, sharedFilesystemsConfigs]) => {
        return pulumi.all(pvcVolumes ?? []).apply((pvcVolumes) => {
          return pulumi.all(nfsVolumes ?? []).apply((nfsVolumes) => {
            return pulumi.all(sharedFilesystemVolumes ?? []).apply((sharedFilesystemVolumes) => {
              const volumes: volume[] = [];

              for (const pvcVolume of pvcVolumes) {
                const pvc = new k8s.core.v1.PersistentVolumeClaim(
                  `${name}-pvc-${pvcVolume.id}`,
                  {
                    metadata: { ...metadata, name: pvcVolume.id },
                    spec: {
                      storageClassName: pvcVolume.storageClassName,
                      accessModes: pvcVolume.accessModes ?? ["ReadWriteOnce"],
                      resources: { requests: { storage: pvcVolume.size ?? "10Gi" } },
                    },
                  },
                  k8sOpts,
                );

                volumes.push({
                  mountPath: pulumi.output(pvcVolume.mountPath),
                  persistentVolumeClaimName: pvc.metadata.name,
                });
              }

              for (const nfsVolume of nfsVolumes) {
                const volume = kubernetes.v1.NfsVolume.fromConfig(
                  `${name}-nfs-${nfsVolume.id}`,
                  { metadata: { ...metadata, name: nfsVolume.id }, config: nfsVolume },
                  opts,
                );

                volumes.push({
                  mountPath: pulumi.output(nfsVolume.mountPath),
                  persistentVolumeClaimName: volume.persistentVolumeClaim.metadata.name,
                });
              }

              for (const sharedFilesystemVolume of sharedFilesystemVolumes) {
                const filesystem = sharedFilesystemsConfigs[sharedFilesystemVolume.id];

                if (filesystem === undefined) {
                  continue;
                }

                let persistentVolumeClaim: k8s.core.v1.PersistentVolumeClaim | undefined;

                switch (filesystem.type) {
                  case rook.SharedFilesystemType: {
                    const rookFilesystem = filesystem as rook.SharedFilesystemConfig;

                    persistentVolumeClaim = new rook.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      rook.sharedFilesystemArgsFromSharedFilesystemConfig(rookFilesystem, {
                        ...metadata,
                        name: sharedFilesystemVolume.id,
                      }),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                  case localPathProvisioner.SharedFilesystemType: {
                    const localPathProvisionerFilesystem = filesystem as localPathProvisioner.SharedFilesystemConfig;

                    persistentVolumeClaim = new localPathProvisioner.SharedFilesystem(
                      `${name}-fs-${sharedFilesystemVolume.id}`,
                      localPathProvisioner.sharedFilesystemArgsFromSharedFilesystemConfig(
                        localPathProvisionerFilesystem,
                        {
                          ...metadata,
                          name: sharedFilesystemVolume.id,
                        },
                      ),
                      opts,
                    ).persistentVolumeClaim;

                    break;
                  }
                }

                if (persistentVolumeClaim !== undefined) {
                  volumes.push({
                    mountPath: pulumi.output(sharedFilesystemVolume.mountPath),
                    persistentVolumeClaimName: persistentVolumeClaim.metadata.name,
                  });
                }
              }

              return volumes;
            });
          });
        });
      });

    const config = pulumi.all([volumes]).apply(([volumes]) => {
      const deploymentVolumes: k8s.types.input.core.v1.Volume[] = [];
      const deploymentVolumeMounts: k8s.types.input.core.v1.VolumeMount[] = [];

      for (const volume of volumes) {
        const volumeName = crypto.createHash("sha1").update(volume.mountPath).digest("hex");

        deploymentVolumes.push({
          name: volumeName,
          persistentVolumeClaim: { claimName: volume.persistentVolumeClaimName },
        });

        deploymentVolumeMounts.push({ name: volumeName, mountPath: volume.mountPath });
      }

      return { volumes: deploymentVolumes, volumeMounts: deploymentVolumeMounts };
    });

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: args.replicas,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              securityContext: { runAsUser: 0, runAsGroup: 0, fsGroup: 0 },
              serviceAccountName: this.serviceAccount.metadata.name,
              nodeSelector: args.nodeSelector,
              hostNetwork: args.hostNetwork,
              containers: [
                {
                  name: "ubuntu",
                  image: "registry.gitlab.com/ydkn/ubuntu-debug:rolling",
                  imagePullPolicy: "Always",
                  command: ["/run/k8s/init/init.sh"],
                  env: [
                    { name: "KUBERNETES_NODE_NAME", valueFrom: { fieldRef: { fieldPath: "spec.nodeName" } } },
                    { name: "KUBERNETES_POD_NAMESPACE", valueFrom: { fieldRef: { fieldPath: "metadata.namespace" } } },
                    { name: "KUBERNETES_POD_NAME", valueFrom: { fieldRef: { fieldPath: "metadata.name" } } },
                    { name: "KUBERNETES_POD_IP", valueFrom: { fieldRef: { fieldPath: "status.podIP" } } },
                  ],
                  ports: [{ name: "ssh", containerPort: Ubuntu.sshContainerPort, protocol: "TCP" }],
                  resources: args.resources,
                  readinessProbe: {
                    periodSeconds: 20,
                    timeoutSeconds: 3,
                    successThreshold: 1,
                    failureThreshold: 2,
                    tcpSocket: { port: "ssh" },
                  },
                  livenessProbe: {
                    periodSeconds: 30,
                    timeoutSeconds: 3,
                    successThreshold: 1,
                    failureThreshold: 5,
                    tcpSocket: { port: "ssh" },
                  },
                  volumeMounts: pulumi.all([config]).apply(([config]) => {
                    return (config.volumeMounts as k8s.types.input.core.v1.VolumeMount[]).concat([
                      { name: "init", mountPath: "/run/k8s/init" },
                      { name: "ssh", mountPath: "/run/k8s/ssh" },
                    ]);
                  }),
                },
              ],
              volumes: pulumi.all([config]).apply(([config]) => {
                return (config.volumes as k8s.types.input.core.v1.Volume[]).concat([
                  { name: "init", configMap: { name: this.initScript.metadata.name, defaultMode: 0o755 } },
                  { name: "ssh", secret: { secretName: this.sshSecret.metadata.name, defaultMode: 0o400 } },
                ]);
              }),
              tolerations: [
                { key: "node-role.kubernetes.io/master", operator: "Exists", effect: "NoSchedule" },
                { key: "node-role.kubernetes.io/control-plane", operator: "Exists", effect: "NoSchedule" },
              ],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      weight: 100,
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/component",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/component"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      k8sOpts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      k8sOpts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          type: args.serviceType ?? "ClusterIP",
          selector: metadata.labels,
          ports: [{ name: "ssh", port: 22, targetPort: "ssh" }],
        },
        hostnames: args.hostname ? [args.hostname] : undefined,
      },
      { ...k8sOpts, dependsOn: [this.deployment] },
    );

    this.registerOutputs();
  }
}
