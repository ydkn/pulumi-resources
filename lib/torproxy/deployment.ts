import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import * as kubernetes from "../kubernetes/index.js";

export interface TorProxyArgs {
  namespace?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  serviceType?: pulumi.Input<string>;
  serviceLabels?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
  serviceLoadBalancerClass?: pulumi.Input<string>;
}

export class TorProxy extends ComponentResource {
  private static readonly healthCheckURL = "https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/";

  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  private readonly service: kubernetes.v1.Service;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new TOR proxy resource.
   * @param name The name of the TOR proxy resource.
   * @param args A bag of arguments to control the TOR proxy creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: TorProxyArgs, opts?: pulumi.ComponentResourceOptions) {
    super("torproxy", name, args, opts);

    opts = this.opts(k8s.Provider);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "torproxy",
        "app.kubernetes.io/component": "torproxy",
        "app.kubernetes.io/instance": name,
      },
    };

    const probeCmd = [
      "curl",
      "--proxy",
      "http://127.0.0.1:8118",
      "--insecure",
      "--silent",
      "--output",
      "/dev/null",
      TorProxy.healthCheckURL,
    ];

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          replicas: args.replicas,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              containers: [
                {
                  name: "torprivoxy",
                  image: "avpnusr/torprivoxy:latest",
                  imagePullPolicy: "Always",
                  securityContext: { runAsUser: 102, runAsGroup: 102 },
                  ports: [{ name: "proxy", protocol: "TCP", containerPort: 8118 }],
                  readinessProbe: {
                    exec: { command: probeCmd },
                    periodSeconds: 30,
                    successThreshold: 1,
                    failureThreshold: 2,
                    timeoutSeconds: 20,
                  },
                  livenessProbe: {
                    exec: { command: probeCmd },
                    periodSeconds: 120,
                    failureThreshold: 10,
                    timeoutSeconds: 30,
                  },
                  resources: {
                    requests: { memory: "64Mi" },
                    limits: { memory: "96Mi" },
                  },
                },
              ],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      weight: 100,
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      opts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          minAvailable: 1,
          selector: { matchLabels: metadata.labels },
        },
      },
      opts,
    );

    this.service = new kubernetes.v1.Service(
      name,
      {
        hostnames: args.hostname ? [args.hostname] : undefined,
        metadata: {
          ...metadata,
          labels: pulumi.all([args.serviceLabels]).apply(([labels]) => {
            return { ...metadata.labels, ...labels };
          }),
        },
        spec: {
          loadBalancerClass: args.serviceLoadBalancerClass,
          type: args.serviceType ?? "ClusterIP",
          selector: metadata.labels,
          ports: [{ name: "proxy", protocol: "TCP", port: 8118 }],
        },
      },
      { ...opts, dependsOn: [this.deployment] },
    );

    this.registerOutputs();
  }
}
