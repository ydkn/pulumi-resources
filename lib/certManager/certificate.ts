import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface IssuerRefArgs {
  name: pulumi.Input<string>;
  kind?: pulumi.Input<string>;
}

export interface SubjectArgs {
  organizations?: pulumi.Input<pulumi.Input<string>[]>;
  countries?: pulumi.Input<pulumi.Input<string>[]>;
  organizationalUnits?: pulumi.Input<pulumi.Input<string>[]>;
  localities?: pulumi.Input<pulumi.Input<string>[]>;
  provinces?: pulumi.Input<pulumi.Input<string>[]>;
  streetAddresses?: pulumi.Input<pulumi.Input<string>[]>;
  postalCodes?: pulumi.Input<pulumi.Input<string>[]>;
  serialNumber?: pulumi.Input<string>;
}

export interface PrivateKeyArgs {
  algorithm?: pulumi.Input<string>;
  size?: pulumi.Input<number>;
  rotationPolicy?: pulumi.Input<string>;
}

export interface PasswordSecretRefArgs {
  name: pulumi.Input<string>;
  key: pulumi.Input<string>;
}

export interface Pkcs12Args {
  create?: pulumi.Input<boolean>;
  passwordSecretRef: pulumi.Input<PasswordSecretRefArgs>;
  profile?: pulumi.Input<string>;
}

export interface JksArgs {
  create?: pulumi.Input<boolean>;
  passwordSecretRef: pulumi.Input<PasswordSecretRefArgs>;
}

export interface KeystoresArgs {
  pkcs12?: pulumi.Input<Pkcs12Args>;
  jks?: pulumi.Input<JksArgs>;
}

export interface CertificateSpecArgs {
  dnsNames?: pulumi.Input<pulumi.Input<string>[]>;
  issuerRef?: pulumi.Input<IssuerRefArgs>;
  subject?: pulumi.Input<SubjectArgs>;
  commonName?: pulumi.Input<string>;
  duration?: pulumi.Input<string>;
  renewBefore?: pulumi.Input<string>;
  secretName?: pulumi.Input<string>;
  privateKey?: pulumi.Input<PrivateKeyArgs>;
  isCA?: pulumi.Input<boolean>;
  keystores?: pulumi.Input<KeystoresArgs>;
  usages?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface CertificateArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<CertificateSpecArgs>;
}

export class Certificate extends k8s.apiextensions.CustomResource {
  public static readonly DefaultClusterIssuer = "letsencrypt";

  public readonly secretName: pulumi.Output<string>;
  public readonly hostnames: pulumi.Output<string[]>;

  /**
   * Create a new certificate resource.
   * @param name The name of the certificate resource.
   * @param args A bag of arguments to control the certificate creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: CertificateArgs, opts?: pulumi.CustomResourceOptions) {
    const spec = pulumi.all([args.spec]).apply(([spec]) => {
      if (spec.secretName === undefined) {
        spec.secretName = `${name}-tls`;
      }

      if (spec.privateKey === undefined) {
        spec.privateKey = {};
      }

      if (spec.privateKey.rotationPolicy === undefined) {
        spec.privateKey.rotationPolicy = "Always";
      }

      if (spec.issuerRef === undefined) {
        spec.issuerRef = { name: Certificate.DefaultClusterIssuer, kind: "ClusterIssuer" };
      }

      if (spec.keystores?.pkcs12 !== undefined) {
        if (spec.keystores.pkcs12.profile === undefined) {
          spec.keystores.pkcs12.profile = "Modern2023";
        }
      }

      return spec;
    });

    super(name, { apiVersion: "cert-manager.io/v1", kind: "Certificate", metadata: args.metadata, spec }, opts);

    this.hostnames = pulumi.all([spec]).apply(([spec]) => (spec.dnsNames ?? []) as string[]);
    this.secretName = pulumi.all([spec]).apply(([spec]) => spec.secretName!);
  }
}
