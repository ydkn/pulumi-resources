import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface SelfSignedArgs {
  _empty?: undefined;
}

export interface CaArgs {
  secretName?: pulumi.Input<string>;
}

export interface PrivateKeySecretRefArgs {
  name: pulumi.Input<string>;
}

export interface SecretKeyRefArgs {
  name: pulumi.Input<string>;
  key: pulumi.Input<string>;
}

export interface CloudflareSolverArgs {
  email: pulumi.Input<string>;
  apiTokenSecretRef: pulumi.Input<SecretKeyRefArgs>;
}

export interface DnsSolverArgs {
  cloudflare?: pulumi.Input<CloudflareSolverArgs>;
}

export interface SolverArgs {
  dns01?: pulumi.Input<DnsSolverArgs>;
}

export interface AcmeArgs {
  email: pulumi.Input<string>;
  server: pulumi.Input<string>;
  privateKeySecretRef: pulumi.Input<PrivateKeySecretRefArgs>;
  solvers: pulumi.Input<pulumi.Input<SolverArgs>[]>;
}

export interface IssuerSpecArgs {
  selfSigned?: pulumi.Input<SelfSignedArgs>;
  ca?: pulumi.Input<CaArgs>;
  acme?: pulumi.Input<AcmeArgs>;
}

export interface IssuerArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  spec: pulumi.Input<IssuerSpecArgs>;
}

export class Issuer extends k8s.apiextensions.CustomResource {
  /**
   * Create a new issuer resource.
   * @param name The name of the issuer resource.
   * @param args A bag of arguments to control the issuer creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: IssuerArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        apiVersion: "cert-manager.io/v1",
        kind: "Issuer",
        metadata: args.metadata,
        spec: args.spec,
      },
      opts,
    );
  }
}

export class ClusterIssuer extends k8s.apiextensions.CustomResource {
  /**
   * Create a new cluster issuer resource.
   * @param name The name of the cluster issuer resource.
   * @param args A bag of arguments to control the cluster issuer creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: IssuerArgs, opts?: pulumi.CustomResourceOptions) {
    super(
      name,
      {
        apiVersion: "cert-manager.io/v1",
        kind: "ClusterIssuer",
        metadata: args.metadata,
        spec: args.spec,
      },
      opts,
    );
  }
}
