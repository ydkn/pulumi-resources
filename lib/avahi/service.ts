import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface AvahiServiceDefinitionArgs {
  type: pulumi.Input<string>;
  port?: pulumi.Input<number>;
  subtype?: pulumi.Input<string>;
  hostname?: pulumi.Input<string>;
  txtRecords?: pulumi.Input<pulumi.Input<string>[]>;
}

export interface AvahiServiceArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  labelName: pulumi.Input<string>;
  name: pulumi.Input<string>;
  services: pulumi.Input<pulumi.Input<AvahiServiceDefinitionArgs>[]>;
}

export type ConfigAvahiServicesArgs = pulumi.Input<{
  [key: string]: pulumi.Input<{
    name: pulumi.Input<string>;
    services: pulumi.Input<pulumi.Input<AvahiServiceDefinitionArgs>[]>;
  }>;
}>;

export interface ConfigAvahiServiceArgs {
  metadata: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
  labelName: pulumi.Input<string>;
  services: ConfigAvahiServicesArgs;
}

export class AvahiService extends k8s.core.v1.ConfigMap {
  /**
   * Create a new Avahi service resource.
   * @param name The name of the Avahi service resource.
   * @param args A bag of arguments to control the Avahi service creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AvahiServiceArgs, opts?: pulumi.CustomResourceOptions) {
    const data = pulumi.all([args.name, args.services]).apply(([name, services]) => {
      const serviceData: string[] = [
        `<?xml version="1.0" standalone="no"?>`,
        `<!DOCTYPE service-group SYSTEM "avahi-service.dtd">`,
        `<service-group>`,
        `<name replace-wildcards="yes">${name}</name>`,
      ];

      for (const service of services) {
        serviceData.push(`<service>`);
        serviceData.push(`<type>${service.type}</type>`);

        if (service.subtype !== undefined) {
          serviceData.push(`<subtype>${service.subtype}</subtype>`);
        }

        if (service.port !== undefined) {
          serviceData.push(`<port>${service.port}</port>`);
        }

        if (service.hostname !== undefined) {
          serviceData.push(`<host-name>${service.hostname}</host-name>`);
        }

        if (service.txtRecords !== undefined) {
          for (const txtRecord of service.txtRecords) {
            serviceData.push(`<txt-record>${txtRecord}</txt-record>`);
          }
        }

        serviceData.push(`</service>`);
      }

      serviceData.push(`</service-group>`);

      const configMapData: { [key: string]: string } = {};

      configMapData[`${name.replaceAll(" ", "-")}.service`] = serviceData.join("\n");

      return configMapData;
    });

    const metadata = pulumi.all([args.labelName, args.metadata]).apply(([labelName, metadata]) => {
      if (metadata === undefined) {
        metadata = {};
      }

      if (metadata.labels === undefined) {
        metadata.labels = {};
      }

      // eslint-disable-next-line security/detect-object-injection
      metadata.labels[labelName] = "true";

      return metadata;
    });

    super(name, { metadata, data }, opts);
  }

  static fromConfig(
    args: ConfigAvahiServiceArgs,
    opts?: pulumi.CustomResourceOptions,
  ): pulumi.Output<pulumi.Output<AvahiService>[]> {
    return pulumi.all([args.services]).apply(([services]) => {
      const avahiServices: pulumi.Output<AvahiService>[] = [];

      for (const name in services) {
        avahiServices.push(
          // eslint-disable-next-line security/detect-object-injection
          pulumi.all([name, services[name]]).apply(
            ([name, services]) =>
              new AvahiService(
                name,
                {
                  ...services,
                  metadata: pulumi.all([name, args.metadata, args.labelName]).apply(([name, metadata, labelName]) => {
                    return { ...metadata, name: pulumi.interpolate`${name}-${labelName}` };
                  }),
                  labelName: args.labelName,
                },
                opts,
              ),
          ),
        );
      }

      return avahiServices;
    });
  }
}
