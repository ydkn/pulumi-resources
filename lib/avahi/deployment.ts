import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";

export interface AvahiArgs {
  namespace?: pulumi.Input<string>;
  networkInterfaces?: pulumi.Input<pulumi.Input<string>[]>;
  nodeSelector?: pulumi.Input<{ [key: string]: pulumi.Input<string> }>;
}

export class Avahi extends ComponentResource {
  private readonly serviceAccount: k8s.core.v1.ServiceAccount;
  private readonly clusterRole: k8s.rbac.v1.ClusterRole;
  private readonly clusterRoleBinding: k8s.rbac.v1.ClusterRoleBinding;
  private readonly deployment: k8s.apps.v1.Deployment;
  private readonly podDisruptionBudget: k8s.policy.v1.PodDisruptionBudget;
  public readonly namespace: pulumi.Output<string>;
  public readonly labelName: pulumi.Output<string>;

  /**
   * Create a new Avahi resource.
   * @param name The name of the Avahi resource.
   * @param args A bag of arguments to control the Avahi creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AvahiArgs, opts?: pulumi.ComponentResourceOptions) {
    super("avahi", name, args, opts);

    opts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, opts).metadata.name;

    const metadata = {
      name,
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "avahi",
        "app.kubernetes.io/component": "avahi",
        "app.kubernetes.io/instance": name,
      },
    };

    this.serviceAccount = new k8s.core.v1.ServiceAccount(name, { metadata }, opts);

    this.clusterRole = new k8s.rbac.v1.ClusterRole(
      name,
      {
        metadata,
        rules: [{ apiGroups: [""], resources: ["configmaps"], verbs: ["get", "list"] }],
      },
      opts,
    );

    this.clusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding(
      name,
      {
        metadata,
        subjects: [
          {
            kind: this.serviceAccount.kind,
            name: this.serviceAccount.metadata.name,
            namespace: this.serviceAccount.metadata.namespace,
          },
        ],
        roleRef: {
          kind: this.clusterRole.kind,
          name: this.clusterRole.metadata.name,
          apiGroup: "rbac.authorization.k8s.io",
        },
      },
      opts,
    );

    this.labelName = pulumi.interpolate`${name}-service`;

    this.deployment = new k8s.apps.v1.Deployment(
      name,
      {
        metadata,
        spec: {
          selector: { matchLabels: metadata.labels },
          strategy: { type: "Recreate" },
          replicas: 1,
          template: {
            metadata: { labels: metadata.labels },
            spec: {
              serviceAccountName: this.serviceAccount.metadata.name,
              nodeSelector: args.nodeSelector,
              hostNetwork: true,
              initContainers: [
                {
                  name: "services",
                  image: "cyrilix/k8s-sidecar:latest",
                  imagePullPolicy: "Always",
                  env: [
                    { name: "NAMESPACE", value: "ALL" },
                    { name: "METHOD", value: "LIST" },
                    { name: "LABEL", value: this.labelName },
                    { name: "FOLDER", value: "/etc/avahi/services" },
                  ],
                  resources: {
                    requests: { memory: "32Mi" },
                    limits: { memory: "48Mi" },
                  },
                  volumeMounts: [{ name: "services", mountPath: "/etc/avahi/services" }],
                },
              ],
              containers: [
                {
                  name: "avahi",
                  image: "ydkn/avahi:latest",
                  imagePullPolicy: "Always",
                  env: [
                    {
                      name: "ALLOW_INTERFACES",
                      value: pulumi
                        .all([args.networkInterfaces ?? []])
                        .apply(([networkInterfaces]) =>
                          pulumi.all(networkInterfaces).apply((networkInterfaces) => networkInterfaces.join(",")),
                        ),
                    },
                  ],
                  resources: {
                    requests: { memory: "12Mi" },
                    limits: { memory: "16Mi" },
                  },
                  volumeMounts: [{ name: "services", mountPath: "/etc/avahi/services" }],
                },
              ],
              volumes: [{ name: "services", emptyDir: {} }],
              affinity: {
                podAntiAffinity: {
                  preferredDuringSchedulingIgnoredDuringExecution: [
                    {
                      weight: 100,
                      podAffinityTerm: {
                        labelSelector: {
                          matchExpressions: [
                            {
                              key: "app.kubernetes.io/name",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/name"]],
                            },
                            {
                              key: "app.kubernetes.io/instance",
                              operator: "In",
                              values: [metadata.labels["app.kubernetes.io/instance"]],
                            },
                          ],
                        },
                        topologyKey: "kubernetes.io/hostname",
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      },
      opts,
    );

    this.podDisruptionBudget = new k8s.policy.v1.PodDisruptionBudget(
      name,
      {
        metadata,
        spec: {
          maxUnavailable: 0,
          selector: { matchLabels: metadata.labels },
        },
      },
      opts,
    );

    this.registerOutputs();
  }
}
