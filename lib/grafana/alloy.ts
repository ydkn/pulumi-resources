import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ComponentResource } from "../common/index.js";
import { Node } from "../node/index.js";
import * as kubernetes from "../kubernetes/index.js";
import * as prometheus from "../prometheus/index.js";
import { Certificate } from "../certManager/index.js";

export interface PrometheusArgs {
  url: pulumi.Input<string>;
  username?: pulumi.Input<string>;
  password?: pulumi.Input<string>;
}

export interface LokiArgs {
  url: pulumi.Input<string>;
  username?: pulumi.Input<string>;
  password?: pulumi.Input<string>;
}

export interface AlloyArgs {
  namespace?: pulumi.Input<string>;
  clusterName: pulumi.Input<string>;
  replicas?: pulumi.Input<number>;
  prometheus?: pulumi.Input<PrometheusArgs>;
  loki?: pulumi.Input<LokiArgs>;
  hostname?: pulumi.Input<string>;
  ingressClassName?: pulumi.Input<string>;
  clusterIssuer?: pulumi.Input<string>;
  resources?: pulumi.Input<k8s.types.input.core.v1.ResourceRequirements>;
  installPrometheusCRDs?: pulumi.Input<boolean>;
  nodes: pulumi.Input<pulumi.Input<Node>[]>;
}

export class Alloy extends ComponentResource {
  private readonly prometheusCRDsRelease: pulumi.Output<kubernetes.helm.v3.Release | undefined>;
  private readonly prometheusSecret: kubernetes.v1.Secret;
  private readonly lokiSecret: kubernetes.v1.Secret;
  private readonly helmRelease: kubernetes.helm.v3.Release;
  private readonly kubeletService: k8s.core.v1.Service;
  private readonly kubeletEndpoints: k8s.core.v1.Endpoints;
  private readonly kubeletServiceMonitor: prometheus.ServiceMonitor;
  public readonly namespace: pulumi.Output<string>;

  /**
   * Create a new grafana alloy resource.
   * @param name The name of the grafana alloy resource.
   * @param args A bag of arguments to control the grafana alloy creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: AlloyArgs, opts?: pulumi.ComponentResourceOptions) {
    super("grafana:alloy", name, args, opts);

    if (args.replicas === undefined) {
      args.replicas = 1;
    }

    if (args.resources === undefined) {
      args.resources = {
        requests: { memory: "512Mi" },
        limits: { memory: "768Mi" },
      };
    }

    if (args.installPrometheusCRDs === undefined) {
      args.installPrometheusCRDs = true;
    }

    const k8sOpts = this.opts(k8s.Provider);

    this.namespace = args.namespace
      ? pulumi.output(args.namespace)
      : new k8s.core.v1.Namespace(name, { metadata: { name } }, k8sOpts).metadata.name;

    this.prometheusCRDsRelease = pulumi.all([args.installPrometheusCRDs]).apply(([installPrometheusCRDs]) => {
      if (installPrometheusCRDs === true) {
        return new kubernetes.helm.v3.Release(
          `${name}-prometheus-crds`,
          {
            name: `${name}-prometheus-crds`,
            namespace: this.namespace,
            repositoryOpts: { repo: "https://prometheus-community.github.io/helm-charts" },
            chart: "prometheus-operator-crds",
          },
          k8sOpts,
        );
      }

      return undefined;
    });

    this.prometheusSecret = new kubernetes.v1.Secret(
      `${name}-prometheus-credentials`,
      {
        metadata: { name: `${name}-prometheus-credentials`, namespace: this.namespace },
        stringData: pulumi.all([args.prometheus]).apply(([prometheus]) => {
          return {
            username: prometheus?.username ?? "",
            password: prometheus?.password ?? "",
          };
        }),
      },
      k8sOpts,
    );

    this.lokiSecret = new kubernetes.v1.Secret(
      `${name}-loki-credentials`,
      {
        metadata: { name: `${name}-loki-credentials`, namespace: this.namespace },
        stringData: pulumi.all([args.loki]).apply(([loki]) => {
          return {
            username: loki?.username ?? "",
            password: loki?.password ?? "",
          };
        }),
      },
      k8sOpts,
    );

    this.helmRelease = new kubernetes.helm.v3.Release(
      name,
      {
        name,
        namespace: this.namespace,
        repositoryOpts: { repo: "https://grafana.github.io/helm-charts" },
        chart: "alloy",
        values: {
          fullnameOverride: name,
          crds: { create: false },
          agent: {
            enableReporting: false,
            clustering: { enabled: true },
            configMap: {
              create: true,
              content: pulumi
                .all([
                  this.namespace,
                  args.clusterName,
                  args.prometheus,
                  args.loki,
                  this.prometheusSecret.metadata.name,
                  this.lokiSecret.metadata.name,
                ])
                .apply(([namespace, clusterName, prometheus, loki, prometheusSecretName, lokiSecretName]) => {
                  const config = ["logging {", `level = "info"`, `format = "json"`, "}"];

                  if (prometheus !== undefined) {
                    if (prometheus.username !== undefined && prometheus.password !== undefined) {
                      config.push(
                        `remote.kubernetes.secret "metrics-credentials" {`,
                        `  namespace = "${namespace}"`,
                        `  name = "${prometheusSecretName}"`,
                        "}",
                      );
                    }

                    config.push(`prometheus.remote_write "metrics" {`, "endpoint {", `url = "${prometheus.url}"`);

                    if (prometheus.username !== undefined && prometheus.password !== undefined) {
                      config.push(
                        "basic_auth {",
                        `username = nonsensitive(remote.kubernetes.secret.metrics-credentials.data["username"])`,
                        `password = remote.kubernetes.secret.metrics-credentials.data["password"]`,
                        "}",
                      );
                    }

                    config.push("}", `external_labels = { "cluster" = "${clusterName}" }`, "}");

                    config.push(
                      `prometheus.operator.podmonitors "metrics" {`,
                      "forward_to = [prometheus.remote_write.metrics.receiver]",
                      "}",
                      `prometheus.operator.servicemonitors "metrics" {`,
                      "forward_to = [prometheus.remote_write.metrics.receiver]",
                      "}",
                    );
                  }

                  if (loki !== undefined) {
                    if (loki.username !== undefined && loki.password !== undefined) {
                      config.push(
                        `remote.kubernetes.secret "logs-credentials" {`,
                        `namespace = "${namespace}"`,
                        `name = "${lokiSecretName}"`,
                        "}",
                      );
                    }

                    config.push(`loki.write "logs" {`, "endpoint {", `url = "${loki.url}"`);

                    if (loki.username !== undefined && loki.password !== undefined) {
                      config.push(
                        "basic_auth {",
                        `username = nonsensitive(remote.kubernetes.secret.logs-credentials.data["username"])`,
                        `password = remote.kubernetes.secret.logs-credentials.data["password"]`,
                        "}",
                      );
                    }

                    config.push("}", `external_labels = { "cluster" = "${clusterName}" }`, "}");

                    config.push(
                      `discovery.kubernetes "pods" {`,
                      `role = "pod"`,
                      "}",
                      `discovery.relabel "pods" {`,
                      "targets = discovery.kubernetes.pods.targets",
                      "rule {",
                      `source_labels = ["__meta_kubernetes_namespace"]`,
                      `action = "replace"`,
                      `target_label = "namespace"`,
                      "}",
                      "rule {",
                      `source_labels = ["__meta_kubernetes_pod_name"]`,
                      `action = "replace"`,
                      `target_label = "pod"`,
                      "}",
                      "rule {",
                      `source_labels = ["__meta_kubernetes_pod_container_name"]`,
                      `action = "replace"`,
                      `target_label = "container"`,
                      "}",
                      "rule {",
                      `source_labels = ["__meta_kubernetes_pod_label_app_kubernetes_io_name"]`,
                      `action = "replace"`,
                      `target_label = "app"`,
                      "}",
                      "rule {",
                      `source_labels = ["__meta_kubernetes_pod_label_app_kubernetes_io_component"]`,
                      `action = "replace"`,
                      `target_label = "component"`,
                      "}",
                      "}",
                      `loki.source.kubernetes "pods" {`,
                      "targets    = discovery.relabel.pods.output",
                      "forward_to = [loki.write.logs.receiver]",
                      "}",
                    );
                  }

                  return config.join("\n");
                }),
            },
            resources: args.resources,
          },
          configReloader: {
            resources: {
              requests: { memory: "32Mi" },
              limits: { memory: "48Mi" },
            },
          },
          controller: {
            type: "statefulset",
            replicas: 1,
            enableStatefulSetAutoDeletePVC: true,
          },
          serviceMonitor: { enabled: true },
          ingress: {
            enabled: args.hostname ?? false,
            ingressClassName: args.ingressClassName ?? "cilium",
            annotations: {
              "cert-manager.io/cluster-issuer": args.clusterIssuer ?? Certificate.DefaultClusterIssuer,
              "traefik.ingress.kubernetes.io/router.entrypoints": "websecure",
              "traefik.ingress.kubernetes.io/router.tls": "true",
            },
            hosts: [args.hostname],
            tls: [{ secretName: `${name}-tls`, hosts: [args.hostname] }],
            faroPort: 12345,
          },
        },
      },
      k8sOpts,
    );

    const kubeletMetadata = {
      name: "kubelet",
      namespace: this.namespace,
      labels: {
        "app.kubernetes.io/name": "kubelet",
      },
    };

    this.kubeletService = new kubernetes.v1.Service(
      "kubelet",
      {
        metadata: kubeletMetadata,
        spec: {
          clusterIP: "None",
          ports: [{ name: "https-metrics", port: 10250, targetPort: 10250, protocol: "TCP" }],
        },
      },
      k8sOpts,
    );

    this.kubeletEndpoints = new k8s.core.v1.Endpoints(
      "kubelet",
      {
        metadata: kubeletMetadata,
        subsets: [
          {
            addresses: pulumi.all([args.nodes]).apply(([nodes]) =>
              nodes.map((node) => ({
                ip: node.privateIpv4Address ?? node.ipv4Address!,
                targetRef: { kind: "Node", name: node.hostname },
              })),
            ),
            ports: [{ name: "https-metrics", port: 10250, protocol: "TCP" }],
          },
        ],
      },
      { ...k8sOpts, dependsOn: this.kubeletService },
    );

    this.kubeletServiceMonitor = new prometheus.ServiceMonitor(
      `${name}-kubelet`,
      {
        metadata: kubeletMetadata,
        spec: {
          endpoints: [
            {
              bearerTokenFile: "/var/run/secrets/kubernetes.io/serviceaccount/token",
              honorLabels: true,
              interval: "60s",
              port: "https-metrics",
              scheme: "https",
              tlsConfig: { insecureSkipVerify: true },
              relabelings: [{ sourceLabels: ["__metrics_path__"], targetLabel: "metrics_path" }],
            },
            {
              bearerTokenFile: "/var/run/secrets/kubernetes.io/serviceaccount/token",
              honorLabels: true,
              honorTimestamps: false,
              interval: "60s",
              path: "/metrics/cadvisor",
              port: "https-metrics",
              scheme: "https",
              tlsConfig: { insecureSkipVerify: true },
              metricRelabelings: [
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: "go_.*",
                },
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: [
                    "container_cpu_cfs_throttled_seconds_total",
                    "container_cpu_load_average_10s",
                    "container_cpu_system_seconds_total",
                    "container_cpu_user_seconds_total",
                  ].join("|"),
                },
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: [
                    "container_fs_io_current",
                    "container_fs_io_time_seconds_total",
                    "container_fs_io_time_weighted_seconds_total",
                    "container_fs_reads_merged_total",
                    "container_fs_sector_reads_total",
                    "container_fs_sector_writes_total",
                    "container_fs_writes_merged_total",
                  ].join("|"),
                },
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: ["container_memory_mapped_file", "container_memory_swap"].join("|"),
                },
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: ["container_file_descriptors", "container_tasks_state", "container_threads_max"].join("|"),
                },
                {
                  action: "drop",
                  sourceLabels: ["__name__"],
                  regex: "container_spec.*",
                },
                {
                  action: "drop",
                  sourceLabels: ["id", "pod"],
                  regex: ".+;",
                },
              ],
              relabelings: [{ sourceLabels: ["__metrics_path__"], targetLabel: "metrics_path" }],
            },
            {
              bearerTokenFile: "/var/run/secrets/kubernetes.io/serviceaccount/token",
              honorLabels: true,
              honorTimestamps: false,
              interval: "60s",
              path: "/metrics/probes",
              port: "https-metrics",
              scheme: "https",
              tlsConfig: { insecureSkipVerify: true },
              relabelings: [{ sourceLabels: ["__metrics_path__"], targetLabel: "metrics_path" }],
            },
          ],
          namespaceSelector: { matchNames: [this.namespace] },
          selector: { matchLabels: kubeletMetadata.labels },
        },
      },
      { ...k8sOpts, dependsOn: this.kubeletEndpoints },
    );

    this.registerOutputs();
  }
}
