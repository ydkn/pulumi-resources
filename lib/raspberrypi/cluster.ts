import * as pulumi from "@pulumi/pulumi";
import * as random from "@pulumi/random";
import * as k8s from "@pulumi/kubernetes";
import * as pulumiK0s from "@ydkn/pulumi-k0s";
import { ComponentResource } from "../common/index.js";
import * as node from "../node/index.js";
import * as fcos from "../fcos/index.js";
import * as flatcar from "../flatcar/index.js";
import * as k0s from "../k0s/index.js";

interface VLANs {
  id: pulumi.Input<number>;
  interface?: pulumi.Input<string>;
  ipv4Address?: pulumi.Input<string>;
  ipv6Address?: pulumi.Input<string>;
}

interface Node extends node.Node, node.IgnitionArgs {
  ethernetInterfaceName?: pulumi.Input<string>;
  vlans?: pulumi.Input<pulumi.Input<VLANs>[]>;
}

export interface ClusterArgs extends node.IgnitionArgs {
  name?: pulumi.Input<string>;
  nodes: pulumi.Input<{ [key: string]: pulumi.Input<Node> }>;
  sshAuthorizedKeys?: pulumi.Input<pulumi.Input<string>[]>;
  k0s?: pulumi.Input<pulumiK0s.types.input.ClusterK0sArgs>;
  hooks?: pulumi.Input<pulumiK0s.types.input.ClusterHooksArgs>;
  cloudProvider?: pulumi.Input<boolean>;
  protect?: boolean;
}

export class Cluster extends ComponentResource {
  private static readonly defaultOperatingSystem = "fcos";
  private static readonly sshUsername = "pulumi";
  private static readonly interfacesMap = {
    fcos: "end0",
    flatcar: "enabcm6e4ei0",
  };

  public readonly name: pulumi.Output<string>;
  public readonly nodes: pulumi.Output<pulumi.Output<node.Node>[]>;
  public readonly kubeconfig: pulumi.Output<string>;
  public readonly provider: k8s.Provider;
  public readonly kubeletDirPath: pulumi.Output<string>;

  /**
   * Create a new RaspberryPi cluster resource.
   * @param name The name of RaspberryPi cluster resource.
   * @param args A bag of arguments to control the RaspberryPi cluster creation.
   * @param opts A bag of options that control this resource's behavior.
   */
  constructor(name: string, args: ClusterArgs, opts?: pulumi.ComponentResourceOptions) {
    super("raspberrypi:cluster", name, args, opts);

    const emptyOpts = this.opts();

    if (args.name === undefined) {
      this.name = new random.RandomId(name, { byteLength: 3 }, { ...emptyOpts, protect: args.protect }).hex;
    } else {
      this.name = pulumi.output(args.name);
    }

    const nodes = this.createServers(
      args.nodes,
      args.disks,
      args.filesystems,
      args.files,
      args.links,
      args.systemdUnits,
      args.sshAuthorizedKeys,
    );

    const enabledNodes = nodes.apply((nodes) =>
      pulumi.all(nodes).apply((nodes) => nodes.filter((n) => n.disabled !== true)),
    );

    this.nodes = nodes;

    const k0sCluster = new k0s.Cluster(
      name,
      {
        name: this.name,
        nodes: enabledNodes,
        k0s: pulumi.all([enabledNodes, args.k0s]).apply(([nodes, k0s]) => {
          if (k0s === undefined) {
            k0s = {};
          }

          if (k0s.config === undefined) {
            k0s.config = {};
          }

          if (k0s.config.spec === undefined) {
            k0s.config.spec = {};
          }

          if (k0s.config.spec.network === undefined) {
            k0s.config.spec.network = {};
          }

          if (k0s.config.spec.network.provider === undefined) {
            k0s.config.spec.network.provider = "calico";
          }

          if (k0s.config.spec.network.provider === "calico") {
            if (k0s.config.spec.network.calico === undefined) {
              k0s.config.spec.network.calico = {};
            }

            if (k0s.config.spec.network.calico.mode === undefined) {
              k0s.config.spec.network.calico.mode = "bird";
            }

            if (k0s.config.spec.network.calico.wireguard === undefined) {
              k0s.config.spec.network.calico.wireguard = false;
            }

            if (k0s.config.spec.network.calico.ipAutodetectionMethod === undefined) {
              const interfaces = new Set<string>();
              for (const iface of Object.values(Cluster.interfacesMap)) {
                interfaces.add(iface);
              }
              for (const node of nodes) {
                if (node.ethernetInterfaceName !== undefined) {
                  interfaces.add(node.ethernetInterfaceName);
                }
              }

              k0s.config.spec.network.calico.ipAutodetectionMethod = `interface=^(${Array.from(interfaces).join("|")})$`;
            }
          }

          return k0s;
        }),
        hooks: args.hooks,
        cloudProvider: false,
        protect: args.protect,
      },
      emptyOpts,
    );

    this.kubeletDirPath = k0sCluster.kubeletDirPath;
    this.kubeconfig = k0sCluster.kubeconfig;
    this.provider = new k8s.Provider(
      `${name}-k8s`,
      { kubeconfig: this.kubeconfig, suppressHelmHookWarnings: true },
      emptyOpts,
    );

    this.registerOutputs();
  }

  private createServers(
    nodes: pulumi.Input<{ [key: string]: pulumi.Input<Node> }>,
    disks?: pulumi.Input<pulumi.Input<node.IgnitionDiskArgs>[]>,
    filesystems?: pulumi.Input<pulumi.Input<node.IgnitionFilesystemArgs>[]>,
    files?: pulumi.Input<pulumi.Input<node.IgnitionFileArgs>[]>,
    links?: pulumi.Input<pulumi.Input<node.IgnitionLinkArgs>[]>,
    systemdUnits?: pulumi.Input<pulumi.Input<node.IgnitionSystemdUnitArgs>[]>,
    sshAuthorizedKeys?: pulumi.Input<pulumi.Input<string>[]>,
  ): pulumi.Output<pulumi.Output<Node>[]> {
    const emptyOpts = this.opts();

    return pulumi.all([nodes]).apply(([nodes]) => {
      const outputNodes: pulumi.Output<Node>[] = [];

      for (const key in nodes) {
        // eslint-disable-next-line security/detect-object-injection
        const node = nodes[key];
        const sanitizedKey = key.replaceAll(":", "").toLowerCase();

        const nodeId = new random.RandomId(`node-${sanitizedKey}`, { byteLength: 3 }, emptyOpts).hex;
        const hostname = pulumi.interpolate`${this.name}-${k0s.Cluster.rolesHostname(node.roles)}-${nodeId}`;

        node.operatingSystem = node.operatingSystem ?? Cluster.defaultOperatingSystem;

        const roles: string[] = node.roles ?? [];

        let isController = false;
        for (const role of roles) {
          if (role === "controller") {
            isController = true;
            break;
          }
        }

        if (isController) {
          roles.push("etcd");
        }

        const host = pulumi
          .all([hostname, files, links, disks, filesystems, systemdUnits, sshAuthorizedKeys])
          .apply(([hostname, files, links, disks, filesystems, systemdUnits, sshAuthorizedKeys]) => {
            let osHost: fcos.Host | flatcar.Host;

            switch (node.operatingSystem) {
              case "fcos":
                osHost = new fcos.Host(
                  hostname,
                  {
                    hostname,
                    disks: pulumi
                      .all(disks ?? [])
                      .apply((disks) => pulumi.all(node.disks ?? []).apply((nodeDisks) => disks.concat(nodeDisks))),
                    filesystems: pulumi
                      .all(filesystems ?? [])
                      .apply((filesystems) =>
                        pulumi
                          .all(node.filesystems ?? [])
                          .apply((nodeFilesystems) => filesystems.concat(nodeFilesystems)),
                      ),
                    files: pulumi
                      .all(files ?? [])
                      .apply((files) =>
                        pulumi
                          .all(node.files ?? [])
                          .apply((nodeFiles) =>
                            pulumi
                              .all([this.networkManagerConfigFiles(node)])
                              .apply(([networkManagerFiles]) => files.concat(nodeFiles).concat(networkManagerFiles)),
                          ),
                      ),
                    links,
                    systemdUnits: pulumi
                      .all(systemdUnits ?? [])
                      .apply((systemdUnits) =>
                        pulumi
                          .all(node.systemdUnits ?? [])
                          .apply((nodeSystemdUnits) => systemdUnits.concat(nodeSystemdUnits)),
                      ),
                    users: pulumi.all(node.users ?? []).apply((users) => {
                      return users.concat([
                        { name: "core", sshAuthorizedKeys: (sshAuthorizedKeys ?? []).map((k) => k) },
                      ]);
                    }),
                    sshUsers: [{ name: Cluster.sshUsername, groups: ["sudo"] }],
                  },
                  emptyOpts,
                );

                break;
              case "flatcar":
                osHost = new flatcar.Host(
                  hostname,
                  {
                    hostname,
                    disks: pulumi
                      .all(disks ?? [])
                      .apply((disks) => pulumi.all(node.disks ?? []).apply((nodeDisks) => disks.concat(nodeDisks))),
                    filesystems: pulumi
                      .all(filesystems ?? [])
                      .apply((filesystems) =>
                        pulumi
                          .all(node.filesystems ?? [])
                          .apply((nodeFilesystems) => filesystems.concat(nodeFilesystems)),
                      ),
                    files: pulumi
                      .all(files ?? [])
                      .apply((files) =>
                        pulumi
                          .all(node.files ?? [])
                          .apply((nodeFiles) =>
                            pulumi
                              .all([this.networkdConfigFiles(node)])
                              .apply(([networkManagerFiles]) => files.concat(nodeFiles).concat(networkManagerFiles)),
                          ),
                      ),
                    links,
                    systemdUnits: pulumi
                      .all(systemdUnits ?? [])
                      .apply((systemdUnits) =>
                        pulumi
                          .all(node.systemdUnits ?? [])
                          .apply((nodeSystemdUnits) => systemdUnits.concat(nodeSystemdUnits)),
                      ),
                    users: pulumi.all(node.users ?? []).apply((users) => {
                      return users.concat([
                        { name: "core", sshAuthorizedKeys: (sshAuthorizedKeys ?? []).map((k) => k) },
                      ]);
                    }),
                    sshUsers: [{ name: Cluster.sshUsername, groups: ["sudo"] }],
                  },
                  emptyOpts,
                );

                break;
              default:
                throw new Error(`Unsupported operating system ${node.operatingSystem}`);
            }

            return {
              id: pulumi.output(hostname),
              hostname: pulumi.output(hostname),
              roles: pulumi.output(roles.map((role) => pulumi.output(role))),
              ipv4Address: node.ipv4Address,
              privateIpv4Address: node.privateIpv4Address,
              ipv6Address: node.ipv6Address,
              privateIpv6Address: node.privateIpv6Address,
              labels: pulumi.all([node.labels ?? {}]).apply(([labels]) => {
                const outputLabels: { [key: string]: pulumi.Output<string> } = {};

                for (const key in labels) {
                  // eslint-disable-next-line security/detect-object-injection
                  outputLabels[key] = pulumi.output(labels[key]);
                }

                return outputLabels;
              }),
              taints: pulumi.all([node.taints ?? []]).apply(([taints]) => {
                return taints.map((taint) =>
                  pulumi.output({
                    effect: pulumi.output(taint.effect),
                    key: pulumi.output(taint.key),
                    value: pulumi.output(taint.value),
                  }),
                );
              }),
              username: pulumi.output(Cluster.sshUsername),
              privateKey: osHost.sshPrivateKeys.apply((sshPrivateKeys) => {
                for (const username in sshPrivateKeys) {
                  if (username === Cluster.sshUsername) {
                    // eslint-disable-next-line security/detect-object-injection
                    return sshPrivateKeys[username].privateKeyOpenssh;
                  }
                }

                throw new Error(`No private key found for user ${Cluster.sshUsername}`);
              }),
              ignitionConfig: osHost.ignitionConfig,
              disabled: pulumi.output(node.disabled ?? false),
              ethernetInterfaceName: node.ethernetInterfaceName,
              vlans: node.vlans,
            };
          });

        outputNodes.push(host);
      }

      return outputNodes;
    });
  }

  private networkManagerConfigFiles(node: Node): pulumi.Output<node.IgnitionFileArgs[]> {
    return pulumi
      .all([node.operatingSystem!, node.ethernetInterfaceName, node.vlans])
      .apply(([os, ifaceName, vlans]) => {
        const configs: node.IgnitionFileArgs[] = [];

        const ethernetInterfaceName = Cluster.ethernetInterfaceName(os, ifaceName);

        configs.push({
          path: `/etc/NetworkManager/system-connections/${ethernetInterfaceName}.nmconnection`,
          contents: [
            "[connection]",
            "type=ethernet",
            `id=${ethernetInterfaceName}`,
            `interface-name=${ethernetInterfaceName}`,
            "[ipv4]",
            "method=auto",
            "[ipv6]",
            "method=auto",
            "addr-gen-mode=eui64",
          ].join("\n"),
          mode: 0o600,
        });

        for (const vlan of vlans ?? []) {
          const parentInterfaceName = vlan.interface ?? ethernetInterfaceName;
          const interfaceName = `${parentInterfaceName}.${vlan.id}`;

          const ipv4: string[] = ["never-default=true"];
          const ipv6: string[] = ["never-default=true"];

          if (vlan.ipv4Address !== undefined) {
            ipv4.push("method=manual", `address1=${vlan.ipv4Address}`);
          } else {
            ipv4.push("method=auto");
          }

          if (vlan.ipv6Address !== undefined) {
            ipv6.push("method=manual", `address1=${vlan.ipv6Address}`);
          } else {
            ipv6.push("method=auto", "addr-gen-mode=eui64");
          }

          configs.push({
            path: `/etc/NetworkManager/system-connections/${interfaceName}.nmconnection`,
            contents: [
              "[connection]",
              `id=${interfaceName}`,
              "type=vlan",
              `interface-name=${interfaceName}`,
              "[vlan]",
              "egress-priority-map=",
              "flags=1",
              `id=${vlan.id}`,
              "ingress-priority-map=",
              `parent=${parentInterfaceName}`,
              "[ipv4]",
              ...ipv4,
              "[ipv6]",
              ...ipv6,
            ].join("\n"),
            mode: 0o600,
          });
        }

        return configs;
      });
  }

  private networkdConfigFiles(node: Node): pulumi.Output<node.IgnitionFileArgs[]> {
    return pulumi
      .all([node.operatingSystem!, node.ethernetInterfaceName, node.vlans])
      .apply(([os, ifaceName, vlans]) => {
        const configs: node.IgnitionFileArgs[] = [];

        const ethernetInterfaceName = Cluster.ethernetInterfaceName(os, ifaceName);

        configs.push({
          path: `/etc/systemd/network/10-${ethernetInterfaceName}.network`,
          contents: [
            "[Match]",
            `Name=${ethernetInterfaceName}`,
            "[Link]",
            "RequiredForOnline=routable",
            "[Network]",
            "DHCP=yes",
            "IPv6AcceptRA=true",
            ...(vlans ?? [])
              .filter((vlan) => (vlan.interface ?? ethernetInterfaceName) === ethernetInterfaceName)
              .map((vlan) => `VLAN=${vlan.interface ?? ethernetInterfaceName}.${vlan.id}`),
          ].join("\n"),
          mode: 0o644,
        });

        for (const vlan of vlans ?? []) {
          const parentInterfaceName = vlan.interface ?? ethernetInterfaceName;
          const interfaceName = `${parentInterfaceName}.${vlan.id}`;

          const network: string[] = [];

          if (vlan.ipv4Address !== undefined) {
            network.push("DHCP=no", `Address=${vlan.ipv4Address}`);
          } else {
            network.push("DHCP=yes");
          }

          configs.push({
            path: `/etc/systemd/network/20-${interfaceName}.netdev`,
            contents: ["[NetDev]", `Name=${interfaceName}`, "Kind=vlan", "[VLAN]", `Id=${vlan.id}`].join("\n"),
            mode: 0o644,
          });

          configs.push({
            path: `/etc/systemd/network/30-${interfaceName}.network`,
            contents: [
              "[Match]",
              `Name=${interfaceName}`,
              "[Network]",
              ...network,
              "IPv6AcceptRA=true",
              "[DHCP]",
              "UseRoutes=false",
            ].join("\n"),
            mode: 0o644,
          });
        }

        return configs;
      });
  }

  private static ethernetInterfaceName(os: string, ifaceName?: string): string {
    if (ifaceName !== undefined) {
      return ifaceName;
    }

    switch (os) {
      case "fcos":
        return Cluster.interfacesMap.fcos;
      case "flatcar":
        return Cluster.interfacesMap.flatcar;
      default:
        throw new Error(`Unsupported operating system ${os}`);
    }
  }
}
